package model.exceptions;

public class CampFullException extends ModelException {
    public CampFullException(String message) {
        super(message);
    }

    public CampFullException() {
    }
}
