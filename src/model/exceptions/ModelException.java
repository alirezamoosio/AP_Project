package model.exceptions;

public class ModelException extends Exception {
    public ModelException(String message) {
        super(message);
    }

    public ModelException() {
    }
}
