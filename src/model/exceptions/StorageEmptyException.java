package model.exceptions;

public class StorageEmptyException extends ModelException {
    private int overFlowAmount;

    public StorageEmptyException(int overFlowAmount) {
        this.overFlowAmount = overFlowAmount;
    }

    public int getOverFlowAmount() {
        return overFlowAmount;
    }

    public StorageEmptyException() {
        super("You don’t have enough resources.");
    }
}