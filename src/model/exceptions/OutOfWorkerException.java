package model.exceptions;

public class OutOfWorkerException extends Exception{
    public OutOfWorkerException(String message) {
        super(message);
    }

    public OutOfWorkerException() {
        super("You don’t have any worker to build this building.");
    }
}
