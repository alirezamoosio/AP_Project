package model.exceptions;

public class InvalidCoordinatesException extends ModelException {
    private int x;
    private int y;

    public InvalidCoordinatesException(int x, int y, String message) {
        super(message);
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public InvalidCoordinatesException(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public InvalidCoordinatesException(String message) {
        super(message);
    }

    public InvalidCoordinatesException() {

    }
}
