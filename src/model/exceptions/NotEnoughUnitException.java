package model.exceptions;

public class NotEnoughUnitException extends ModelException {
    public NotEnoughUnitException(String message) {
        super(message);
    }

    public NotEnoughUnitException() {
    }


}
