package model.exceptions;

public class BuildingTowerException extends ModelException {
    public BuildingTowerException(String message) {
        super(message);
    }
}
