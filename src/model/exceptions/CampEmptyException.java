package model.exceptions;

public class CampEmptyException extends NotEnoughUnitException {
    private int overFlowAmount;

    public CampEmptyException(int overFlowAmount) {
        this.overFlowAmount = overFlowAmount;
    }

    public int getOverFlowAmount() {
        return overFlowAmount;
    }

    @Override
    public String getMessage() {
        return super.getMessage() + "\n" + "overFlowAmount : " + overFlowAmount;
    }
}
