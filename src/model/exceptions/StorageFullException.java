package model.exceptions;

public class StorageFullException extends ModelException {
    private int overFlowAmount;

    public StorageFullException(int overFlowAmount) {
        this.overFlowAmount = overFlowAmount;
    }

    public int getOverFlowAmount() {
        return overFlowAmount;
    }
}
