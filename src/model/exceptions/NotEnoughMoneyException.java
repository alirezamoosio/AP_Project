package model.exceptions;

public class NotEnoughMoneyException extends ModelException {
    public NotEnoughMoneyException() {
        super("You don’t have enough resources.");
    }
}
