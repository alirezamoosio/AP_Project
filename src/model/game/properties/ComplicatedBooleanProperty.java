package model.game.properties;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class ComplicatedBooleanProperty extends SimpleBooleanProperty implements Serializable {
    public ComplicatedBooleanProperty() {
    }

    public ComplicatedBooleanProperty(boolean initialValue) {
        super(initialValue);
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeObject(get());
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        set((Boolean) in.readObject());
    }
}
