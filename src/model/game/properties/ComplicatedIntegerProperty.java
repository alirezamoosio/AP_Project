package model.game.properties;

import javafx.beans.property.SimpleIntegerProperty;
import model.game.map.Point;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class ComplicatedIntegerProperty extends SimpleIntegerProperty implements Serializable {

    public ComplicatedIntegerProperty() {
    }

    public ComplicatedIntegerProperty(int initialValue) {
        super(initialValue);
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeObject(get());
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        set((Integer) in.readObject());
    }
}
