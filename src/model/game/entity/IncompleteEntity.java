package model.game.entity;

import constants.BookOfConstants;
import constants.ConstantNames;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class IncompleteEntity {
    private int type;
    private IntegerProperty timeRemaining = new SimpleIntegerProperty();

    public IncompleteEntity(int type) {
        this.type = type;
        setTimeRemaining(BookOfConstants.getConstant(ConstantNames.TIME_REQUIRED, type));
    }

    public IncompleteEntity(int type, int level) {
        this(type);
        setTimeRemaining(Math.max(0, getTimeRemaining() - level));
    }
    public int getType() {
        return type;
    }

    public boolean isCompleted() {
        return getTimeRemaining() == 0;
    }

    public void work() {
        if (isCompleted())
            return;
        decrementTime();
    }

    public int getTimeRemaining() {
        return timeRemaining.get();
    }

    public IntegerProperty timeRemainingProperty() {
        return timeRemaining;
    }

    public void setTimeRemaining(int timeRemaining) {
        this.timeRemaining.set(timeRemaining);
    }

    public void decrementTime() {
        setTimeRemaining(getTimeRemaining() - 1);
    }
}
