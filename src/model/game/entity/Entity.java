package model.game.entity;

import constants.BookOfConstants;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import model.game.map.HasPosition;
import model.game.map.Map;
import model.game.map.Position;
import model.game.map.PositionProperty;
import model.game.properties.ComplicatedBooleanProperty;
import model.game.properties.ComplicatedIntegerProperty;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.HashMap;

import static constants.ConstantNames.INITIAL_HEALTH;

public abstract class Entity implements HasPosition, Serializable {
    private static HashMap<Integer, Integer> typeCount = new HashMap<>();
    private IntegerProperty level = new ComplicatedIntegerProperty(0);
    private PositionProperty position = new PositionProperty();
    private IntegerProperty maxHealth = new ComplicatedIntegerProperty();
    private IntegerProperty health = new ComplicatedIntegerProperty();
    private BooleanProperty isDestroyed = new ComplicatedBooleanProperty(false);
    private Integer ID;
    private Map map;

    public Entity() {
        int before = typeCount.getOrDefault(getType(), 0);
        ID = before;
        typeCount.put(getType(), before + 1);
        maxHealth.set(BookOfConstants.getConstant(INITIAL_HEALTH, getType()));
        health.set(maxHealth.get());
    }

    public abstract int getType();

    public Position getPosition() {
        return position.get();
    }

    public void setPosition(Position position) {
        this.position.set(position);
    }

    public PositionProperty positionProperty() {
        return position;
    }


    public int getLevel() {
        return level.get();
    }

    public IntegerProperty levelProperty() {
        return level;
    }

    private void setLevel(int level) {
        this.level.set(level);
    }

    protected void incrementLevel() {
        setLevel(getLevel() + 1);
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

//    public void increaseHealth(int amount) {
//        health.set(Math.min(maxHealth.get(), health.get() + amount));
//    }

    public void damage(int amount) {
        health.set(Math.max(health.get() - amount, 0));
        if (getHealth() == 0)
            destroy();
    }

    public int getHealth() {
        return health.get();
    }

    public IntegerProperty healthProperty() {
        return health;
    }

    public void destroy() {
        isDestroyed.set(true);
    }

    public boolean isDestroyed() {
        return isDestroyed.get();
    }

    public BooleanProperty isDestroyedProperty() {
        return isDestroyed;
    }

    protected void increaseMaxHealth(int amount) {
        maxHealth.set(maxHealth.get() + amount);
        //We keep health synced with maxHealth as this function cannot be called during battle
        //Both fields are necessary for when we want to restore health for units that are alive
        health.set(maxHealth.get());
    }

    public String getName() {
        return BookOfConstants.getName(getType());
    }

    public Integer getID() {
        return ID;
    }

    public void restore() {
        health.set(maxHealth.get());
    }

    @Override
    public String toString() {
        return getPosition().getX() + " "+getPosition().getY();
    }

    public void increaseHealth(int increaseHealthAmount) {
        int newHealth = getHealth() + increaseHealthAmount;
        healthProperty().set(Math.min(maxHealth.get(), newHealth));
    }

    public int getMaxHealth() {
        return maxHealth.get();
    }

    public IntegerProperty maxHealthProperty() {
        return maxHealth;
    }

    public void setHealth(int health) {
        this.health.set(health);
    }

    public int getX() {
        return getPosition().getX();
    }

    public int getY() {
        return getPosition().getY();
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }
}
