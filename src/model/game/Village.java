package model.game;

import constants.ConstantNames;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import model.exceptions.*;
import model.game.building.Building;
import model.game.building.IncompleteBuilding;
import model.game.building.defense.DefenseBuilding;
import model.game.building.village.TownHall;
import model.game.building.village.mine.Mine;
import constants.BookOfConstants;
import model.game.map.Cell;
import model.game.map.Map;
import model.game.map.Position;
import model.game.ministry.Civil;
import model.game.ministry.Treasury;
import model.game.ministry.War;
import model.game.unit.Unit;

import java.util.ArrayList;
import java.util.HashMap;

import static constants.ConstantNames.*;
import static constants.EntityNames.*;

public class Village {
    public static final int MAP_SIZE = 30;
    private static final String NOT_ENOUGH_RESOURCES = "NOT_ENOUGH_RESOURCES";
    private static final String NO_WORKERS = "not enough workers";
    private static final int TOWNHALL_GOLD_PRIZE = 1000;
    private static final int TOWNHALL_ELIXIR_PRIZE = 500;

    private int time = 0;
    private IntegerProperty score = new SimpleIntegerProperty(0);
    private TownHall townHall;
    private Map map = new Map(MAP_SIZE, MAP_SIZE);
    private VillageMode mode = VillageMode.NORMAL;
    private Treasury treasury;
    private Civil civil;
    private War war;
    private Map attackingMap;
    private HashMap<Integer, ArrayList<Unit>> attackingUnits = new HashMap<>();
    private HashMap<Integer, ArrayList<Unit>> selectedUnits;


    public HashMap<Integer, ArrayList<Unit>> getSelectedUnits() {
        return selectedUnits;
    }
    private static Village instance;
    public static Village getInstance() {
        return instance;
    }
    public void setSelectedUnits(HashMap<Integer, ArrayList<Unit>> selectedUnits) {
        this.selectedUnits = selectedUnits;
    }

    public Village() {
        instance = this;
        buildInitialBuildings();
        Treasury.setInstance(map.getListOfBuildings(ELIXIR_STORAGE), map.getListOfBuildings(GOLD_STORAGE));
        War.setInstance(map.getListOfBuildings(BARRACKS), map.getListOfBuildings(CAMP));
        Civil.setInstance(map);
        treasury = Treasury.getInstance();
        civil = Civil.getInstance();
        war = War.getInstance();
        treasury.addToStorage(BookOfConstants.getConstant(INITIAL_GOLD), BookOfConstants.getType(GOLD_STORAGE));
        treasury.addToStorage(BookOfConstants.getConstant(INITIAL_ELIXIR), BookOfConstants.getType(ELIXIR_STORAGE));
    }

    private void buildInitialBuildings() {
        townHall = (TownHall) map.newBuilding(new Position(MAP_SIZE / 2, MAP_SIZE / 2), TOWNHALL);
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                if (i == 0 && j == 0)
                    continue;
                // TODO: 6/20/18 fix this
//                map.getCell(MAP_SIZE / 2 + i, MAP_SIZE / 2 + j).add(townHall);
            }
        }
        map.newBuilding(new Position(4, 4), GOLD_MINE);
        map.newBuilding(new Position(4, 24), GOLD_STORAGE);
        map.newBuilding(new Position(24, 4), ELIXIR_MINE);
        map.newBuilding(new Position(24, 24), ELIXIR_STORAGE);
    }

    public Treasury getTreasury() {
        return treasury;
    }

    public Civil getCivil() {
        return civil;
    }

    public War getWar() {
        return war;
    }

    public int getTime() {
        return time;
    }

    public int getLevel() {
        return townHall.getLevel();
    }

    public int getScore() {
        return score.get();
    }

    public IntegerProperty scoreProperty() {
        return score;
    }

    public void getPrize(int type) {
        System.err.println("prize: " + (score.get() + BookOfConstants.getConstant(ConstantNames.DAMAGE_PRIZE, type)));
        score.setValue(score.get() + BookOfConstants.getConstant(ConstantNames.DAMAGE_PRIZE, type));
    }

    public void setMode(VillageMode mode) {
        this.mode = mode;
    }

    public VillageMode getMode() {
        return mode;
    }

    public TownHall getTownHall() {
        return townHall;
    }

    public Map getMap() {
        return map;
    }

    public Map getAttackingMap() {
        return attackingMap;
    }

    public void setAttackingMap(Map attackingMap) {
        this.attackingMap = attackingMap;
    }

    public HashMap<Integer, ArrayList<Unit>> getAttackingUnits() {
        return attackingUnits;
    }

    public void setAttackingUnits(HashMap<Integer, ArrayList<Unit>> attackingUnits) {
        this.attackingUnits = attackingUnits;
    }

    public void nextDefensiveTurn() {
        attackingMap.nextAttackingTurn();
        time++;
    }

    public void nextNormalTurn() {
        System.out.println("next normal turn called");
        mine();
        civil.work();
        war.work();
        time++;
    }

    private void mine() {
        int goldMined = 0, elixirMined = 0;
        ArrayList<Building> goldMines = map.getListOfBuildings(GOLD_MINE), elixirMines = map.getListOfBuildings(ELIXIR_MINE);
        for (Building mine : goldMines) {
            goldMined += ((Mine) mine).getRate();
        }
        for (Building mine : elixirMines) {
            elixirMined += ((Mine) mine).getRate();
        }
        treasury.addToStorage(goldMined, BookOfConstants.getType(GOLD_STORAGE));
        treasury.addToStorage(elixirMined, BookOfConstants.getType(ELIXIR_STORAGE));
    }

    public IncompleteBuilding build(int x, int y, int type) throws BuildingTowerException, InvalidCoordinatesException {
        try {
            if (x == 0 || x == map.getWidth() || y == 0 || y == map.getHeight())
                throw new InvalidCoordinatesException();
            int goldRequired = BookOfConstants.getConstant(GOLD_REQUIRED, type);
            int elixirRequired = BookOfConstants.getConstant(ELIXIR_REQUIRED, type);
            treasury.extract(goldRequired, elixirRequired);
            return civil.build(new Position(x, y), type);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new InvalidCoordinatesException(x, y);
        } catch (StorageEmptyException e) {
            throw new BuildingTowerException(NOT_ENOUGH_RESOURCES);
        } catch (OutOfWorkerException e) {
            throw new BuildingTowerException(NO_WORKERS);
        }
    }

    public void upgrade(int type, int number) throws NotEnoughMoneyException {
        Building building = getBuilding(type, number);
        if (getCivil().getUpgradeList().containsKey(building))
            throw new IllegalArgumentException("Upgrade is in progress.");
        try {
            treasury.extractGold(building.upgradeCost());
            civil.upgrade(building);
        } catch (StorageEmptyException e) {
            throw new NotEnoughMoneyException();
        }
    }

    private void attack() {
        for (ArrayList<Unit> units : attackingUnits.values()) {
            for (Unit unit : units)
                unit.nextTurn();
        }
        for (int i = 0; i < attackingMap.getWidth(); i++) {
            for (int j = 0; j < attackingMap.getHeight(); j++) {
                Cell cell = map.getCell(i, j);
                if (cell.hasBuilding() && cell.getBuilding().isDefence())
                    ((DefenseBuilding) cell.getBuilding()).nextTurn();
            }
        }
    }

    public void returnUnitsToCamp(ArrayList<Unit> selectedUnits) {
        ArrayList<Unit> returningUnits = new ArrayList<>(selectedUnits);
        for (ArrayList<Unit> units : attackingUnits.values()) {
            for (Unit unit : units) {
                unit.restore();
                map.removeFromMap(unit);
                unit.setMap(null);
            }
        }
        War.getInstance().unitsToCamp(returningUnits);
    }

    public Building getBuilding(int type, int id) {
        return map.getListOfBuildings(type).get(id);
    }

    public void setMap(Map map) {
        this.map = map;
    }
}

