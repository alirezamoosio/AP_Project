package model.game;

import model.game.building.Building;

@FunctionalInterface
public interface BuildingCondition {
    boolean accept(Building building);
}
