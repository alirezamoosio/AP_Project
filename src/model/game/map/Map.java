package model.game.map;

import constants.BookOfConstants;
import constants.ConstantNames;
import model.exceptions.InvalidCoordinatesException;
import model.game.Village;
import model.game.building.Building;
import model.game.building.defense.Cannon;
import model.game.building.defense.DefenseBuilding;
import model.game.entity.Entity;
import model.game.unit.Direction;
import model.game.unit.Unit;
import network.udp.EntityMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Map {
    private HashMap<Integer, ArrayList<Building>> buildingsMap = new HashMap<>();
    private HashMap<Integer, ArrayList<Unit>> unitMap = new HashMap<>();
    private Cell[][] cells;
    private MapNewEntityListener listener;

    public Map(int width, int height) {
        cells = new Cell[width][height];
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                cells[i][j] = new Cell(i, j);
        for (int type : ConstantNames.getAllSoldierTypes())
            unitMap.put(type, new ArrayList<>());
        for (int type : ConstantNames.getAllBuildingTypes())
            buildingsMap.put(type, new ArrayList<>());
    }

    public Building newBuilding(Position pos, int type) {
        Building building = Building.getBuilding(type);
        addEntity(building, pos);
        return building;
    }

    private void addEntity(Entity entity, Position pos) {
        if (entity instanceof Building)
            buildingsMap.get(entity.getType()).add((Building) entity);
        if (entity instanceof Unit)
            unitMap.get(entity.getType()).add((Unit) entity);
        entity.setPosition(pos);
        getCell(pos).addEntity(entity);
        entity.setMap(this);

        //listeners
        addListeners(entity);
        if (listener != null)
            listener.addedCell(entity);
    }

    public void addListeners(Entity entity) {
        entity.positionProperty().addListener(((observable, oldValue, newValue) -> {
            if (oldValue != null)
                getCell((Position) oldValue).removeEntity(entity);
            if (newValue != null)
                getCell((Position) newValue).addEntity(entity);
        }));
        entity.isDestroyedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                Position position = entity.getPosition();
                if (entity instanceof Building) {
                    buildingsMap.get(entity.getType()).remove(entity);
                } else if (entity instanceof Unit) {
                    unitMap.get(entity.getType()).remove(entity);
                }
                getCell(position).removeEntity(entity);
            }
        });
    }

    public Building newBuilding(Position pos, String name) {
        return newBuilding(pos, BookOfConstants.getType(name));
    }

    public Cell getCell(Position position) {
        return getCell(position.getX(), position.getY());
    }

    public Cell getCell(int x, int y) {
        return cells[x][y];
    }

    public ArrayList<Building> getListOfBuildings(int type) {
        return buildingsMap.get(type);
    }

    public ArrayList<Building> getListOfBuildings(String name) {
        return buildingsMap.get(BookOfConstants.getType(name));
    }

    public int getWidth() {
        if (cells != null)
            return cells.length;
        return 0;
    }

    public int getHeight() {
        if (cells != null && cells.length > 0 && cells[0] != null)
            return cells[0].length;
        return 0;
    }

    public boolean isValidPosition(Position position) {
        return position.getX() >= 0 && position.getX() < getWidth()
                && position.getY() >= 0 && position.getY() <= getHeight();
    }

    public void setListener(MapNewEntityListener listener) {
        this.listener = listener;
    }

    public void removeFromMap(Unit unit) {
        List<Unit> sameTypeUnits = unitMap.get(unit.getType());
        if (!sameTypeUnits.contains(unit))
            throw new IllegalArgumentException("Unit is not on map");
        sameTypeUnits.remove(unit);
        unit.setPosition(null);
        unit.setMap(null);
    }

    public void addUnit(Unit unit, Position position) throws InvalidCoordinatesException {
        if (position.getX() == 0 || position.getY() == 0 || position.getX() == Village.MAP_SIZE - 1 || position.getY() == Village.MAP_SIZE - 1)
            addEntity(unit, position);
        else
            throw new InvalidCoordinatesException();
    }

    public void nextAttackingTurn() {
        for (ArrayList<Unit> units : unitMap.values())
            for (Unit unit : units) {
                unit.nextTurn();
                System.err.println(unit);
            }
        for (ArrayList<Building> buildings : buildingsMap.values()) {
            for (Building building : buildings) {
                if (building.isDefence())
                    ((DefenseBuilding) building).nextTurn();
                else
                    break;
            }
        }
    }

    public Entity getEntity(int type, int id) {
        Entity entity = null;
        if (buildingsMap.containsKey(type)) {
            for (Building building : buildingsMap.get(type))
                if (building.getID() == id)
                    entity = building;
        }
        if (unitMap.containsKey(type)) {
            for (Unit unit : unitMap.get(type))
                if (unit.getID() == id)
                    entity = unit;
        }
        if (entity == null)
            throw new IllegalArgumentException("no entity with given type / id");
        return entity;
    }

    public List<EntityMessage> turnToMessages() {
        List<EntityMessage> messages = new ArrayList<>();
        for (ArrayList<Unit> units : unitMap.values())
            for (Unit unit : units) {
                messages.add(new EntityMessage(unit));
            }
        for (ArrayList<Building> buildings : buildingsMap.values()) {
            for (Building building : buildings) {
                messages.add(new EntityMessage(building));
            }
        }

        return messages;
    }

    public void changeWithMessages(List<EntityMessage> messages) {
        for (EntityMessage message : messages) {
           changeWithMessage(message);
        }
    }

    public void changeWithMessage(EntityMessage message) {
        Entity entity = null;
        try {
            entity = getEntity(message.getEntityType(), message.getEntityID());
        } catch (IllegalArgumentException e) {
            try {
                entity = newBuilding(message.getPosition(), message.getEntityType());
                while (entity.getLevel() < message.getLevel())
                    try {
                        ((Building) entity).upgrade();
                    }catch (IllegalArgumentException ignored) {
                    }
            } catch (IllegalArgumentException f) {
                entity = Unit.getUnit(message.getEntityType(), message.getLevel());
                try {
                    addUnit((Unit) entity, message.getPosition());
                } catch (InvalidCoordinatesException e1) {
                    e1.printStackTrace();
                }
            } finally {
                try {
                    entity.setID(message.getEntityID());
                } catch (NullPointerException z) {
                    for (int i = 0; i < 10; i++) {
                        System.out.println("z null");
                    }
                }
            }
        }
        entity.setPosition(message.getPosition());

        if (entity instanceof Unit) {
            ((Unit) entity).setPoint(message.getPoint());
            ((Unit) entity).setDirection(Direction.getDirection(message.getDirection()));
        }
        entity.setHealth(message.getNewHealth());
        //check for destruction of entity (no listeners are implemented
        if (entity.getHealth() <= 0) {
            entity.destroy();
        }


    }

    public HashMap<Integer, ArrayList<Building>> getBuildingsMap() {
        return buildingsMap;
    }

    public void addListenerForAllBuildings() {
        for (List<Building> buildings : buildingsMap.values()) {
            for (Building building : buildings) {
                addListeners(building);
                building.setMap(this);
            }
        }
    }

    public HashMap<Integer, ArrayList<Unit>> getUnitMap() {
        return unitMap;
    }
}
