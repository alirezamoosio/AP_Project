package model.game.map;

import model.game.entity.Entity;

public interface MapNewEntityListener {
    void addedCell(Entity entity);
}
