package model.game.map;

import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class PositionProperty implements ObservableValue, Serializable {

    private ArrayList<InvalidationListener> invalidationListeners = new ArrayList<>();
    private ArrayList<ChangeListener> changeListeners = new ArrayList<>();

    private Position position;

    public Position get() {
        return position;
    }

    public void set(Position position) {
        Position oldValue = this.position;
        this.position = position;
        notifyChange(oldValue, position);
    }

    @Override
    public void addListener(ChangeListener listener) {
        changeListeners.add(listener);
    }

    @Override
    public void removeListener(ChangeListener listener) {
        changeListeners.remove(listener);
    }

    @Override
    public Object getValue() {
        return position;
    }

    @Override
    public void addListener(InvalidationListener listener) {
        invalidationListeners.add(listener);
    }

    @Override
    public void removeListener(InvalidationListener listener) {
        invalidationListeners.remove(listener);
    }

    private void notifyChange(Position oldValue, Position newValue) {
        for (InvalidationListener listener : invalidationListeners)
            listener.invalidated(this);
        for (ChangeListener listener : changeListeners)
            listener.changed(this, oldValue, newValue);
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeObject(position);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        position = (Position) in.readObject();
    }
}
