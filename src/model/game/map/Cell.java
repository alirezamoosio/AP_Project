package model.game.map;

import model.game.building.Building;
import model.game.building.defense.Trap;
import model.game.entity.Entity;
import model.game.unit.Unit;

import java.util.ArrayList;
import java.util.List;

public class Cell implements HasPosition {
    private Position position;
    private ArrayList<Unit> units = new ArrayList<>();
    private Building building;

    Cell(Position position) {
        this.position = position;
    }

    Cell(int x, int y) {
        this(new Position(x, y));
    }

    public Building getBuilding() {
        return building;
    }

    public List<Unit> getUnits() {
        return units;
    }

    void addBuilding(Building building) {
        if (hasBuilding())
            throw new IllegalArgumentException("This cell already has a building.");
        this.building = building;
    }

    void removeBuilding() {
        if (!hasBuilding())
            throw new IllegalArgumentException("Cell has No building to remove.");
        building = null;
    }

    void removeEntity(Entity entity) {
        if (entity instanceof Building) {
            if (getBuilding().equals(building))
                removeBuilding();
            else
                throw new IllegalArgumentException("Building requested to remove is different from bulding in map");
        } else {
            removeUnit((Unit) entity);
        }
    }

    void addEntity(Entity entity) {
        if (entity instanceof Building) {
            addBuilding((Building) entity);
        } else {
            addUnit((Unit) entity);
        }
    }

    private void addUnit(Unit unit) {
        units.add(unit);
    }

    void removeUnit(Unit unit) {
        units.remove(unit);
    }

    public boolean hasBuilding() {
        return building != null;
    }

    public boolean hasVisibleBuilding() {
        return building != null && !(building instanceof Trap) && !building.isDestroyed();
    }

    public boolean hasUnit() {
        return !units.isEmpty();
    }

    @Override
    public Position getPosition() {
        return position;
    }
}