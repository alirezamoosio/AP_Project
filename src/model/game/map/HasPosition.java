package model.game.map;

public interface HasPosition {
    Position getPosition();
}
