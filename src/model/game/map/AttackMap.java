package model.game.map;

import javafx.beans.property.IntegerProperty;
import model.game.building.Building;

public class AttackMap extends Map {

    private IntegerProperty goldGained;
    private IntegerProperty elixirGained;

    public AttackMap(int width, int height) {
        super(width, height);
    }

    @Override
    public Building newBuilding(Position pos, int type) {
        Building building = super.newBuilding(pos, type);
        building.isDestroyedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                increaseGoldGained(building.getPrize());
            }
        });
        return building;
    }

    public int getGoldGained() {
        return goldGained.get();
    }

    public IntegerProperty goldGainedProperty() {
        return goldGained;
    }

    public int getElixirGained() {
        return elixirGained.get();
    }

    public IntegerProperty elixirGainedProperty() {
        return elixirGained;
    }

    private void increaseGoldGained(int amount) {
        goldGainedProperty().set(getGoldGained() + amount);
    }

    private void increaseElixirGained(int amount) {
        elixirGainedProperty().set(getElixirGained() + amount);
    }

}
