package model.game.map;

import java.util.Objects;

public class Position {
    private final int x;
    private final int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x &&
                y == position.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public static double getDistance(Position firstPoint, Position secondPoint) {
        int disX = firstPoint.x - secondPoint.x;
        int disY = firstPoint.y - secondPoint.y;
        return Math.sqrt(disX * disX + disY * disY);
    }

    public static double getDistance(HasPosition first, HasPosition second) {
        return getDistance(first.getPosition(), second.getPosition());
    }

    public Point toPoint() {
        return new Point(x, y);
    }

    @Override
    public String toString() {
        return
                "x=" + x +
                ", y=" + y;
    }
}
