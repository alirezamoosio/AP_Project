package model.game.map;

import java.io.Serializable;
import java.util.Objects;

public class Point implements Serializable {
    private static final double DELTA = 0.01;
    private final double x;
    private final double y;


    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || obj.getClass() != getClass())
            return false;
        Point point = (Point) obj;
        return this.x == point.x && this.y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public boolean isClose(Position position) {
        return (x - position.getX()) * (x - position.getX()) + (y - position.getY()) * (y - position.getY()) < DELTA;
    }

    public boolean isCloseToInt() {
        if ((x - (int)x) * (x - (int)x) + (y - (int)y) * (y - (int)y) < DELTA)
            return true;
        if ((x - (int)x + 1) * (x - (int)x + 1) + (y - (int)y) * (y - (int)y) < DELTA)
            return true;
        if ((x - (int)x) * (x - (int)x) + (y - (int)y + 1) * (y - (int)y + 1) < DELTA)
            return true;
        if ((x - (int)x + 1) * (x - (int)x + 1) + (y - (int)y + 1) * (y - (int)y + 1) < DELTA)
            return true;
        return false;
    }

    public Point pairWiseDivide(Point other) {
        return new Point(x / other.x , y /other.y);
    }

    public Point pairWiseProduct(Point other) {
        return new Point(x * other.x, y * other.y);
    }

    public Point pairWiseSubstract(Point other) {
        return new Point(x - other.x, y - other.y);
    }

    public Point multiplyAll(double ratio) {
        return pairWiseProduct(new Point(ratio, ratio));
    }
}
