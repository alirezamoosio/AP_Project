package model.game.building;

import constants.BookOfConstants;
import constants.ConstantNames;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import model.game.map.Position;
import model.game.map.Map;

public class IncompleteBuilding extends Building {

    private int futureType;
    private IntegerProperty timeRemaining = new SimpleIntegerProperty();

    public void work() {
        setTimeRemaining(getTimeRemaining() - 1);
        if (getTimeRemaining() == 0) {
            Position position = getPosition();
            Map map = getMap();
            destroy();
            map.newBuilding(position, futureType);
        }
    }

    public int getFutureType() {
        return futureType;
    }
    public void setFutureType(int futureType) {
        this.futureType = futureType;
        setTimeRemaining(BookOfConstants.getConstant(ConstantNames.TIME_REQUIRED, futureType));
    }

    public int getTimeRemaining() {
        return timeRemaining.get();
    }
    public IntegerProperty timeRemainingProperty() {
        return timeRemaining;
    }
    private void setTimeRemaining(int timeRemaining) {

        this.timeRemaining.set(timeRemaining);
    }

    @Override
    public void upgrade() {

    }
    @Override
    public int getType() {
        return 1378;
    }

}
