package model.game.building.defense;

import model.game.UnitCondition;
import model.game.gun.Gun;
import model.game.gun.TrapGun;
import model.game.unit.Unit;

public class Trap extends DefenseBuilding {
    private Gun gun = new TrapGun(getTargetCondition(), this);

    @Override
    protected Gun getGun() {
        return gun;
    }

    @Override
    protected UnitCondition getTargetCondition() {
        return Unit::isGround;
    }

    @Override
    public int getType() {
        return 13;
    }
}
