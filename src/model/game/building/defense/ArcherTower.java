package model.game.building.defense;

import model.game.UnitCondition;
import model.game.gun.Gun;
import model.game.gun.LittleGun;
import model.game.unit.Unit;

public class ArcherTower extends DefenseBuilding {


    private Gun gun = new LittleGun(getTargetCondition(), getInitialDamage());

    @Override
    public Gun getGun() {
        return gun;
    }

    @Override
    public UnitCondition getTargetCondition() {
        return Unit::isGround;
    }

    @Override
    public int getType() {
        return 8;
    }
}
