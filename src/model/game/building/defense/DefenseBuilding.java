package model.game.building.defense;

import constants.BookOfConstants;
import model.game.map.Position;
import model.game.UnitCondition;
import model.game.building.Building;
import model.game.gun.Gun;
import model.game.map.Cell;
import model.game.unit.Unit;

import static constants.ConstantNames.ATTACK_RANGE;
import static constants.ConstantNames.INITIAL_DAMAGE;

public abstract class DefenseBuilding extends Building {
    private Position target;

    public void upgrade() {
        increaseMaxHealth(healthUpgradeRate());
        getGun().increaseDamage(damageUpgradeRate());
        incrementLevel();
    }

    private int damageUpgradeRate() {
        return 1;
    }

    private int healthUpgradeRate() {
        return 10;
    }

    public void nextTurn() {
        setTarget();
        getGun().shoot(getMap(), target);
    }

    private void setTarget() {
        target = null;
        double minDist = Double.MAX_VALUE;
        for (int i = 0; i < getMap().getWidth(); i++) {
            for (int j = 0; j < getMap().getHeight(); j++) {
                Cell candidateCell = getMap().getCell(i, j);
                if (!candidateCell.hasUnit())
                    continue;
                for (Unit unit : candidateCell.getUnits()) {
                    if (getTargetCondition().accept(unit)) {
                        double distance = Position.getDistance(candidateCell, this);
                        if (distance < minDist && distance <= BookOfConstants.getConstant(ATTACK_RANGE, getType())) {
                            minDist = distance;
                            target = candidateCell.getPosition();
                        }
                    }
                }
            }
        }
    }

    int getInitialDamage() {
        return BookOfConstants.getConstant(INITIAL_DAMAGE, getType());
    }
    protected abstract Gun getGun();
    protected abstract UnitCondition getTargetCondition();
}
