package model.game.building.defense;

import model.game.UnitCondition;
import model.game.gun.DummyGun;
import model.game.gun.Gun;

public class Wall extends DefenseBuilding {
    private Gun gun = new DummyGun();

    @Override
    protected Gun getGun() {
        return gun;
    }

    @Override
    protected UnitCondition getTargetCondition() {
        return unit -> false;
    }

    @Override
    public int getType() {
        return 12;
    }
}
