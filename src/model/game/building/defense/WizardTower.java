package model.game.building.defense;

import model.game.UnitCondition;
import model.game.gun.CannonGun;
import model.game.gun.Gun;
import model.game.map.Cell;
import model.game.unit.Unit;


public class WizardTower extends DefenseBuilding {

    private Gun gun = new CannonGun(unit -> true, getInitialDamage());

    @Override
    public Gun getGun() {
        return gun;
    }

    @Override
    public UnitCondition getTargetCondition() {
        return Unit::isAir;
    }

    @Override
    public int getType() {
        return 11;
    }
}
