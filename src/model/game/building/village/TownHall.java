package model.game.building.village;

import model.game.building.Building;
import constants.BookOfConstants;
import constants.ConstantNames;
import model.game.ministry.Civil;
import model.game.ministry.Treasury;
import java.util.ArrayList;
import static constants.ConstantNames.ELIXIR_REQUIRED;
import static constants.ConstantNames.GOLD_REQUIRED;
import static constants.EntityNames.INCOMPLETE_BUILDING;
import static constants.EntityNames.TOWNHALL;

public class TownHall extends Building {
    @Override
    public void upgrade() {
        incrementLevel();
        Civil.getInstance().addWorker();
    }

    @Override
    public int getType() {
        return 5;
    }

    public ArrayList<String> availableBuildings() {
        ArrayList<String> availableBuildings = new ArrayList<>();
        for (int type : ConstantNames.getAllBuildingTypes()) {
            if (type == BookOfConstants.getType(TOWNHALL) || type == BookOfConstants.getType(INCOMPLETE_BUILDING))
                continue;
            int goldRequired = BookOfConstants.getConstant(GOLD_REQUIRED, type);
            int elixirRequired = BookOfConstants.getConstant(ELIXIR_REQUIRED, type);
            if (Treasury.getInstance().hasResources(goldRequired, elixirRequired))
                availableBuildings.add(BookOfConstants.getName(type));
        }
        return availableBuildings;
    }
}
