package model.game.building.village.mine;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import model.game.building.Building;
import constants.BookOfConstants;
import constants.ConstantNames;

public abstract class Mine extends Building {
    public int getRate() {
        return rate.get();
    }

    public IntegerProperty rateProperty() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate.set(rate);
    }

    private IntegerProperty rate =
            new SimpleIntegerProperty(BookOfConstants.getConstant(ConstantNames.INITIAL_RATE, getType()));


    @Override
    public void upgrade() {
        incrementLevel();
        setRate(getRate() * 8 / 5);
    }
}
