package model.game.building.village;

import model.game.building.Building;
import model.game.entity.IncompleteEntity;

import java.util.LinkedList;
import java.util.Queue;

public class Barracks extends Building {
    private Queue<Integer> waitingList = new LinkedList<>();
    private Queue<Integer> doneList = new LinkedList<>();
    private IncompleteEntity currentUnit;

    public void work() {
        //no current unit
        if (!isWorking()) {
            if (waitingList.isEmpty())
                return;
            callNextUnit();
        } else {
            currentUnit.work();
            if (currentUnit.isCompleted()) {
                sendToDoneList(currentUnit.getType());
                currentUnit = null;
                callNextUnit();
            }
        }
    }

    private void callNextUnit() {
        if (isWorking() || waitingList.isEmpty())
            return;
        currentUnit = new IncompleteEntity(waitingList.poll(), getLevel());
        //handling case of zero build time
        if (currentUnit.isCompleted()) {
            sendToDoneList(currentUnit.getType());
            callNextUnit();
        }
    }

    private void sendToDoneList(int type) {
        doneList.add(type);
    }

    private void createUnit(int type) {
        waitingList.add(type);
        if (!isWorking())
            callNextUnit();
    }

    public void createUnit(int type, int number) {
        for (int i = 0; i < number; i++) {
            createUnit(type);
        }
    }


    public Integer[] getWaitingList() {
        return waitingList.toArray(new Integer[waitingList.size()]);
    }

    public Queue<Integer> getDoneList() {
        return doneList;
    }

    public int pollDoneList() {
        return doneList.poll();
    }

    public IncompleteEntity getCurrentUnit() {
        return currentUnit;
    }

    public boolean isWorking() {
        return currentUnit != null;
    }

    @Override
    public void upgrade() {
        incrementLevel();
    }

    @Override
    public int getType() {
        return 6;
    }
}
