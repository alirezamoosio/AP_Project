package model.game.building.village.storage;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import model.exceptions.StorageEmptyException;
import model.exceptions.StorageFullException;
import model.game.building.Building;
import constants.BookOfConstants;
import constants.ConstantNames;

public abstract class Storage extends Building implements Comparable<Storage>{
    private IntegerProperty holding = new SimpleIntegerProperty(0);
    private IntegerProperty capacity =
        new SimpleIntegerProperty(BookOfConstants.getConstant(ConstantNames.INITIAL_CAPACITY, getType()));

    public void add(int amount) throws StorageFullException {
        if(getHolding() + amount > getCapacity())
            throw new StorageFullException(amount + getHolding() - getCapacity());
        setHolding(getHolding() + amount);
    }

    public void withdraw(int amount) throws StorageEmptyException {
        if (getHolding() < amount) {
            throw new StorageEmptyException(amount - getHolding());
        }
        setHolding(getHolding() - amount);
    }

    @Override
    public void upgrade() {
        incrementLevel();
        setCapacity(getCapacity() * 8 / 5);
    }

    public void fillStorage() {
        setHolding(getCapacity());
    }
    public boolean isFull() {
        return getHolding() == getCapacity();
    }
    public void emptyStorage() {
        setHolding(0);
    }

    @Override
    public int compareTo(Storage storage) {
        return getHolding() - storage.getHolding();
    }


    public int getHolding() {
        return holding.get();
    }
    public IntegerProperty holdingProperty() {
        return holding;
    }
    public void setHolding(int holding) {

        this.holding.set(holding);
    }

    public int getCapacity() {
        return capacity.get();
    }
    public IntegerProperty capacityProperty() {
        return capacity;
    }
    public void setCapacity(int capacity) {
        this.capacity.set(capacity);
    }
}
