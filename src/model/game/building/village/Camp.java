package model.game.building.village;

import model.exceptions.CampFullException;
import model.exceptions.NotEnoughUnitException;
import model.game.building.Building;
import constants.BookOfConstants;
import constants.ConstantNames;
import model.game.unit.Unit;

import java.util.ArrayList;
import java.util.HashMap;

public class Camp extends Building implements Comparable<Camp> {

    private HashMap<Integer, ArrayList<Unit>> units = new HashMap<>();

    private int capacity = BookOfConstants.getConstant(ConstantNames.INITIAL_CAPACITY, getType());
    private int numberOfUnits = 0;

    public Camp() {
        for (int type : ConstantNames.getAllSoldierTypes()) {
            units.put(type, new ArrayList<>());
        }
    }

    public ArrayList<Unit> extract(int type, int number) throws NotEnoughUnitException {
        ArrayList<Unit> appropriateUnits = units.get(type);
        if (appropriateUnits.size() < number)
            throw new NotEnoughUnitException();
        numberOfUnits -= number;
        ArrayList<Unit> extracted = new ArrayList<>();
        for (int i = 0; i < number; i++)
            extracted.add(units.get(type).get(i));
        units.get(type).removeAll(extracted);
        return extracted;
    }

    public void addUnit(Unit newUnit) throws CampFullException {
        if (capacity == numberOfUnits)
            throw new CampFullException();
        numberOfUnits++;
        units.get(newUnit.getType()).add(newUnit);
    }

    public boolean hasSpace() {
        return capacity > numberOfUnits;
    }

    @Override
    public void upgrade() {
        throw new IllegalArgumentException();
    }

    public int getCapacity() {
        return capacity;
    }

    public int getNumberOfUnits() {
        return numberOfUnits;
    }

    @Override
    public int getType() {
        return 7;
    }

    public HashMap<Integer, ArrayList<Unit>> getUnits() {
        return units;
    }

    @Override
    public int compareTo(Camp camp) {
        return numberOfUnits - camp.numberOfUnits;
    }

    public int getTypeCount(Integer type) {
        if (units.containsKey(type))
            return units.get(type).size();
        return 0;
    }

}
