package model.game.building;

import constants.BookOfConstants;
import constants.ConstantNames;
import constants.EntityNames;
import model.game.building.defense.*;
import model.game.building.village.Barracks;
import model.game.building.village.Camp;
import model.game.building.village.TownHall;
import model.game.building.village.mine.ElixirMine;
import model.game.building.village.mine.GoldMine;
import model.game.building.village.mine.Mine;
import model.game.building.village.storage.ElixirStorage;
import model.game.building.village.storage.GoldStorage;
import model.game.building.village.storage.Storage;
import model.game.entity.Entity;

import static constants.ConstantNames.*;
import static constants.EntityNames.*;

public abstract class Building extends Entity {
    public abstract void upgrade();

    public boolean isDefence() {
        return this instanceof DefenseBuilding;
    }

    public boolean isResource() {
        return this instanceof Mine || this instanceof Storage;
    }

    public boolean isStorage() {
        return this instanceof Storage;
    }

    public int getPrize() {
        return BookOfConstants.getConstant(DAMAGE_PRIZE, getType());
    }

    public int getGoldCost() {
        return BookOfConstants.getConstant(GOLD_REQUIRED, getType());
    }

    public int getElixirCost() {
        return BookOfConstants.getConstant(ELIXIR_REQUIRED, getType());
    }

    public static Building getBuilding(int type) {

        Building building = null;
        if (type == BookOfConstants.getType(TOWNHALL)) {
            building = new TownHall();
        }
        if (type == BookOfConstants.getType(BARRACKS)) {
            building = new Barracks();
        }
        if (type == BookOfConstants.getType(CAMP)) {
            building = new Camp();
        }
        if (type == BookOfConstants.getType(GOLD_STORAGE)) {
            building = new GoldStorage();
        }
        if (type == BookOfConstants.getType(ELIXIR_STORAGE)) {
            building = new ElixirStorage();
        }
        if (type == BookOfConstants.getType(GOLD_MINE)) {
            building = new GoldMine();
        }
        if (type == BookOfConstants.getType(ELIXIR_MINE)) {
            building = new ElixirMine();
        }
        if (type == BookOfConstants.getType(AIR_DEFENSE)) {
            building = new AirDefense();
        }
        if (type == BookOfConstants.getType(ARCHER_TOWER)) {
            building = new ArcherTower();
        }
        if (type == BookOfConstants.getType(CANNON)) {
            building = new Cannon();
        }
        if (type == BookOfConstants.getType(WIZARD_TOWER)) {
            building = new WizardTower();
        }
        if (type == BookOfConstants.getType(INCOMPLETE_BUILDING)) {
            building = new IncompleteBuilding();
        }
        if (type == BookOfConstants.getType(WALL))
            building = new Wall();
        if (type == BookOfConstants.getType(TRAP))
            building = new Trap();
        if (building == null)
            throw new IllegalArgumentException();
        return building;
    }

    public int upgradeCost() {
        return BookOfConstants.getConstant(ConstantNames.UPGRADE_COST, getType());
    }

    public boolean isTrap() {
        return getType() == BookOfConstants.getType(EntityNames.TRAP);
    }
}
