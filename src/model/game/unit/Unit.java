package model.game.unit;

import constants.BookOfConstants;
import javafx.scene.image.ImageView;
import model.game.BuildingCondition;
import model.game.map.*;
import model.game.entity.Entity;
import model.game.gun.Gun;
import model.game.gun.UnitGun;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import static constants.ConstantNames.*;
import static constants.EntityNames.*;
import static model.game.unit.Direction.*;

public abstract class Unit extends Entity {
    private static final int HEALTH_UPGRADE_RATE = 5;
    protected static final int DAMAGE_UPGRADE_RATE = 1;
    private static final double DELTA_T = 0.01;
    protected Position target;
    private Direction direction;
    private PointProperty pointProperty;
    private Position nextCell;
    protected Gun gun;
    private Queue<Position> path = new LinkedList<>();
    private ImageView unitImage;
    private int gifIndex;


    protected void setTarget(BuildingCondition buildingCondition) {
        target = null;
        double minDist = Double.MAX_VALUE;
        for (int i = 0; i < getMap().getWidth(); i++) {
            for (int j = 0; j < getMap().getHeight(); j++) {
                Cell candidateCell = getMap().getCell(i, j);
                if (candidateCell.hasVisibleBuilding() && buildingCondition.accept(candidateCell.getBuilding())) {
                    double distance = Position.getDistance(candidateCell, this);
                    if (distance < minDist) {
                        minDist = distance;
                        target = candidateCell.getPosition();
                    }
                }
            }
        }
    }

    @Override
    public void setPosition(Position position) {
        super.setPosition(position);
        if (pointProperty == null)
            pointProperty = new PointProperty();
        pointProperty.set(new Point(position.getX(), position.getY()));
    }

    protected void setTarget() {
        setTarget(getTargetCondition());
        if (target == null)
            setTarget(building -> true);
    }

    public void setPoint(Point point) {
        pointProperty.set(point);
    }

    public PointProperty PointProperty() {
        return pointProperty;
    }

    public Point getPoint() {
        return pointProperty.get();
    }

    protected abstract BuildingCondition getTargetCondition();

    public Unit(int level) {
        levelProperty().set(level);
        increaseMaxHealth(level * HEALTH_UPGRADE_RATE);
        gun = new UnitGun(BookOfConstants.getConstant(INITIAL_DAMAGE, getType()));
        gun.increaseDamage(level * DAMAGE_UPGRADE_RATE);
    }


    private void move() {
        if (direction != null) {
            Point newPoint;
            switch (direction) {
                case U:
                    newPoint = new Point(getPoint().getX(), getPoint().getY() - getSpeed() * DELTA_T);
                    break;
                case D:
                    newPoint = new Point(getPoint().getX(), getPoint().getY() + getSpeed() * DELTA_T);
                    break;
                case R:
                    newPoint = new Point(getPoint().getX() + getSpeed() * DELTA_T, getPoint().getY());
                    break;
                case L:
                    newPoint = new Point(getPoint().getX() - getSpeed() * DELTA_T, getPoint().getY());
                    break;
                case UR:
                    newPoint = new Point(getPoint().getX() + getSpeed() * DELTA_T / Math.sqrt(2), getPoint().getY() - getSpeed() * DELTA_T / Math.sqrt(2));
                    break;
                case UL:
                    newPoint = new Point(getPoint().getX() - getSpeed() * DELTA_T / Math.sqrt(2), getPoint().getY() - getSpeed() * DELTA_T / Math.sqrt(2));
                    break;
                case DR:
                    newPoint = new Point(getPoint().getX() + getSpeed() * DELTA_T / Math.sqrt(2), getPoint().getY() + getSpeed() * DELTA_T / Math.sqrt(2));
                    break;
                case DL:
                    newPoint = new Point(getPoint().getX() - getSpeed() * DELTA_T / Math.sqrt(2), getPoint().getY() + getSpeed() * DELTA_T / Math.sqrt(2));
                    break;
                default:
                    newPoint = null;
            }
            pointProperty.set(newPoint);
        }
    }

    private void setDirection() {
        if (nextCell.getX() == getPosition().getX()) {
            switch (nextCell.getY() - getPosition().getY()) {
                case 1:
                    direction = D;
                    return;
                case -1:
                    direction = U;
                    return;
            }
        }
        if (nextCell.getX() > getPosition().getX()) {
            switch (nextCell.getY() - getPosition().getY()) {
                case 1:
                    direction = DR;
                    return;
                case -1:
                    direction = UR;
                    return;
                case 0:
                    direction = R;
            }
        } else if (nextCell.getX() < getPosition().getX()) {
            switch (nextCell.getY() - getPosition().getY()) {
                case 1:
                    direction = DL;
                    return;
                case -1:
                    direction = UL;
                    return;
                case 0:
                    direction = L;
                    return;
            }
        }
    }


    private Queue<Position> fillPath(HashMap<Position, Position> par, Position position) {
        Queue<Position> ret;
        if (par.get(position).equals(position)) {
            ret = new LinkedList<>();
            return ret;
        }
        ret = fillPath(par, par.get(position));
        ret.add(position);
        return ret;
    }

    private boolean findPath() {
        Queue<Position> q = new LinkedList<>();
        HashMap<Position, Position> par = new HashMap<>();
        //Individual elements are not assigned in the next line as the default value of a boolean is false
        boolean mark[][] = new boolean[getMap().getWidth()][getMap().getHeight()];
        q.add(getPosition());
        mark[getPosition().getX()][getPosition().getY()] = true;
        par.put(getPosition(), getPosition());
        boolean end = false;
        while (!q.isEmpty()) {
            Position cur = q.poll();
            int dx[] = {1, 1, 1, 0, 0, -1, -1, -1}, dy[] = {1, 0, -1, 1, -1, 1, 0, -1};
            for (int i = 0; i < dx.length; i++) {
                int nextX = cur.getX() + dx[i], nextY = cur.getY() + dy[i];
                if (nextX < 0 || nextX >= getMap().getWidth() || nextY < 0 || nextY >= getMap().getHeight()) continue;
                if (mark[nextX][nextY]) continue;
                Position nextPosition = new Position(nextX, nextY);
                if (nextPosition.equals(target)) {
                    end = true;
                    mark[nextX][nextY] = true;
                    q.add(nextPosition);
                    par.put(nextPosition, cur);
                    break;
                }
                if (!isAir() && getMap().getCell(nextPosition).hasVisibleBuilding())
                    continue;
                mark[nextX][nextY] = true;
                q.add(nextPosition);
                par.put(nextPosition, cur);
            }
            if (end)
                break;
        }
        if (!mark[target.getX()][target.getY()]) return false;
        path = fillPath(par, par.get(target));
        return true;
    }

    public void nextTurn() {
            if (target == null) {
            setTarget();
            findPath();
        }
        if (isInRange()) {
            if (getMap().getCell(target).hasVisibleBuilding())
                shoot();
            else
                target = null;
        } else {
            if (nextCell == null && !path.isEmpty())
                nextCell = path.poll();
            if (pointProperty.get().isClose(nextCell)) {
                setPosition(nextCell);
                if (!path.isEmpty())
                    nextCell = path.poll();
                setDirection();
            }
            if (direction == null)
                setDirection();
            move();
        }
    }

    protected void shoot() {
        gun.shoot(getMap(), target);
    }

    private boolean isInRange() {
        return Position.getDistance(target, getPosition()) <= BookOfConstants.getConstant(ATTACK_RANGE, getType());
    }

    public boolean isGround() {
        return (getType() != 17);
    }

    public boolean isAir() {
        return (getType() == 17);
    }

    public static Unit getUnit(int type, int level) {
        Unit ret = null;
        if (type == BookOfConstants.getType(ARCHER))
            ret = new Archer(level);
        if (type == BookOfConstants.getType(GUARDIAN))
            ret = new Guardian(level);
        if (type == BookOfConstants.getType(DRAGON))
            ret = new Dragon(level);
        if (type == BookOfConstants.getType(GIANT))
            ret = new Giant(level);
        if (type == BookOfConstants.getType(WALL_BREAKER))
            return new WallBreaker(level);
        if (type == BookOfConstants.getType(HEALER))
            return new Healer(level);
        if (ret == null)
            throw new IllegalArgumentException();
        return ret;
    }

    public int getSpeed() {
        return BookOfConstants.getConstant(SPEED, getType());
    }

    public Direction getDirection() {
        return direction;
    }

    public int getGifIndex() {
        gifIndex = (gifIndex + 1) % 10;
        return gifIndex < 5 ? 0 : 1;
    }

    public void setUnitImage(ImageView unitImage) {
        this.unitImage = unitImage;
    }

    public ImageView getUnitImage() {
        return unitImage;
    }

    @Override
    public String toString() {
        return BookOfConstants.getName(getType()) + " " + getPoint().getX() + " " + getPoint().getY() + " " + getHealth() + " " + "Target: " + target.getX() + " " + target.getY() + " Position: " + getPosition();
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
