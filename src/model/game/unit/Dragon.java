package model.game.unit;

import model.game.BuildingCondition;
import model.game.map.Cell;
import model.game.building.Building;

public class Dragon extends Unit {
    public Dragon(int level) {
        super(level);
    }

    @Override
    public int getType() {
        return 17;
    }

    @Override
    protected BuildingCondition getTargetCondition() {
        return building -> true;
    }
}
