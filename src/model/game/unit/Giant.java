package model.game.unit;

import model.game.BuildingCondition;
import model.game.map.Cell;
import model.game.building.Building;

public class Giant extends Unit {

    public Giant(int level) {
        super(level);
    }

    @Override
    public int getType() {
        return 16;
    }

    @Override
    protected BuildingCondition getTargetCondition() {
        return Building::isResource;
    }


}
