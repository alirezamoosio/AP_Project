package model.game.unit;

import model.game.BuildingCondition;
import model.game.map.Cell;
import model.game.building.Building;

public class Guardian extends Unit {

    public Guardian(int level) {
        super(level);
    }

    @Override
    public int getType() {
        return 15;
    }

    @Override
    protected BuildingCondition getTargetCondition() {
        return building -> true;
    }
}
