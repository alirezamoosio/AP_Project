package model.game.unit;

import model.game.BuildingCondition;
import model.game.gun.UnitGun;
import model.game.gun.Gun;
import model.game.map.Cell;
import model.game.building.Building;

public class Archer extends Unit {
    public Archer(int level) {
        super(level);
    }


    @Override
    public int getType() {
        return 18;
    }

    @Override
    protected BuildingCondition getTargetCondition() {
        return Building::isDefence;
    }
}
