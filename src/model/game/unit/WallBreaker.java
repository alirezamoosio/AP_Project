package model.game.unit;

import constants.BookOfConstants;
import constants.EntityNames;
import model.game.BuildingCondition;
import model.game.building.defense.Wall;
import model.game.gun.Gun;
import model.game.gun.UnitGun;

import static constants.ConstantNames.INITIAL_DAMAGE;

public class WallBreaker extends Unit {
    private Gun wallBreakerGun;
    public WallBreaker(int level) {
        super(level);
        gun = new UnitGun(BookOfConstants.getConstant(INITIAL_DAMAGE,BookOfConstants.getType(EntityNames.GUARDIAN)));
        gun.increaseDamage(level * DAMAGE_UPGRADE_RATE);
        wallBreakerGun = new UnitGun(BookOfConstants.getConstant(INITIAL_DAMAGE,getType()));
        wallBreakerGun.increaseDamage(level * DAMAGE_UPGRADE_RATE);
    }

    @Override
    protected void shoot() {
        if (getMap().getCell(target).hasVisibleBuilding() && getMap().getCell(target).getBuilding() instanceof Wall) {
            wallBreakerGun.shoot(getMap(), target);
            if(!getMap().getCell(target).hasVisibleBuilding()) this.destroy();
        }
        else gun.shoot(getMap(),target);
    }

    @Override
    protected BuildingCondition getTargetCondition() {
        return building -> building instanceof Wall;
    }

    @Override
    public int getType() {
        return 19;
    }
}
