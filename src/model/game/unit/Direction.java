package model.game.unit;

import java.util.Arrays;

public enum Direction {
    U(0, "up"), R(1, "right"), D(2, "down"), L(3, "left"), UR(4, "upRight"), DR(5, "downRight"), DL(6, "downLeft"), UL(7, "upLeft");
    public static final int DIRECTIONS = 8;
    public static final int GIF_IMAGES = 2;

    private int index;
    private String sign;

    Direction(int i, String c) {
        index = i;
        sign = c;
    }

    public int getIndex() {
        return index;
    }

    public String getSign() {
        return sign;
    }

    public static Direction[] getAllDirections() {
        return new Direction[]{U, R, D, L, UR, DR, DL, UL};
    }

    public static int toInt(Direction direction) {
        if (direction == null)
            return -1;
        return Arrays.asList(getAllDirections()).indexOf(direction);
    }

    public static Direction getDirection(int d) {
        if (d == -1)
            return null;
        return getAllDirections()[d];
    }
}
