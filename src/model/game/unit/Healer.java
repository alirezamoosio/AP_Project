package model.game.unit;

import javafx.geometry.Pos;
import model.game.BuildingCondition;
import model.game.map.Cell;
import model.game.map.Position;

// TODO: 6/22/18 implement
public class Healer extends Unit {
    private static int INCREASE_HEALTH_AMOUNT = 25;
    private int timeRemaining = 10;
    public Healer(int level) {
        super(level);
    }

    @Override
    protected BuildingCondition getTargetCondition() {
        return building -> true;
    }

    @Override
    public int getType() {
        return 20;
    }

    @Override
    public void nextTurn() {
        super.nextTurn();
        timeRemaining--;
        if (timeRemaining == 0)
            destroy();
    }

    @Override
    public void damage(int amount) {
        //intentionally left blank
    }

    @Override
    protected void shoot() {
        int[] dx = new int[]{-1, 0, 1};
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int x = getPosition().getX() + dx[i], y = getPosition().getY() + dx[j];
                Position newTarget = new Position(x, y);
                if (getMap().isValidPosition(newTarget)) {
                    for (Unit unit : getMap().getCell(x, y).getUnits())
                        unit.increaseHealth(INCREASE_HEALTH_AMOUNT);
                }
            }
        }
    }

    @Override
    protected void setTarget() {
        double minDistance = Double.MAX_VALUE;
        for (int i = 0; i < getMap().getWidth(); i++) {
            for (int j = 0; j < getMap().getHeight(); j++) {
                Cell cell = getMap().getCell(i, j);
                boolean hasWoundedUnit = false;
                for (Unit unit : cell.getUnits()) {
                    if (unit.getHealth() != unit.getMaxHealth()) {
                        hasWoundedUnit = true;
                        break;
                    }
                }
                if (hasWoundedUnit) {
                    double distance = Position.getDistance(cell.getPosition(), getPosition());
                    if (distance < minDistance) {
                        minDistance = distance;
                        target = cell.getPosition();
                    }
                }
            }
        }

        if (target == null)
            super.setTarget();
    }
}
