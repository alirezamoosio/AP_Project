package model.game.ministry;

import constants.BookOfConstants;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import model.game.map.Position;
import model.game.map.Map;
import model.exceptions.InvalidCoordinatesException;
import model.exceptions.OutOfWorkerException;
import model.game.building.Building;
import model.game.building.IncompleteBuilding;

import java.util.ArrayList;
import java.util.HashMap;

import static constants.ConstantNames.TIME_REQUIRED;
import static constants.EntityNames.INCOMPLETE_BUILDING;

public class Civil {
    private static Civil ourInstance;
    private Map map;
    private ArrayList<IncompleteBuilding> buildingList = new ArrayList<>();
    private HashMap<Building, Integer> upgradeList = new HashMap<>();
    private ArrayList<IncompleteBuilding> toRemove;
    private IntegerProperty freeWorker = new SimpleIntegerProperty(0);

    public static void setInstance(Map map) {
        ourInstance = new Civil(map);
    }

    public static void setInstance(Civil civil) {
        ourInstance = civil;
    }

    public static Civil getInstance() {
        return ourInstance;
    }

    private Civil(Map map) {
        this.map = map;
        addWorker();
    }

    public void work() {
        workBuildingList();
        workUpgradeList();
    }

    public IncompleteBuilding build(Position position, int type) throws OutOfWorkerException, InvalidCoordinatesException {
        if (getFreeWorker() == 0)
            throw new OutOfWorkerException();
        if (map.getCell(position).hasBuilding())
            throw new InvalidCoordinatesException(position.getX(), position.getY(), "Cell Full");
        IncompleteBuilding building = (IncompleteBuilding) map.newBuilding(position, INCOMPLETE_BUILDING);
        building.setFutureType(type);
        buildingList.add(building);
        setFreeWorker(getFreeWorker() - 1);
        building.isDestroyedProperty().addListener((observable, oldValue, newValue) -> {
            //Safety check to ensure building is destroyed
            if (newValue) {
                //When a building is done we will add it to our kill list and destroy at the end of the round
                //It is automatically created because of the methods in IncompleteBuilding class
                //building is not removed immediately to prevent concurrent modification exception
                toRemove.add(building);
                setFreeWorker(getFreeWorker() + 1);
            }
        });
        return building;
    }

    private void workBuildingList() {
        toRemove = new ArrayList<>();
        for (IncompleteBuilding building : buildingList) {
            building.work();
        }
        buildingList.removeAll(toRemove);
    }


    public void upgrade(Building building) {
        int upgradeTime = BookOfConstants.getConstant(TIME_REQUIRED, building.getType());
        upgradeList.put(building, upgradeTime);
    }

    private void workUpgradeList() {
        ArrayList<Building> upgradeListKeySet = new ArrayList<>(upgradeList.keySet());
        for (Building building : upgradeListKeySet) {
            int timeRemaining = upgradeList.get(building);
            timeRemaining--;
            if (timeRemaining > 0) {
                upgradeList.replace(building, timeRemaining);
                return;
            }
            upgradeList.remove(building);
            building.upgrade();
        }
    }

    public void addWorker() {
        setFreeWorker(getFreeWorker() + 1);
    }

    public boolean hasAvailableWorker() {
        return getFreeWorker() > 0;
    }

    public HashMap<Building, Integer> getUpgradeList() {
        return upgradeList;
    }

    public ArrayList<IncompleteBuilding> getBuildingList() {
        return buildingList;
    }

    private int getFreeWorker() {
        return freeWorker.get();
    }

    public IntegerProperty freeWorkerProperty() {
        return freeWorker;
    }

    private void setFreeWorker(int freeWorker) {
        this.freeWorker.set(freeWorker);
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }
}
