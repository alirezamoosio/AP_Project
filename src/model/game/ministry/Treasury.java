package model.game.ministry;

import constants.BookOfConstants;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import model.exceptions.StorageEmptyException;
import model.exceptions.StorageFullException;
import model.game.building.Building;
import model.game.building.village.storage.Storage;

import java.util.ArrayList;
import java.util.Collections;

import static constants.EntityNames.ELIXIR_STORAGE;
import static constants.EntityNames.GOLD_STORAGE;

public class Treasury {
    private static Treasury ourInstance;

    private ArrayList<Building> elixirStorage;
    private ArrayList<Building> goldStorage;
    private IntegerProperty goldAmountProperty = new SimpleIntegerProperty(0);
    private IntegerProperty elixirAmountProperty = new SimpleIntegerProperty(0);
    private IntegerProperty goldGainedProperty = new SimpleIntegerProperty(0);
    private IntegerProperty elixirGainedProperty = new SimpleIntegerProperty(0);

    public static Treasury getInstance() {
        return ourInstance;
    }

    public static void setInstance(ArrayList<Building> elixirStorage, ArrayList<Building> goldStorage) {
        ourInstance = new Treasury(elixirStorage, goldStorage);
    }

    public static void setInstance(Treasury treasury) {
        ourInstance = treasury;
    }

    private Treasury(ArrayList<Building> elixirStorage, ArrayList<Building> goldStorage) {
        this.elixirStorage = elixirStorage;
        this.goldStorage = goldStorage;
    }

    private ArrayList<Building> getStorage(int type) {
        if (type == BookOfConstants.getType(GOLD_STORAGE))
            return goldStorage;
        else
            return elixirStorage;
    }

    public int getGoldAmount() {
        return getResourceAmount(BookOfConstants.getType(GOLD_STORAGE));
    }

    public IntegerProperty getGoldAmountProperty() {
        return goldAmountProperty;
    }

    public IntegerProperty getElixirAmountProperty() {
        return elixirAmountProperty;
    }

    public int getElixirAmount() {
        return getResourceAmount(BookOfConstants.getType(ELIXIR_STORAGE));
    }

    public int getGoldCapacity() {
        return getResourceCapacity(BookOfConstants.getType(GOLD_STORAGE));
    }

    public int getElixirCapacity() {
        return getResourceCapacity(BookOfConstants.getType(ELIXIR_STORAGE));
    }

    private int getResourceCapacity(int storageType) {
        int capacity = 0;
        for (Building sotrage : getStorage(storageType)) {
            capacity += ((Storage) sotrage).getCapacity();
        }
        return capacity;
    }

    private int getResourceAmount(int storageType) {
        IntegerProperty property = storageType == BookOfConstants.getType(GOLD_STORAGE) ? goldAmountProperty : elixirAmountProperty;
        return property.get();
    }

    private void getFromStorage(int amount, int storageType) throws StorageEmptyException {
        ArrayList<Building> buildingStorages = getStorage(storageType);
        ArrayList<Storage> storages = new ArrayList<>();
        for (Building storage : buildingStorages) {
            storages.add((Storage) storage);
        }
        if (storages.isEmpty())
            throw new StorageEmptyException(amount);
        if (amount > getResourceAmount(storageType))
            throw new StorageEmptyException(amount - getResourceAmount(storageType));
        storages.sort(Collections.reverseOrder()); // sorts from fullest to emptiest
        for (Storage storage : storages) {
            try {
                storage.withdraw(amount);
                amount = 0;
            } catch (StorageEmptyException e) {
                storage.emptyStorage();
                amount = e.getOverFlowAmount();
            }
            if (amount == 0)
                break;
        }
    }

    public void extract(int goldAmount, int elixirAmount) throws StorageEmptyException {
        extractGold(goldAmount);
        extractElixir(elixirAmount);
    }

    public void extractGold(int amount) throws StorageEmptyException {
        getFromStorage(amount, BookOfConstants.getType(GOLD_STORAGE));
        goldAmountProperty.setValue(goldAmountProperty.get() - amount);
    }

    public void extractElixir(int amount) throws StorageEmptyException {
        getFromStorage(amount, BookOfConstants.getType(ELIXIR_STORAGE));
        elixirAmountProperty.setValue(elixirAmountProperty.get() - amount);
    }

    public void addToStorage(int amountMined, int type) {
        int initialValue = amountMined;
        IntegerProperty property = type == BookOfConstants.getType(GOLD_STORAGE) ? goldAmountProperty : elixirAmountProperty;
        ArrayList<Building> buildingStorages = getStorage(type);
        ArrayList<Storage> storages = new ArrayList<>();
        for (Building storage : buildingStorages) {
            storages.add((Storage) storage);
        }
        Collections.sort(storages); // sorts from emptiest to fullest
        for (Storage storage : storages) {
            try {
                storage.add(amountMined);
                amountMined = 0;
            } catch (StorageFullException e) {
                amountMined = e.getOverFlowAmount();
                storage.fillStorage();
            }
            if (amountMined == 0)
                break;
        }
            property.setValue(property.get() + initialValue - amountMined);
    }

    public boolean hasResources(int goldRequired, int elixirRequired) {
        return goldRequired <= getGoldAmount() && elixirRequired <= getElixirAmount();
    }

    public void addGainedResource(int amount, int type) {
        IntegerProperty gained = type == BookOfConstants.getType(GOLD_STORAGE) ? goldGainedProperty : elixirGainedProperty;
        gained.setValue(gained.get() + amount);
    }

    public IntegerProperty getGoldGainedProperty() {
        return goldGainedProperty;
    }

    public IntegerProperty getElixirGainedProperty() {
        return elixirGainedProperty;
    }

    public void resetGainedResource() {
        goldGainedProperty.setValue(0);
        elixirGainedProperty.setValue(0);
    }

    public boolean isGoldStoragesFull() {
        for (Building storage : goldStorage) {
            if (!((Storage) storage).isFull())
                return false;
        }
        return true;
    }

    public boolean isElixirStoragesFull() {
        for (Building storage : elixirStorage) {
            if (!((Storage) storage).isFull())
                return false;
        }
        return true;
    }
}
