package model.game.ministry;

import constants.BookOfConstants;
import constants.ConstantNames;
import javafx.beans.binding.NumberBinding;
import model.exceptions.CampFullException;
import model.exceptions.NotEnoughMoneyException;
import model.exceptions.NotEnoughUnitException;
import model.exceptions.StorageEmptyException;
import model.game.building.Building;
import model.game.building.village.Barracks;
import model.game.building.village.Camp;
import model.game.unit.Unit;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;

public class War {
    private static War ourInstance;

    public static War getInstance() {
        return ourInstance;
    }

    private ArrayList<Building> barracksBuildings;
    private ArrayList<Building> camps;

    public static void setInstance(ArrayList<Building> barracksBuildings, ArrayList<Building> camps) {
        ourInstance = new War(barracksBuildings, camps);
    }

    public static void setInstance(War war) {
        ourInstance = war;
    }

    private War(ArrayList<Building> barracksBuildings, ArrayList<Building> camps) {
        this.barracksBuildings = barracksBuildings;
        this.camps = camps;
    }

    public void createUnit(int barracksNumber, int unitType, int number) throws NotEnoughMoneyException, StorageEmptyException {
        int elixirRequired = BookOfConstants.getConstant(ConstantNames.ELIXIR_REQUIRED, unitType) * number;
        if (Treasury.getInstance().getElixirAmount() < elixirRequired)
            throw new NotEnoughMoneyException();
        Treasury.getInstance().extractElixir(elixirRequired);
        ((Barracks) (barracksBuildings.get(barracksNumber))).createUnit(unitType, number);
    }

    public void work() {
        for (Building building : barracksBuildings) {
            Barracks barracks = (Barracks) building;
            barracks.work();
            barracksToCamp(barracks);
        }
    }

    private void barracksToCamp(Barracks barracks) {
        while (!barracks.getDoneList().isEmpty()) {
            try {
                Camp camp = findEmptyCamp();
                Unit unit = Unit.getUnit(barracks.pollDoneList(), barracks.getLevel());
                camp.addUnit(unit);
            } catch (CampFullException e) {
                return;
            }
        }
    }

    public void unitsToCamp(ArrayList<Unit> units) {
        for (Unit unit : units) {
            try {
                Camp camp = findEmptyCamp();
                camp.addUnit(unit);
            } catch (CampFullException ignored) {
            }
        }
    }


    private Camp findEmptyCamp() throws CampFullException {
        ArrayList<Camp> camps = new ArrayList<>();
        for (Building camp : this.camps) {
            camps.add((Camp) camp);
        }
        Collections.sort(camps);
        for (Camp camp : camps) {
            if (camp.hasSpace())
                return camp;
        }
        throw new CampFullException();
    }

    public int getNumberOfUnits() {
        int totalCount = 0;
        for (Building camp : camps) {
            totalCount += ((Camp) camp).getNumberOfUnits();
        }
        return totalCount;
    }

    public int getNumberOfUnits(int type) {
        int numberOfUnits = 0;
        for (Building camp : camps) {
            numberOfUnits += ((Camp) camp).getTypeCount(type);
        }
        return numberOfUnits;
    }

    public int getCapacity() {
        int capacity = 0;
        for (Building camp : camps) {
            capacity += ((Camp) camp).getCapacity();
        }
        return capacity;
    }

    public ArrayList<Unit> extractUnit(String unitName, int count) throws NotEnoughUnitException {
        //check if we can
        int available = 0;
        for (Building building : camps) {
            available += ((Camp) building).getTypeCount(BookOfConstants.getType(unitName));
            if (available >= count)
                break;
        }
        if (available < count)
            throw new NotEnoughUnitException();

        ArrayList<Unit> extracted = new ArrayList<>();
        for (Building building : camps) {
            int type = BookOfConstants.getType(unitName);
            Camp camp = (Camp) building;
            int diff = Math.min(count, camp.getTypeCount(type));
            count -= diff;
            extracted.addAll(camp.extract(type, diff));
        }
        return extracted;
    }
}
