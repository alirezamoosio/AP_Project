package model.game;

import model.game.unit.Unit;

@FunctionalInterface
public interface UnitCondition {
    boolean accept(Unit unit);
}
