package model.game.gun;

import model.game.building.defense.Cannon;
import model.game.building.defense.DefenseBuilding;
import model.game.map.Position;
import model.game.UnitCondition;
import model.game.map.Map;
import model.game.unit.Unit;

public class LittleGun implements Gun {
    private UnitCondition unitCondition;
    private int damage;

    public LittleGun(UnitCondition unitCondition, int damage) {
        this.unitCondition = unitCondition;
        this.damage = damage;
    }

    @Override
    public void shoot(Map map, Position target) {
        if (target == null || !map.isValidPosition(target))
            return;
        Unit[] units = new Unit[map.getCell(target).getUnits().size()];
        for (int i = 0; i < units.length; i++) {
            units[i] = map.getCell(target).getUnits().get(i);
        }
        for (Unit unit : units) {
            if (unitCondition == null)
                unit.damage(damage);
            else if (unitCondition.accept(unit))
                unit.damage(damage);
        }
    }

    @Override
    public void increaseDamage(int amount) {
        damage += amount;
    }
}
