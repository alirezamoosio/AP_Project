package model.game.gun;

import model.game.UnitCondition;
import model.game.building.defense.Trap;
import model.game.map.Map;
import model.game.map.Position;
import model.game.unit.Unit;

public class TrapGun implements Gun {
    private UnitCondition unitCondition;
    private Trap trap;

    public TrapGun(UnitCondition unitCondition, Trap trap) {
        this.unitCondition = unitCondition;
        this.trap = trap;
    }

    @Override
    public void shoot(Map map, Position target) {
        if (target == null || !map.isValidPosition(target))
            return;
        if (!map.getCell(target).getUnits().isEmpty()) {
            Unit unit = map.getCell(target).getUnits().get(0);
            if (unitCondition.accept(unit)) {
                unit.destroy();
                trap.destroy();
            }
        }
    }

    @Override
    public void increaseDamage(int amount) {

    }
}
