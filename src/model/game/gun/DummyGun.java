package model.game.gun;

import model.game.map.Map;
import model.game.map.Position;

public class DummyGun implements Gun {
    /*Does nothing. Defense buildings like Wall that don't shoot can use this.
    It is basically a hack to keep design unchanged;
     */
    @Override
    public void shoot(Map map, Position target) {

    }

    @Override
    public void increaseDamage(int amount) {

    }
}
