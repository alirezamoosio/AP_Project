package model.game.gun;

import model.game.map.Position;
import model.game.map.Map;

public interface Gun {

    void shoot(Map map, Position target);

    void increaseDamage(int amount);
}
