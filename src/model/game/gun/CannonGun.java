package model.game.gun;

import model.game.UnitCondition;
import model.game.map.Map;
import model.game.map.Position;

public class CannonGun implements Gun {
    private LittleGun littleGun;

    public CannonGun(UnitCondition unitCondition, int damage) {
        littleGun = new LittleGun(unitCondition, damage);
    }

    @Override
    public void shoot(Map map, Position target) {
        if (target == null) return;
        int[] dx = new int[]{-1, 0, 1};
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int x = target.getX() + dx[i], y = target.getY() + dx[j];
                Position newTarget = new Position(x, y);
                if (map.isValidPosition(newTarget))
                    littleGun.shoot(map, newTarget);
            }
        }
    }

    @Override
    public void increaseDamage(int amount) {
        littleGun.increaseDamage(amount);
    }
}
