package model.game.gun;

import model.game.map.Position;
import model.game.map.Cell;
import model.game.map.Map;

public class UnitGun implements Gun {
    private int damage;

    public UnitGun(int damage) {
        this.damage = damage;
    }

    @Override
    public void shoot(Map map, Position target) {
        Cell targetCell = map.getCell(target);
        if (targetCell.hasVisibleBuilding())
            targetCell.getBuilding().damage(damage);
    }

    @Override
    public void increaseDamage(int amount) {
        damage += amount;
    }
}
