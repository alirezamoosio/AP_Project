package constants;

import model.exceptions.InvalidClassTypeException;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.HashMap;
import java.util.Scanner;

import static constants.ConstantNames.INITIAL_ELIXIR;
import static constants.ConstantNames.INITIAL_GOLD;

public class BookOfConstants {
    //Gold Required 1
    private static HashMap<String, Integer> theBook = new HashMap<>();
    private static HashMap<Integer, String> names = new HashMap<>();

    public static void load() {
        File configFile = new File("src/constants/config_constants.txt");
        File entityNames = new File("src/constants/entity_names.txt");
        try {
            Scanner configScanner = new Scanner(configFile), entityNameScanner = new Scanner(entityNames);
            addToMapFromScanner(configScanner);
            addToMapFromScanner(entityNameScanner);
            entityNameScanner.close();
            entityNameScanner = new Scanner(entityNames);
            addToNamesFromScanner(entityNameScanner);
            configScanner.close();
            // hardcode part
            theBook.put(INITIAL_GOLD, 0);
            theBook.put(INITIAL_ELIXIR, 0);
            // end of hardcode part

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void addToMapFromScanner(Scanner scanner) {
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] words = line.split(" ");
            if (words.length == 3)
                theBook.put(words[0] + " " + words[1], Integer.parseInt(words[2]));
            else if (words.length == 2) {
                theBook.put(words[0], Integer.parseInt(words[1]));
            }
        }
    }

    private static void addToNamesFromScanner(Scanner scanner) {
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] words = line.split(" ");
            if (words.length == 3)
                names.put(Integer.parseInt(words[2]), words[0] + " " + words[1]);
            else if (words.length == 2)
                names.put(Integer.parseInt(words[1]), words[0]);
        }
    }

    public static int getConstant(String constantKind, int type) {
        return getConstant(constantKind + " " + type);
    }

    public static int getConstant(String constantKind, String buildingName) {
        return getConstant(constantKind + " " + getType(buildingName));
    }

    public static int getType(String className) throws InvalidClassTypeException {
        Integer type = theBook.get(className);
        if (type == null)
            throw new InvalidClassTypeException();
        return type;
    }

    public static int getConstant(String string) {
        try {
            return theBook.get(string);
        } catch (NullPointerException e) {
            System.out.println(string);
        }
        return theBook.get(string);
    }

    public static String getName(int type) {
        return names.get(type);
    }

    public static URL getURL(String name) {
        return BookOfConstants.class.getResource(name);
    }
}
