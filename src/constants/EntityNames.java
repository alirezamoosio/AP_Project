package constants;

public class EntityNames {
    public static final String GOLD_MINE = "Gold Mine";
    public static final String ELIXIR_MINE = "Elixir Mine";
    public static final String GOLD_STORAGE = "Gold Storage";
    public static final String ELIXIR_STORAGE = "Elixir Storage";
    public static final String TOWNHALL = "Town Hall";
    public static final String BARRACKS = "Barracks";
    public static final String CAMP = "Camp";
    public static final String AIR_DEFENSE = "Air Defense";
    public static final String ARCHER_TOWER = "Archer Tower";
    public static final String CANNON = "Cannon";
    public static final String WIZARD_TOWER = "Wizard Tower";
    public static final String INCOMPLETE_BUILDING = "Incomplete Building";
    public static final String WALL = "Wall";
    public static final String TRAP = "Trap";
    public static final String GUARDIAN = "Guardian";
    public static final String DRAGON = "Dragon";
    public static final String GIANT = "Giant";
    public static final String ARCHER = "Archer";
    public static final String WALL_BREAKER  = "WallBreaker";
    public static final String HEALER = "Healer";
}
