package constants;

import javafx.scene.image.Image;
import model.game.entity.Entity;
import model.game.map.Cell;
import model.game.building.Building;
import model.game.unit.Direction;
import model.game.unit.Unit;

public class ResourceConstants {
    public static final String IMAGE_BUTTON = "../view/resources/images/menus/button.png";
    public static final String IMAGE_LABEL = "../view/resources/images/menus/label.png";
    public static final String IMAGE_GOLD_ICON = "../view/resources/images/menus/gold_icon.png";
    public static final String IMAGE_ELIXIR_ICON = "../view/resources/images/menus/elixir_icon.png";
    public static final String IMAGE_EXIT_CLOSED = "../view/resources/images/menus/exit.png";
    public static final String IMAGE_EXIT_OPENED = "../view/resources/images/menus/exit_hover.png";
    public static final String IMAGE_SAVE = "../view/resources/images/menus/save.png";
    public static final String IMAGE_HOME = "../view/resources/images/menus/home.png";
    public static final String IMAGE_BACKGROUND = "../view/resources/images/start/background.png";
    public static final String IMAGE_LOADING = "../view/resources/images/start/loading.png";
    public static final String IMAGE_BUILT = "../view/resources/images/buildings/Builders_Hut.png";
    public static final String SONG_THEME = "../view/resources/songs/A_Lannister_Always_Pays_His_Debts.mp3";
    public static final String FONT_ADVENTURE = "../view/resources/fonts/Adventure.otf";
    public static final String BUILDING_IMAGE_PATH = "../view/resources/images/buildings/";
    // TODO: 6/8/18 replace image_empty url with and actuall empty image
    public static final String IMAGE_EMPTY = "../view/resources/images/buildings/White_Flag.png";
    public static final String IMAGE_SEARCH = "../view/resources/images/map/search.png";
    public static final String IMAGE_UNFOCUSED = "../view/resources/images/map/unfocused.png";
    public static final String IMAGE_FOCUSED = "../view/resources/images/map/focused.png";
    public static final String IMAGE_BORDER = "../view/resources/images/map/border.png";

    public static String getBuildingIcon(Building building) {
        if (building != null) {
            int imageNo = building.getLevel() == 0 ? 1 : 2;
            System.out.println(building.getLevel());
            return BUILDING_IMAGE_PATH + building.getName().replace(" ", "_") + imageNo +
                    ".png";
        } else {
            return IMAGE_EMPTY;
        }
    }

    public static String getUnitIcon(int type) {
        return "../view/resources/images/menus/" + BookOfConstants.getName(type).toLowerCase() + "_icon.png";
    }

    public static String getEntityIcon(Entity entity) {
        if (entity instanceof Building)
            return getBuildingIcon((Building) entity);
        if (entity instanceof Unit)
            return getUnitIcon(entity.getType());
        return "";
    }

    public static void setUnitImages(Image[][][] unitImages, double cellWidth, double cellHeight) {
        for (int i = 0; i < unitImages.length; i++) {
            String unitName = BookOfConstants.getName(i + ConstantNames.getAllSoldierTypes()[0]);
            for (Direction direction : Direction.getAllDirections()) {
                // TODO: 6/29/18 complete resources with unit pics
                try {
                    String unitPath = "../view/resources/images/units/" + unitName.toLowerCase() + "/" + direction.getSign() + "/" + unitName;
                    unitImages[i][direction.getIndex()][0] = new Image(ResourceConstants.class.getResource(unitPath + "_0" + direction.getSign() + ".png").toExternalForm()
                            , cellWidth, cellHeight, false, true);
                    unitImages[i][direction.getIndex()][1] = new Image(ResourceConstants.class.getResource(unitPath + "_1" + direction.getSign() + ".png").toExternalForm()
                            , cellWidth, cellHeight, false, true);
                } catch (NullPointerException ignored) {
                }
            }
        }

    }
}
