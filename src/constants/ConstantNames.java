package constants;

import java.util.ArrayList;

public class ConstantNames {
    public static final String INITIAL_GOLD = "Initial Gold";
    public static final String INITIAL_ELIXIR = "Initial Elixir";
    public static final String GOLD_REQUIRED = "Gold_Required";
    public static final String ELIXIR_REQUIRED = "Elixir_Required";
    public static final String TIME_REQUIRED = "Time_Required";
    public static final String INITIAL_CAPACITY = "Initial_Rate";
    public static final String INITIAL_RATE = "Initial_Rate";
    public static final String UPGRADE_COST = "Upgrade_Cost";
    public static final String INITIAL_HEALTH = "Initial_Health";
    public static final String DAMAGE_PRIZE = "Damage_Prize";
    public static final String INITIAL_DAMAGE = "Initial_Damage";
    public static final String ATTACK_RANGE = "Attack_Range";
    public static final String SPEED = "Speed";

    public static Integer[] getAllBuildingTypes() {
        ArrayList<Integer> allTypes = new ArrayList<>();
        int numberOfBuildings = 13;
        for (int i = 1; i <= numberOfBuildings; i++)
            allTypes.add(i);
        allTypes.add(1378);
        return allTypes.toArray(new Integer[0]);
    }

    public static int[] getAllSoldierTypes() {
        int numberOfTypes = 5;
        int[] allSoldierTypes = new int[numberOfTypes];
        int firstSoldierType = 15;
        for (int i = 0; i < numberOfTypes; i++) {
                allSoldierTypes[i] = i + firstSoldierType;

        }
        return allSoldierTypes;
    }

    public static boolean isDefenseType(int type) {
        return type >= 8 && type <= 13;
    }
}
