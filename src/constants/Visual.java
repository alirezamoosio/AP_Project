package constants;

import javafx.stage.Screen;

public class Visual {
    // TODO: 6/4/18 Don't hardcode maybe:?
    public static final double START_WIDTH = 640;
    public static final double START_HEIGHT = 371;
    public static final double ATTACK_WIDTH = 1000;
    public static final double ATTACK_HEIGHT = 600;
    public static final double MULTI_WIDTH = 600;
    public static final double MULTI_HEIGHT = 400;
    public static final double LOADING_X = START_WIDTH / 2 - 32;
    public static final double LOADING_Y = START_HEIGHT / 2- 32;
    public static final double GAME_WIDTH = Screen.getPrimary().getBounds().getWidth();
    public static final double GAME_HEIGHT = Screen.getPrimary().getBounds().getHeight() - 40;
    public static final double SIDE_MENU_WIDTH = 0.3 * GAME_WIDTH;
    public static final double MAP_WIDTH = GAME_WIDTH - SIDE_MENU_WIDTH;
    public static final double MAP_HEIGHT = GAME_HEIGHT;
    public static final double START_MENU_X = START_WIDTH / 10;
    public static final double START_MENU_Y = START_HEIGHT / 2;
    public static final double SETTINGS_MENU_X = START_WIDTH / 20;
    public static final double SETTINGS_MENU_Y = START_HEIGHT * 2 / 3;
    public static final double SIDE_BUTTON_X = SIDE_MENU_WIDTH / 2 - 96;
    public static final double SIDE_BUTTON_Y = GAME_HEIGHT / 2 - 180;
    public static final double ATTACK_MAIN_X = SIDE_MENU_WIDTH / 2 - 140;
    public static final double ATTACK_MAIN_Y = GAME_HEIGHT / 2 - 280;
    public static final double STATUS_BAR_SPACING = 50;
    public static final double STATUS_BAR_X = SIDE_MENU_WIDTH / 2 - STATUS_BAR_SPACING - 150;
    public static final double STATUS_BAR_Y = 40 * GAME_HEIGHT / 1080;
    public static final double DOWN_BAR_SPACING = 50;
    public static final double DOWN_BAR_X = SIDE_MENU_WIDTH / 2 - DOWN_BAR_SPACING - 64 - 32;
    public static final double DOWN_BAR_Y = GAME_HEIGHT - 100;
    public static final double NEWGAME_MENU_X = START_WIDTH / 20;
    public static final double NEWGAME_MENU_Y = 0.7 * START_HEIGHT;
}
