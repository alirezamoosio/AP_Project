package constants;

public class CSSConstants {
    public static final String MENUS = "../view/css/menus.css";
    public static final String START = "../view/css/start.css";
    public static final String ATTACK = "../view/css/attack.css";
    public static final String MULTI = "../view/css/multi.css";
    public static final String CHATROOM = "../view/css/chatroom.css";
}
