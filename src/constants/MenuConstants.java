package constants;

public class MenuConstants {
    public static final String SHOW_BUILDINGS = "show buildings";
    public static final String SHOW_RESOURCES = "show resources";
    public static final String AVAILABLE_BUILDINGS = "available buildings";
    public static final String INFO = "info";
    public static final String STATUS = "status";
    public static final String OVERALL = "overall info";
    public static final String UPGRADE = "upgrade info";
    public static final String BACK = "back";
    public static final String BUILD_SOLDIER = "build soldiers";
    public static final String SOLDIERS = "soldiers";
    public static final String CAPACITY_INFO = "capacity info";
    public static final String SOURCES = "sources";
}
