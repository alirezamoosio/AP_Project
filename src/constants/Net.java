package constants;

public class Net {
    public static final String GUEST_DEFAULT_IP = "LOCALHOST";
    public static final int HOST_DEFAULT_PORT = 8050;
    public static final int GUEST_DEFAULT_PORT = 8060;
    public static final int ATTACKER_DEFAULT_PORT = 8071;
    public static final int DEFENDER_DEFAULT_PORT = 8070;
    public static final int SIZE_OF_INT = 8;
    public static final int SIZE_OF_DOUBLE = 8;
}