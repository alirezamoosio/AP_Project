package network.udp;

import model.game.map.Map;

import java.io.IOException;
import java.net.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class MapSender implements Runnable {
    private static final int CAPACITY = 70;
    private Queue<List<EntityMessage>> maps = new LinkedList<>();
    private InetAddress address;
    private int port;
    private DatagramSocket socket;

    public MapSender(InetAddress address, int port) {
        this.address = address;
        this.port = port;
        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public synchronized void addMap(List<EntityMessage> map) {
        while (maps.size() == CAPACITY) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        maps.add(map);
        notify();
    }

    @Override
    public void run() {
        while (true) {
            List<EntityMessage> map = getNewMap();
            for (EntityMessage message : map) {
                byte[] buf = message.toByte();
                DatagramPacket packet = new DatagramPacket(buf, buf.length, address, port);
                try {
                    socket.send(packet);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public synchronized List<EntityMessage> getNewMap() {
        while (maps.size() == 0) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.err.println("packet sent");
        List<EntityMessage> map = maps.poll();
        notify();
        return map;
    }
}
