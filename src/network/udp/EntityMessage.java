package network.udp;

import model.game.entity.Entity;
import model.game.map.Point;
import model.game.map.Position;
import model.game.unit.Direction;
import model.game.unit.Unit;

public class EntityMessage {
    private static final int END_CODE = 1123;

    public EntityMessage(byte[] bytes) {
        new MessageReader(bytes).readMessage(this);
    }

    public EntityMessage(Entity entity) {
        entityType = entity.getType();
        entityID = entity.getID();
        newHealth = entity.getHealth();
        position = entity.getPosition();
        level = entity.getLevel();
        if (entity instanceof Unit) {
            point = ((Unit) entity).getPoint();
            direction = Direction.toInt(((Unit) entity).getDirection());
        }
        else {
            point = new Point(-1, -1);
            direction = -1;
        }
    }

    public static EntityMessage endMessage() {
        EntityMessage message = new EntityMessage();
        message.setEntityType(END_CODE);
        return message;
    }

    private EntityMessage() {
    }

    private int entityType;
    private int entityID;
    private int newHealth;
    private int level;
    private Point point;
    private Position position;
    private int direction;

    public int getEntityType() {
        return entityType;
    }

    public void setEntityType(int entityType) {
        this.entityType = entityType;
    }

    public int getEntityID() {
        return entityID;
    }

    public void setEntityID(int entityID) {
        this.entityID = entityID;
    }

    public int getNewHealth() {
        return newHealth;
    }

    public void setNewHealth(int newHealth) {
        this.newHealth = newHealth;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public byte[] toByte() {
        return new MessageWriter(this).getBuf();
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isEnd() {
        return getEntityID() == END_CODE;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }
}


