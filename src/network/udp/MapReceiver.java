package network.udp;

import model.game.entity.Entity;
import model.game.map.Map;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.Queue;

public class MapReceiver implements Runnable {
    private static int CAPACITY = 10000;
    private DatagramSocket socket;
    private byte[] buf = new byte[256];
    private Queue<EntityMessage> messages = new LinkedList<>();
    public MapReceiver(int port) throws SocketException {
        socket = new DatagramSocket(port);
    }

    @Override
    public void run() {
        while (true) {
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            try {
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.err.println("packet recieved");
            EntityMessage message = new EntityMessage(packet.getData());
            addMessage(message);
        }
    }

    public synchronized EntityMessage getNewMessage() {
        while (messages.size() == 0) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        EntityMessage message = messages.poll();
        notify();
        return message;
    }

    private synchronized void addMessage(EntityMessage message) {
        while (messages.size() == CAPACITY) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        messages.add(message);
        notify();
    }

    public boolean hasMessage() {
        return !messages.isEmpty();
    }
}
