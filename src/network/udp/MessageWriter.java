package network.udp;

import constants.Net;

import java.nio.ByteBuffer;

public class MessageWriter {
    private byte[] buf;
    private int index;

    public MessageWriter(EntityMessage message) {
        writeMessage(message);
    }

    public void writeMessage(EntityMessage message) {
        buf = new byte[256];
        index = 0;
        writeInt(message.getEntityType());
        writeInt(message.getEntityID());
        writeInt(message.getNewHealth());
        writeInt(message.getLevel());

        writeInt(message.getPosition().getX());
        writeInt(message.getPosition().getY());
        writeDouble(message.getPoint().getX());
        writeDouble(message.getPoint().getY());
        writeInt(message.getDirection());
    }

    private void writeInt(int value) {
        ByteBuffer.wrap(buf, index, Net.SIZE_OF_INT).putInt(value);
        index += Net.SIZE_OF_INT;
    }

    private void writeDouble(double value) {
        ByteBuffer.wrap(buf, index, Net.SIZE_OF_DOUBLE).putDouble(value);
        index += Net.SIZE_OF_DOUBLE;
    }

    public byte[] getBuf() {
        return buf;
    }
}
