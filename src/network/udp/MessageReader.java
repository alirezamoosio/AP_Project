package network.udp;

import constants.Net;
import model.game.map.Point;
import model.game.map.Position;

import java.nio.ByteBuffer;

public class MessageReader {
    private byte[] buf;
    private int index;

    public MessageReader(byte[] buf) {
        this.buf = buf;
        index = 0;
    }

    public void readMessage(EntityMessage message) {
        message.setEntityType(readInt());
        message.setEntityID(readInt());
        message.setNewHealth(readInt());
        message.setLevel(readInt());

        int xPosition = readInt();
        int yPosition = readInt();
        message.setPosition(new Position(xPosition, yPosition));

        double xPoint = readDouble();
        double yPoint = readDouble();
        message.setPoint(new Point(xPoint, yPoint));

        message.setDirection(readInt());
    }

    private int readInt() {
        int n = ByteBuffer.wrap(buf, index, Net.SIZE_OF_INT).getInt();
        index += Net.SIZE_OF_INT;
        return n;
    }

    private double readDouble() {
        double d =  ByteBuffer.wrap(buf, index, Net.SIZE_OF_DOUBLE).getDouble();
        index += Net.SIZE_OF_DOUBLE;
        return d;
    }

}
