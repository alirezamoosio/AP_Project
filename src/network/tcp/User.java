package network.tcp;

import constants.BookOfConstants;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;
import java.util.Formatter;
import java.util.Scanner;

import static constants.EntityNames.GOLD_STORAGE;

public class User implements Serializable {
    private StringProperty userName = new SimpleStringProperty("");
    private Socket socket;
    private Formatter formatter;
    private Scanner scanner;
    private IntegerProperty gold = new SimpleIntegerProperty(0);
    private IntegerProperty elixir = new SimpleIntegerProperty(0);
    private IntegerProperty score = new SimpleIntegerProperty(0);
    private IntegerProperty goldGained = new SimpleIntegerProperty(0);
    private IntegerProperty elixirGained = new SimpleIntegerProperty(0);

    public User(Socket socket, String userName, Formatter formatter, Scanner scanner) {
        this.socket = socket;
        this.userName.setValue(userName);
        this.formatter = formatter;
        this.scanner = scanner;
    }

    public String getUserName() {
        return userName.get();
    }

    public void sendString(String string) {
        try {

            formatter.format(string);
            formatter.flush();
        } catch (NullPointerException ignored) {
        }
    }

    public void setReceiver(NetworkModel networkModel) {
        new Receiver(scanner, networkModel).start();
    }

    public void close() throws IOException {
        if (formatter != null)
            formatter.close();
        if (scanner != null)
            scanner.close();
        if (socket != null)
            socket.close();
    }

    public IntegerProperty elixirProperty() {
        return elixir;
    }

    public IntegerProperty goldProperty() {
        return gold;
    }

    public IntegerProperty scoreProperty() {
        return score;
    }

    public IntegerProperty goldGainedProperty() {
        return goldGained;
    }

    public IntegerProperty elixirGainedProperty() {
        return elixirGained;
    }

    public void setResource(int resourceType, int value) {
        if (resourceType == BookOfConstants.getType(GOLD_STORAGE))
            gold.setValue(value);
        else
            elixir.setValue(value);
    }

    public void setPrize(int resourceType, int value) {
        if (resourceType == BookOfConstants.getType(GOLD_STORAGE))
            goldGained.setValue(value);
        else
            elixirGained.setValue(value);
    }

    public void linkResource(int resourceType, IntegerProperty toLink) {
        if (resourceType == BookOfConstants.getType(GOLD_STORAGE))
            gold = toLink;
        else
            elixir = toLink;
    }

    public void linkScore(IntegerProperty toLink) {
        score = toLink;
    }

    public void setScore(int value) {
        score.setValue(value);
    }

    @Override
    public String toString() {
        return userName.get();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof User)
            return ((User) obj).userName.equals(userName);
        return false;
    }

}
