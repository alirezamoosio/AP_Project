package network.tcp;

import constants.BookOfConstants;
import constants.Net;
import control.Game;
import control.defense.GameDefense;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.net.*;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Formatter;
import java.util.Scanner;

import static constants.EntityNames.ELIXIR_STORAGE;
import static constants.EntityNames.GOLD_STORAGE;

public class Host extends NetworkModel {
    private ServerSocket server;
    private int port = Net.HOST_DEFAULT_PORT;
    private static Host instance = new Host();
    private boolean isListening;

    private Runnable listenRunnable = () -> {
        try {
            while (true) {
                if (isListening) {
                    Socket socket = server.accept();
                    User user = initConnection(socket);
                    user.setReceiver(this);
                    Platform.runLater(() -> {
                        users.add(user);
                        sendNewUser(user);
                    });
                }
            }
        } catch (SocketException e) {
            System.err.println("Server closed");
        } catch (IOException e) {
            e.printStackTrace();
        }
    };

    public User initConnection(Socket socket) {
        try {
            Scanner scanner = new Scanner(socket.getInputStream());
            Formatter formatter = new Formatter(socket.getOutputStream());
            Scanner help = new Scanner(scanner.nextLine());
            String userName = help.next();
            int gold = help.nextInt();
            int elixir = help.nextInt();
            int score = help.nextInt();
            formatter.format(myName + "\n");
            formatter.flush();
            String content = chatContents.get().equals("") ? "\n" : chatContents.get();
            formatter.format(content);
            formatter.format("End of chat log\n");
            formatter.flush();
            for (User user : users) {
                formatter.format("NewUser: " + user + " " + user.goldProperty().get() + " " + user.elixirProperty().get() +
                        " " + user.scoreProperty().get() +"\n");
            }
            formatter.flush();
            User user = new User(socket, userName, formatter, scanner);
            user.setResource(BookOfConstants.getType(GOLD_STORAGE), gold);
            user.setResource(BookOfConstants.getType(ELIXIR_STORAGE), elixir);
            user.setScore(score);
            return user;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    private Host() {

    }

    public static Host getInstance() {
        return instance;
    }

    public ListProperty<User> usersProperty() {
        return usersProperty;
    }

    public void startUp(String myName) {
        this.myName = myName;
        users.add(new User(null, myName, null, null));
        isListening = true;
        try {
            server = new ServerSocket(port);
            Thread listenThread = new Thread(listenRunnable);
            listenThread.setDaemon(true);
            listenThread.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void close() {
        try {
            for (User user : users) {
                user.sendString("Server closed");
                user.close();
            }
            if (server != null)
                server.close();
            users.clear();
            isListening = false;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendNewUser(User user) {
        for (User eachUser : users) {
            String toSend = "NewUser: " + user + " " + user.goldProperty().get() + " " + user.elixirProperty().get() +
                    " " + user.scoreProperty().get() +"\n";
            eachUser.sendString(toSend);
        }
    }

    private void sendToAll(Message message) {
        for (User user : users) {
            String toSend = "NewMessage From: " + message.getSender() + " " + message.getContent() + "\n";
            user.sendString(toSend);
        }
    }

    @Override
    public void getMessage(Message message) {
        chatContents.setValue(chatContents.get() + message.getSender() + ": " + message.getContent() + "\n");
        sendToAll(message);
    }

    @Override
    public void sendMessage(Message message) {
        chatContents.setValue(chatContents.get() + message.getSender() + ": " + message.getContent() + "\n");
        sendToAll(message);
    }

    @Override
    public StringProperty getChatContents() {
        return chatContents;
    }

    public User getUserByUserName(String userName) {
        for (User user : users)
            if (user.getUserName().equals(userName))
                return user;
        return null;
    }

    public void removeUser(User user) throws IOException {
        user.sendString("You are removed");
        for (User eachUser : users) {
            if (!user.equals(eachUser)) {
                String toSend = "UserRemoved: " + user + "\n";
                eachUser.sendString(toSend);
            }
        }
        Host.getInstance().getUsers().remove(user);
        user.close();
    }

    public void removeUser(String userName) throws IOException {
        removeUser(getUserByUserName(userName));
    }

    @Override
    public void updateUserResource(String userName, int resourceType, int value) {
        User user = getUserByUserName(userName);
        user.setResource(resourceType, value);
        for (User eachUSer : users) {
            String toSend = "NewResource From: " + userName + " " + resourceType + " " + value + "\n";
            eachUSer.sendString(toSend);
        }
    }

    @Override
    public void updateUserScore(String userName, int score) {
        User user = getUserByUserName(userName);
        user.setScore(score);
        for (User eachUser : users) {
            String toSend = "NewScore From: " + userName + " " + score + "\n";
            eachUser.sendString(toSend);
        }
    }

    @Override
    public void updateUserPrize(String userName, int resourceType, int value) {
        User user = getUserByUserName(userName);
        user.setPrize(resourceType, value);
        for (User eachUser : users) {
            String toSend = "NewPrize From: " + userName + " " + resourceType + " " + value + "\n";
            eachUser.sendString(toSend);
        }
    }

    public String getIP() {
        try {
            Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
            for (NetworkInterface netif : Collections.list(nets)) {
                Enumeration<InetAddress> addresses = netif.getInetAddresses();
                for (InetAddress address : Collections.list(addresses)) {
                    if (address.getHostAddress().matches("\\d+.\\d+.\\d+.\\d+") && !address.isLoopbackAddress())
                        return address.getHostAddress();
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return null;
    }
}
