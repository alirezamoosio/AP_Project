package network.tcp;

import constants.BookOfConstants;
import constants.Net;
import control.Game;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import model.game.ministry.Treasury;
import model.game.ministry.War;
import model.game.unit.Unit;
import view.View;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Scanner;

import static constants.EntityNames.ELIXIR_STORAGE;
import static constants.EntityNames.GOLD_STORAGE;

public class Guest extends NetworkModel {
    private int port = Net.GUEST_DEFAULT_PORT;
    private User server;
    private String attackDest;
    private String serverIP;

    public String getAttackDest() {
        return attackDest;
    }

    public void setAttackDest(String attackDest) {
        this.attackDest = attackDest;
    }

    private int defensePort = -1;
    private String viewedMap;
    private ObservableList<User> users = FXCollections.observableArrayList();
    private ListProperty<User> usersProperty = new SimpleListProperty<>(users);
    private StringProperty chatContents = new SimpleStringProperty("");
    private static Guest instance = new Guest();

    public int getDefensePort() {
        return defensePort;
    }

    public void setDefensePort(int defensePort) {
        this.defensePort = defensePort;
    }

    public void sendDefensePortRequest() {
        Guest.getInstance().getServer().sendString("GetDefensePortOf " + attackDest + " " + myName + "\n");
    }

    public User getServer() {
        return server;
    }

    private Guest() {
    }

    public static Guest getInstance() {
        return instance;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    public void connect(String serverIP, int port, Game game) {
        new Thread(() -> {
            try {
                this.serverIP = serverIP;
                myName = game.getName();
                Socket socket = new Socket(serverIP, port);
                server = initConnection(socket, game);
                server.setReceiver(this);
                sendNewResource(BookOfConstants.getType(GOLD_STORAGE), Treasury.getInstance().getGoldAmount());
                sendNewResource(BookOfConstants.getType(ELIXIR_STORAGE), Treasury.getInstance().getElixirAmount());
                sendNewScore(game.getVillage().getScore());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public User initConnection(Socket socket, Game game) {
        try {
            Formatter formatter = new Formatter(socket.getOutputStream());
            Scanner scanner = new Scanner(socket.getInputStream());
            formatter.format(myName + " " + Treasury.getInstance().getGoldAmount() + " " + Treasury.getInstance().getElixirAmount()
                    + " " + game.getVillage().getScore() + "\n");
            formatter.flush();
            String yourName = scanner.nextLine();
            StringBuilder chatLog = new StringBuilder();
            String log;
            while (!(log = scanner.nextLine()).equals("End of chat log")) {
                if (!log.equals(""))
                    chatLog.append(log).append("\n");
            }
            chatContents.setValue(chatLog.toString());
            System.err.println("Connected to " + yourName);
            return new User(socket, yourName, formatter, scanner);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void getMessage(Message message) {
        chatContents.setValue(chatContents.get() + message.getSender() + ": " + message.getContent() + "\n");
    }

    @Override
    public void sendMessage(Message message) {
        server.sendString("NewMessage From: " + message.getSender() + " " + message.getContent() + "\n");
    }

    public void sendNewResource(int type, int value) {
        server.sendString("NewResource From: " + myName + " " + type + " " + value + "\n");
    }

    public void sendNewScore(int value) {
        server.sendString("NewScore From: " + myName + " " + value + "\n");
    }

    public void sendNewPrize(int type, int value) {
        server.sendString("NewPrize From: " + myName + " " + type + " " + value + "\n");
    }

    @Override
    public void updateUserScore(String userName, int value) {
        User user = getUserByUserName(userName);
        if (user != null)
            user.setScore(value);
    }


    @Override
    public void updateUserResource(String userName, int resourceType, int value) {
        User user = getUserByUserName(userName);
        if (user != null)
            user.setResource(resourceType, value);
    }

    @Override
    public void updateUserPrize(String userName, int resourceType, int value) {
        User user = getUserByUserName(userName);
        if (user != null)
            user.setResource(resourceType, value);
    }

    @Override
    public StringProperty getChatContents() {
        return chatContents;
    }

    public void addUser(User user) {
        users.add(user);
    }

    public void close() {
        try {
            if (server != null) {
                for (ArrayList<Unit> units : game.getVillage().getAttackingUnits().values())
                    War.getInstance().unitsToCamp(units);
                server.sendString("Closed: " + myName + "\n");
                server.sendString("Closed: " + myName +" "+attackDest+ "\n");
                server.close();
                server = null;
            }
            users.clear();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ObservableList<User> getUsers() {
        return users;
    }

    public ListProperty<User> usersProperty() {
        return usersProperty;
    }

    public void serverClosed() {
        new Alert(Alert.AlertType.WARNING, "Host terminated this room!").show();
        View.getInstance().closeMultiStage();
    }

    public void setViewedMap(String s) {
        viewedMap = s;
    }

    public void getJsonMapRequest(User dest) {
        attackDest = dest.getUserName();
        server.sendString("GiveMap: " + dest.getUserName() + " to " + myName + "\n");
    }

    public void removeUser(String userName) {
        users.remove(getUserByUserName(userName));
    }

    public User getUserByUserName(String userName) {
        for (User user : users) {
            if (user.getUserName().equals(userName))
                return user;
        }
        return null;
    }

    public String getViewedMap() {
        return viewedMap;
    }

    public String getServerIP() {
        return serverIP;
    }
}
