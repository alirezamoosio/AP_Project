package network.tcp;

import constants.BookOfConstants;
import constants.FXMLConstants;
import constants.Net;
import constants.Visual;
import control.defense.GameDefense;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextInputDialog;
import view.View;

import java.io.IOException;
import java.util.Optional;
import java.util.Scanner;

import static constants.EntityNames.ELIXIR_STORAGE;
import static constants.EntityNames.GOLD_STORAGE;

public class Receiver extends Thread {
    private Scanner scanner;
    private NetworkModel networkModel;

    Receiver(Scanner scanner, NetworkModel networkModel) {
        this.scanner = scanner;
        this.networkModel = networkModel;
    }

    @Override
    public void run() {
        while (true) {
            String string = scanner.nextLine();
            if (string.startsWith("NewUser: ")) {
                Scanner temp = new Scanner(string);
                temp.next();
                String userName = temp.next();
                int gold = temp.nextInt();
                int elixir = temp.nextInt();
                int score = temp.nextInt();
                User user = new User(null, userName, null, null);
                user.setResource(BookOfConstants.getType(GOLD_STORAGE), gold);
                user.setResource(BookOfConstants.getType(ELIXIR_STORAGE), elixir);
                user.setScore(score);
                Platform.runLater(() -> Guest.getInstance().addUser(user));
                System.err.println("New user received at client-side");
            } else if (string.startsWith("NewMessage From: ")) {
                Scanner temp = new Scanner(string);
                temp.next();
                temp.next();
                String sender = temp.next();
                String content = temp.nextLine();
                Message message = new Message(sender, content);
                Platform.runLater(() -> networkModel.getMessage(message));
                System.err.println("Message received from " + sender);
            } else if (string.equals("Server closed") || string.equals("You are removed")) {
                Platform.runLater(() -> Guest.getInstance().serverClosed());
            } else if (string.startsWith("UserRemoved: ")) {
                Platform.runLater(() -> Guest.getInstance().removeUser(string.substring(13)));
            } else if (string.startsWith("Closed: ")) {
                Platform.runLater(() -> {
                    try {
                        Scanner temp = new Scanner(string);
                        temp.next();
                        String attacker = temp.next();
                        if (temp.hasNext()) {
                            String dfender = temp.next();
                            User defender = Host.getInstance().getUserByUserName(dfender);
//                            defender.sendString("EndDefense\n");
                            Host.getInstance().setGame(Host.getInstance().getGameDefense().getGame());
                            View.getInstance().popScene(true);
                            Host.getInstance().setGameDefense(null);
                        }
                        Host.getInstance().removeUser(attacker);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            } else if (string.startsWith("EndDefense")) {
                Host.getInstance().setGame(Host.getInstance().getGameDefense().getGame());
                View.getInstance().popScene(true);
                Host.getInstance().setGameDefense(null);
            } else if (string.startsWith("NewResource From: ")) {
                Scanner temp = new Scanner(string);
                temp.next();
                temp.next();
                String userName = temp.next();
                int resourceType = temp.nextInt();
                int value = temp.nextInt();
                networkModel.updateUserResource(userName, resourceType, value);
            } else if (string.startsWith("NewScore From: ")) {
                Scanner temp = new Scanner(string);
                temp.next();
                temp.next();
                String userName = temp.next();
                int value = temp.nextInt();
                networkModel.updateUserScore(userName, value);
            } else if (string.startsWith("NewPrize From: ")) {
                Scanner temp = new Scanner(string);
                temp.next();
                temp.next();
                String userName = temp.next();
                int resourceType = temp.nextInt();
                int value = temp.nextInt();
                networkModel.updateUserPrize(userName, resourceType, value);
            } else if (string.startsWith("GiveMap:")) {
                Scanner temp = new Scanner(string);
                temp.next();
                String dest = temp.next();
                temp.next();
                String src = temp.next();
                User destination = Host.getInstance().getUserByUserName(dest), source = Host.getInstance().getUserByUserName(src);
                if (dest.equals(Host.getInstance().getMyName())) {
                    String jsonMap = Host.getInstance().getJsonMap();
                    source.sendString("SendJsonMap: " + jsonMap + "\n");
                } else
                    destination.sendString("GiveYourMapToGiveItTo " + src + "\n");
            } else if (string.startsWith("GiveYourMapToGiveItTo")) {
                Scanner temp = new Scanner(string);
                temp.next();
                String src = temp.next();
                String jsonMap = Guest.getInstance().getJsonMap();
                Guest.getInstance().getServer().sendString("GiveThisMapTo " + src + " " + jsonMap + "\n");
            } else if (string.startsWith("GiveThisMapTo")) {
                Scanner temp = new Scanner(string);
                temp.next();
                String src = temp.next(), map = temp.next();
                User source = Host.getInstance().getUserByUserName(src);
                source.sendString("SendJsonMap: " + map + "\n");
            } else if (string.startsWith("SendJsonMap: ")) {
                Guest.getInstance().setViewedMap(string.substring("SendJsonMap: ".length()));
            } else if (string.startsWith("GetDefensePortOf")) {
                Scanner temp = new Scanner(string);
                temp.next();
                String dfender = temp.next(), attacker = temp.next();
                User defender = Host.getInstance().getUserByUserName(dfender), attackerUser = Host.getInstance().getUserByUserName(attacker);
                if (dfender.equals(Host.getInstance().getMyName())) {
                    Platform.runLater(() -> {
                        Optional<String> result = getDefensePortFromDialog();
                        if (result.isPresent()) {
                            attackerUser.sendString("DefensePort " + result.get().trim() + "\n");
                        }
                    });
                } else
                    defender.sendString("GiveYourPortToGiveItTo " + attacker + "\n");
            } else if (string.startsWith("GiveYourPortToGiveItTo")) {
                Scanner temp = new Scanner(string);
                temp.next();
                String attacker = temp.next();
                Platform.runLater(() -> {
                    Optional<String> result = getDefensePortFromDialog();
                    if (result.isPresent()) {
                        Guest.getInstance().getServer().sendString("GiveMyPortTo " + attacker + " " + result.get().trim() + "\n");
                    }
                });
            } else if (string.startsWith("GiveMyPortTo")) {
                Scanner temp = new Scanner(string);
                temp.next();
                String attackr = temp.next(), port = temp.next();
                User attacker = Host.getInstance().getUserByUserName(attackr);
                attacker.sendString("DefensePort " + port + "\n");
            } else if (string.startsWith("DefensePort")) {
                Scanner temp = new Scanner(string);
                temp.next();
                int port = temp.nextInt();
                Guest.getInstance().setDefensePort(port);
            }
            System.err.println(string);
        }
    }

    private Optional<String> getDefensePortFromDialog() {
        TextInputDialog dialog = new TextInputDialog(Net.DEFENDER_DEFAULT_PORT + "");
        dialog.setTitle("Please Enter Your Port");
        dialog.setHeaderText("Please Enter Your Port");
        dialog.setContentText("Port: ");
        Optional<String> ret = dialog.showAndWait();
        initGameDefense(Integer.parseInt(ret.get()));
        return ret;
    }

    private void initGameDefense(int port) {
        try {
            FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(FXMLConstants.GAME_DEFENSE));
            View.getInstance().pushScene(new Scene(loader.load(), Visual.GAME_WIDTH, Visual.GAME_HEIGHT), true);
            GameDefense controller = loader.getController();
            Host.getInstance().setGameDefense(controller);
            controller.setPort(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}