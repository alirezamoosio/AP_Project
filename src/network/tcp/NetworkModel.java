package network.tcp;

import com.gilecode.yagson.YaGson;
import control.Game;
import control.defense.GameDefense;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import network.tcp.Message;

public abstract class NetworkModel {
    ObservableList<User> users = FXCollections.observableArrayList();
    ListProperty<User> usersProperty = new SimpleListProperty<>(users);
    StringProperty chatContents = new SimpleStringProperty("");
    Game game;
    String myName;
    private GameDefense gameDefense;


    public GameDefense getGameDefense() {
        return gameDefense;
    }

    public void setGameDefense(GameDefense gameDefense) {
        this.gameDefense = gameDefense;
    }

    public abstract void getMessage(Message message);

    public abstract void sendMessage(Message message);

    public abstract StringProperty getChatContents();

    public abstract void updateUserResource(String userName, int resourceType, int value);

    public abstract void updateUserScore(String userName, int value);

    public abstract void updateUserPrize(String userName, int resourceType, int value);

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public ObservableList<User> getUsers() {
        return users;
    }

    public String getMyName() {
        return myName;
    }

    public String getJsonMap() {
        YaGson gson = new YaGson();
        String ret = gson.toJson(game.getVillage().getMap());
        ret = ret.replace("@root.buildingsMap.7-val.0.units.0-key", "15");
        ret = ret.replace("@root.buildingsMap.7-val.0.units.1-key", "16");
        ret = ret.replace("@root.buildingsMap.7-val.0.units.2-key", "17");
        ret = ret.replace("@root.buildingsMap.7-val.0.units.3-key", "18");
        ret = ret.replace("@root.buildingsMap.7-val.0.units.4-key", "19");
        return ret;
    }

}
