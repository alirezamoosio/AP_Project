package control;

import model.game.map.Position;

@FunctionalInterface
public interface CellClickAction {
    void accept(Position clicked);
}
