package control.multiMenus;


import constants.BookOfConstants;
import constants.FXMLConstants;
import javafx.event.ActionEvent;
import javafx.scene.control.TextField;
import model.game.ministry.Treasury;
import network.tcp.Host;
import network.tcp.User;

import java.io.IOException;

import static constants.EntityNames.ELIXIR_STORAGE;
import static constants.EntityNames.GOLD_STORAGE;

public class StartHostMenu extends MultiPlayerGUI {
    public TextField port;

    public void initHost(ActionEvent actionEvent) throws IOException {
        Host.getInstance().setPort(Integer.parseInt(port.getCharacters().toString()));
        Host.getInstance().startUp(game.getName());
        Host.getInstance().setGame(game);
        MainMenu mainMenu = (MainMenu) setMenu(FXMLConstants.HOST_MAIN);
        mainMenu.setNetworkModel(Host.getInstance());
        User server = Host.getInstance().getUserByUserName(game.getName());
        server.linkResource(BookOfConstants.getType(GOLD_STORAGE), Treasury.getInstance().getGoldAmountProperty());
        server.linkResource(BookOfConstants.getType(ELIXIR_STORAGE), Treasury.getInstance().getElixirAmountProperty());
        server.linkScore(game.getVillage().scoreProperty());
        server.goldProperty().addListener((observable, oldValue, newValue) ->
                Host.getInstance().updateUserResource(game.getName(), BookOfConstants.getType(GOLD_STORAGE), newValue.intValue()));
        server.elixirProperty().addListener((observable, oldValue, newValue) ->
                Host.getInstance().updateUserResource(game.getName(), BookOfConstants.getType(ELIXIR_STORAGE), newValue.intValue()));
        server.scoreProperty().addListener((observable, oldValue, newValue) ->
                Host.getInstance().updateUserScore(game.getName(), newValue.intValue()));
        server.goldGainedProperty().addListener((observable, oldValue, newValue) ->
                Host.getInstance().updateUserPrize(game.getName(), BookOfConstants.getType(GOLD_STORAGE), newValue.intValue()));
        server.elixirGainedProperty().addListener((observable, oldValue, newValue) ->
                Host.getInstance().updateUserPrize(game.getName(), BookOfConstants.getType(ELIXIR_STORAGE), newValue.intValue()));
    }
}
