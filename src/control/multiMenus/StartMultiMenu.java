package control.multiMenus;

import constants.FXMLConstants;
import network.tcp.Guest;
import network.tcp.Host;
import view.View;

import java.io.IOException;

public class StartMultiMenu extends MultiPlayerGUI {
    public void host() throws IOException {
        setMenu(FXMLConstants.START_HOST);
        View.getInstance().setOnMultiCloseRequest(() -> Host.getInstance().close());
    }

    public void guest() throws IOException {
        setMenu(FXMLConstants.START_GUEST);
        View.getInstance().setOnMultiCloseRequest(() -> Guest.getInstance().close());
    }
}
