package control.multiMenus;

import constants.BookOfConstants;
import constants.FXMLConstants;
import javafx.event.ActionEvent;
import javafx.scene.control.TextField;
import model.game.ministry.Treasury;
import network.tcp.Guest;

import java.io.IOException;

import static constants.EntityNames.ELIXIR_STORAGE;
import static constants.EntityNames.GOLD_STORAGE;

public class StartGuestMenu extends MultiPlayerGUI {

    public TextField serverIp, serverPort;

    public void connect(ActionEvent actionEvent) throws IOException {
        String serverIp = this.serverIp.getCharacters().toString();
        int serverPort = Integer.parseInt(this.serverPort.getCharacters().toString());
        Guest.getInstance().connect(serverIp, serverPort, game);
        MainMenu mainMenu = (MainMenu) setMenu(FXMLConstants.GUEST_MAIN);
        mainMenu.setNetworkModel(Guest.getInstance());
        Guest.getInstance().setGame(game);
        Treasury.getInstance().getGoldAmountProperty().addListener((observable, oldValue, newValue) ->
                Guest.getInstance().sendNewResource(BookOfConstants.getType(GOLD_STORAGE), newValue.intValue()));
        Treasury.getInstance().getElixirAmountProperty().addListener((observable, oldValue, newValue) ->
                Guest.getInstance().sendNewResource(BookOfConstants.getType(ELIXIR_STORAGE), newValue.intValue()));
        game.getVillage().scoreProperty().addListener((observable, oldValue, newValue) ->
                Guest.getInstance().sendNewScore(newValue.intValue())
        );
        Treasury.getInstance().getGoldGainedProperty().addListener((observable, oldValue, newValue) ->
                Guest.getInstance().sendNewPrize(BookOfConstants.getType(GOLD_STORAGE), newValue.intValue()));
        Treasury.getInstance().getElixirGainedProperty().addListener((observable, oldValue, newValue) ->
                Guest.getInstance().sendNewPrize(BookOfConstants.getType(ELIXIR_STORAGE), newValue.intValue()));
    }
}
