package control.multiMenus;

import com.gilecode.yagson.YaGson;
import constants.BookOfConstants;
import constants.FXMLConstants;
import constants.Visual;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import model.game.VillageMode;
import model.game.map.Map;
import model.game.unit.Unit;
import network.tcp.Guest;
import network.tcp.User;
import view.View;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

public class GuestMainMenu extends MainMenu {
    public ListView<User> guestList;

    public void viewGuest(ActionEvent actionEvent) {

        Guest.getInstance().setGame(game);
        Guest.getInstance().setViewedMap(null);
        Guest.getInstance().getGame().getVillage().setAttackingMap(null);
        User selectedUser = guestList.getSelectionModel().getSelectedItem();
        if (selectedUser.getUserName().equals(Guest.getInstance().getMyName()))
            new Alert(Alert.AlertType.WARNING, "You cannot attack yourself!").show();
        else {
            Guest.getInstance().getJsonMapRequest(selectedUser);
            new Thread(() -> {

                if (Guest.getInstance().getViewedMap() == null) {
                    while (Guest.getInstance().getViewedMap() == null)
                    {
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                System.err.println(Guest.getInstance().getViewedMap());
                YaGson yaGson = new YaGson();
                Map attackMap = yaGson.fromJson(Guest.getInstance().getViewedMap(), Map.class);
                attackMap.addListenerForAllBuildings();
                game.getVillage().setAttackingMap(yaGson.fromJson(Guest.getInstance().getViewedMap(),Map.class));
                Platform.runLater(() -> {
                    try {
                        FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(FXMLConstants.VIEW_ATTACK));

                        View.getInstance().pushMultiScene(new Scene(loader.load(), Visual.GAME_WIDTH,Visual.GAME_HEIGHT),true);
                        ViewAttack controller = loader.getController();
                        game.getVillage().setMode(VillageMode.ATTACKING);
                        HashMap<Integer, ArrayList<Unit>> selectedUnits = new HashMap<>();
                        game.getVillage().setSelectedUnits(selectedUnits);

                        controller.setGame(game);
                        controller.setMenu(FXMLConstants.VIEW_ATTACK_MAIN);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }).start();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        guestList.itemsProperty().bindBidirectional(Guest.getInstance().usersProperty());
    }

    public void leave(ActionEvent actionEvent) {
        Guest.getInstance().close();
        View.getInstance().closeMultiStage();
    }
}
