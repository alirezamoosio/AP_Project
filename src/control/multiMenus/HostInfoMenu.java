package control.multiMenus;

import control.multiMenus.MultiPlayerGUI;
import javafx.scene.control.Label;
import network.tcp.Host;

import java.net.URL;
import java.util.ResourceBundle;

public class HostInfoMenu extends MultiPlayerGUI {

    public Label ip, port;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ip.setText(Host.getInstance().getIP());
        port.setText("" + Host.getInstance().getPort());
    }
}
