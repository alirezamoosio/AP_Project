package control.multiMenus;

import javafx.event.ActionEvent;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import network.tcp.NetworkModel;
import network.tcp.Message;

import java.net.URL;
import java.util.ResourceBundle;


public class ChatRoomMenu extends MultiPlayerGUI {

    public TextArea chatArea;
    public TextField chatMessage;
    private NetworkModel networkModel;

    public void send(ActionEvent actionEvent) {
        String content = chatMessage.getCharacters().toString();
        String sender = game.getName();
        Message message = new Message(sender, content);
        networkModel.sendMessage(message);
    }

    public void setNetworkModel(NetworkModel networkModel) {
        this.networkModel = networkModel;
        chatArea.setText(networkModel.getChatContents().getValue());
        networkModel.getChatContents().addListener((observable, oldValue, newValue) -> {
            chatArea.setText(newValue);
        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        chatArea.setEditable(false);
        chatArea.setStyle("-fx-text-inner-color: white; -fx-font-size: 20;");
    }

}
