package control.multiMenus;

import constants.BookOfConstants;
import constants.FXMLConstants;
import constants.Visual;
import control.attackMenus.StartAttackMenu;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import network.tcp.Guest;
import view.View;

import java.io.IOException;

public class ViewAttackMain {
    public void back(ActionEvent event) {
        Guest.getInstance().getGame().getVillage().setAttackingMap(null);
        Guest.getInstance().setViewedMap(null);
        View.getInstance().popMultiScene(false);
    }

    public void attack(ActionEvent event) {
        new Thread(() -> {

                Guest.getInstance().sendDefensePortRequest();
                while (Guest.getInstance().getDefensePort() == -1) {
                    try {
                        Thread.sleep(50 );
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
//                Guest.getInstance().setViewedMap(null);
                Platform.runLater(()->
                {
                    try {
                        View.getInstance().popMultiScene(false);
                        FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(FXMLConstants.START_ATTACK));
                        View.getInstance().pushMultiScene(new Scene(loader.load(), Visual.GAME_WIDTH, Visual.GAME_HEIGHT), false);
                        StartAttackMenu controller = loader.getController();
                        controller.setGame(Guest.getInstance().getGame());
                        controller.init();
//                        controller.setOnlineDefender(InetAddress.getByName("localhost"), Guest.getInstance().getDefensePort());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        }).start();

    }

}
