package control.multiMenus;

import constants.FXMLConstants;
import javafx.event.ActionEvent;
import network.tcp.NetworkModel;

import java.io.IOException;

public class MainMenu extends MultiPlayerGUI {
    private NetworkModel networkModel;

    public void enterChatRoom(ActionEvent actionEvent) throws IOException {
        ChatRoomMenu chatRoomMenu = (ChatRoomMenu) setMenu(FXMLConstants.CHAT_ROOM);
        chatRoomMenu.setNetworkModel(networkModel);
    }

    public void leaderBoard(ActionEvent actionEvent) throws IOException {
        LeaderBoard leaderBoard = (LeaderBoard) setMenu(FXMLConstants.LEADER_BOARD);
        leaderBoard.setUsers(networkModel.getUsers());
    }

    public void scoreBoard(ActionEvent actionEvent) throws IOException {
        ScoreBoard scoreBoard = (ScoreBoard) setMenu(FXMLConstants.SCORE_BOARD);
        scoreBoard.setUsers(networkModel.getUsers());
    }

    public void setNetworkModel(NetworkModel networkModel) {
        this.networkModel = networkModel;
    }
}
