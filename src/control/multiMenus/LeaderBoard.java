package control.multiMenus;

import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import network.tcp.Host;
import network.tcp.User;

import java.net.URL;
import java.util.ResourceBundle;

public class LeaderBoard extends MultiPlayerGUI {

    public TableView<User> board;
    public TableColumn userColumn, goldColumn, elixirColumn, scoreColumn;

    public void setUsers(ObservableList<User> users) {
        userColumn.setCellValueFactory(new PropertyValueFactory<>("userName"));
        goldColumn.setCellValueFactory(new PropertyValueFactory<>("gold"));
        elixirColumn.setCellValueFactory(new PropertyValueFactory<>("elixir"));
        scoreColumn.setCellValueFactory(new PropertyValueFactory<>("score"));
        board.setItems(users);
    }
}
