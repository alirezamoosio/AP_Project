package control.multiMenus;

import constants.BookOfConstants;
import constants.CSSConstants;
import constants.FXMLConstants;
import control.Game;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import view.View;

import javax.swing.text.html.CSS;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public abstract class MultiPlayerGUI implements Initializable {
    protected Game game;
    public MultiPlayerGUI setMenu(String name) throws IOException
    {
        return setMenu(name,false);
    }
    public MultiPlayerGUI setMenu(String name, boolean isMaximized) throws IOException {
        FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(name));
        Parent root = loader.load();
        if (name.equals(FXMLConstants.CHAT_ROOM))
            root.getStylesheets().add(BookOfConstants.getURL(CSSConstants.CHATROOM).toExternalForm());
        else
            root.getStylesheets().add(BookOfConstants.getURL(CSSConstants.MULTI).toExternalForm());
        View.getInstance().pushMultiScene(root,isMaximized);
        ((MultiPlayerGUI) loader.getController()).setGame(game);
        return loader.getController();
    }

//    public Object setMenuReturnObject(String name, boolean isMaximized) throws IOException {
//        FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(name));
//        Parent root = loader.load();
//        if (name.equals(FXMLConstants.CHAT_ROOM))
//            root.getStylesheets().add(BookOfConstants.getURL(CSSConstants.CHATROOM).toExternalForm());
//        else
//            root.getStylesheets().add(BookOfConstants.getURL(CSSConstants.MULTI).toExternalForm());
//        View.getInstance().pushMultiScene(root,isMaximized);
//        ((MultiPlayerGUI) loader.getController()).setGame(game);
//        return loader.getController();
//    }

    public void back() {
        View.getInstance().popMultiScene(false);
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
