package control.multiMenus;


import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import network.tcp.User;

public class ScoreBoard extends MultiPlayerGUI {
    public TableView<User> board;
    public TableColumn userColumn, goldGainedColumn, elixirGainedColumn;

    public void setUsers(ObservableList<User> users) {
        userColumn.setCellValueFactory(new PropertyValueFactory<>("userName"));
        goldGainedColumn.setCellValueFactory(new PropertyValueFactory<>("goldGained"));
        elixirGainedColumn.setCellValueFactory(new PropertyValueFactory<>("elixirGained"));
        board.setItems(users);
    }
}
