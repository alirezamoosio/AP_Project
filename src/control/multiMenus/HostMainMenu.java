package control.multiMenus;

import constants.FXMLConstants;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import network.tcp.Host;
import network.tcp.User;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class HostMainMenu extends MainMenu {

    public ListView<User> guestList;

    public void viewGuest(ActionEvent actionEvent) {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        guestList.itemsProperty().bindBidirectional(Host.getInstance().usersProperty());
    }

    public void kick(ActionEvent actionEvent) {
        User user = guestList.getSelectionModel().getSelectedItem();
        if (user.getUserName().equals(game.getName())) {
            new Alert(Alert.AlertType.ERROR, "You can't kick yourself!").show();
        } else {
            try {
                Host.getInstance().removeUser(user);
            } catch (IOException e) {
                new Alert(Alert.AlertType.ERROR, "Unable to kick user").show();
            }
        }
    }

    public void info(ActionEvent actionEvent) throws IOException {
        setMenu(FXMLConstants.HOST_INFO);
    }
}
