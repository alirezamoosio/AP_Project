package control.multiMenus;

import constants.BookOfConstants;
import constants.CSSConstants;
import control.GameFight;
import control.GameScene;
import control.attackMenus.GraphicalAttackControl;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ViewAttack extends GameScene {
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        slider.valueProperty().addListener(
                (observable, oldValue, newValue) -> zoom(newValue.doubleValue()));
    }

    @Override
    public void back() {

    }

    @Override
    protected void cellClick(int x, int y) {

    }

    public void setMenu(String name) {
        ViewAttackMain controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(name));
            AnchorPane root = loader.load();
            splitPane.getItems().set(0, root);
            controller = loader.getController();
            root.getStylesheets().add(BookOfConstants.getURL(CSSConstants.MULTI).toExternalForm());
            root.setStyle("-fx-background-color: #6f1516");
            roots.push(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
