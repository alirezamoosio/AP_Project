package control.graphicsMenus;

import constants.BookOfConstants;
import constants.ConstantNames;
import constants.ResourceConstants;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import model.exceptions.NotEnoughMoneyException;
import model.exceptions.StorageEmptyException;
import model.game.Village;
import model.game.building.village.Barracks;
import model.game.ministry.War;

import java.util.ArrayList;

public class SoldierList extends BuildingMenu {
    public TextField countTextField;
    public Label chosenUnit;
    public GridPane pane;
    public Spinner numberAvailable;
    private int choice;


    public void selectSoldier() {
        // TODO: 6/5/18 potentially buggy if the order of the list is changed. think of a better way
        Barracks barracks = (Barracks) building;
        int soldierType = choice;
        int count = (int) numberAvailable.getValue();
        try {
            War.getInstance().createUnit(barracks.getID(), soldierType, count);
        } catch (NotEnoughMoneyException | StorageEmptyException e) {
            new Alert(Alert.AlertType.ERROR, "You don't have enough resources:(").show();
        }
        // TODO: 6/5/18 decide how to implement e.g: should he select the cell on the map? if so how to handle
    }


    private SoldierCount[] availableSoldiers() {
        int[] soldierTypes = ConstantNames.getAllSoldierTypes();
        ArrayList<SoldierCount> availableSoldiers = new ArrayList<>();
        Village village = gameVillage.getGame().getVillage();
        for (int type : soldierTypes) {
            SoldierCount thisSoldier = new SoldierCount(type);
            // TODO: 5/5/18 remove dragon hack in late phases
            int totalElixir = village.getTreasury().getElixirAmount();
            int unitCost = BookOfConstants.getConstant(ConstantNames.ELIXIR_REQUIRED, type);
            thisSoldier.count = totalElixir / unitCost;
            availableSoldiers.add(thisSoldier);
        }
        return availableSoldiers.toArray(new SoldierCount[0]);
    }

    private class SoldierCount {
        public SoldierCount(Integer type) {
            this.type = type;
        }

        Integer type;
        Integer count;
    }

//    public Barracks getBarracks() {
//        return barracks;
//    }
//
//    public void setBarracks(Barracks barracks) {
//        this.barracks = barracks;
//    }

    @Override
    public void init() {
        SoldierCount[] soldierCounts = availableSoldiers();
        for (int i = 0; i < soldierCounts.length; i++) {
            ImageView icon = new ImageView(new Image(BookOfConstants.getURL(ResourceConstants.getUnitIcon(soldierCounts[i].type)).toExternalForm()));
            Button unitName = new Button(BookOfConstants.getName(soldierCounts[i].type) + "       " + soldierCounts[i].count);
            unitName.setId("unit");
            unitName.setTextFill(Color.RED);
            int counter = i;
            unitName.setOnMouseClicked(event -> {
                chosenUnit.setText(BookOfConstants.getName(soldierCounts[counter].type));
                choice = soldierCounts[counter].type;
            });
            pane.add(icon, 0, i);
            pane.add(unitName, 1, i);
        }
    }
}
