package control.graphicsMenus;

import constants.BookOfConstants;
import constants.ResourceConstants;
import javafx.beans.property.IntegerProperty;
import javafx.collections.FXCollections;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import model.game.building.village.Camp;
import model.game.unit.Unit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class CampMenu extends BuildingMenu {
    // TODO: 6/6/18 Redesign
    public ListView<String> campSoldiers = new ListView<>();
    public GridPane pane;

    @Override
    public void init() {
        HashMap<Integer, ArrayList<Unit>> units = getCamp().getUnits();
        Integer types[] = units.keySet().toArray(new Integer[0]);
        Arrays.sort(types);
        for (int i = 0; i < types.length; i++) {
            pane.add(new ImageView(new Image(BookOfConstants.getURL(ResourceConstants.getUnitIcon(types[i])).toExternalForm())), 0, i);
            Label unitName = new Label(BookOfConstants.getName(types[i]) + "       " + units.get(types[i]).size());
            unitName.setId("unit");
            pane.add(unitName, 1, i);
        }
    }

    public Camp getCamp() {
        return (Camp) getBuilding();
    }
}
