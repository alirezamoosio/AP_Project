package control.graphicsMenus;

import control.Game;
import javafx.scene.control.Label;
import model.game.Village;
import model.game.building.Building;
import model.game.building.village.storage.Storage;

import java.util.ArrayList;

public class StorageInfoMenu extends InfoMenu {

    public Label capacityLabel;
    public Label holdingLabel;

    public Storage getStorage() {
        return (Storage) getBuilding();
    }

    @Override
    public void setBuilding(Building building) {
        super.setBuilding(building);
        bind(holdingLabel, getStorage().holdingProperty(), "holding: ");
        bind(capacityLabel, getStorage().capacityProperty(), "capacity: ");
    }
}
