package control.graphicsMenus;

import com.gilecode.yagson.YaGson;
import constants.BookOfConstants;
import constants.ResourceConstants;
import control.Game;
import control.StatusBar;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import model.game.ministry.Treasury;
import view.View;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class VillageStatusBar extends StatusBar {
    @FXML
    private HBox downBar;
    @FXML
    private ImageView exitIcon, saveIcon, homeIcon;
    @FXML
    private Label level, score;
    private Image exitHoverImage = new Image(BookOfConstants.getURL(ResourceConstants.IMAGE_EXIT_OPENED).toExternalForm());
    private Image exitImage = new Image(BookOfConstants.getURL(ResourceConstants.IMAGE_EXIT_CLOSED).toExternalForm());

    public HBox getBar() {
        return bar;
    }

    public HBox getDownBar() {
        return downBar;
    }

    public void setAndLoad(Game game) {
        setGame(game);
        level.setText("Village level: " + game.getVillage().getLevel());
        score.setText("Score: " + game.getVillage().getScore());
        game.getVillage().scoreProperty().addListener((observable, oldValue, newValue) -> score.setText("Score: " + newValue.intValue()));
        game.getVillage().getTownHall().levelProperty().addListener((observable, oldValue, newValue) -> level.setText("Village level: " + newValue.intValue()));
    }

    @FXML
    private void save(MouseEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home") + File.separator + "Desktop"));
        fileChooser.setTitle("Select Path to Save");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Json", "*.json"));
        File file = fileChooser.showSaveDialog(View.getInstance().getPrimaryStage());
        try {
            FileWriter fileWriter = new FileWriter(file);
            String gameString = new YaGson().toJson(game);
            // debugging yaGson bugs!!!
            gameString = gameString.replace("@root.village.map.buildingsMap.7-val.0.units.0-key", "15");
            gameString = gameString.replace("@root.village.map.buildingsMap.7-val.0.units.1-key", "16");
            gameString = gameString.replace("@root.village.map.buildingsMap.7-val.0.units.2-key", "17");
            gameString = gameString.replace("@root.village.map.buildingsMap.7-val.0.units.3-key", "18");
            gameString = gameString.replace("@root.village.map.buildingsMap.7-val.0.units.4-key", "19");
            //end of debug!!!
            fileWriter.write(gameString);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void home(MouseEvent event) {
        View.getInstance().popScene(false);
        View.getInstance().popScene(false);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        saveIcon.setImage(new Image(BookOfConstants.getURL(ResourceConstants.IMAGE_SAVE).toExternalForm()));
        homeIcon.setImage(new Image(BookOfConstants.getURL(ResourceConstants.IMAGE_HOME).toExternalForm()));
        exitIcon.setImage(exitImage);
        exitIcon.setOnMouseEntered(event -> exitIcon.setImage(exitHoverImage));
        exitIcon.setOnMouseExited(event -> exitIcon.setImage(exitImage));
        exitIcon.setOnMouseClicked(event -> View.getInstance().close());
        goldAmount.setText("" + Treasury.getInstance().getGoldAmount());
        elixirAmount.setText("" + Treasury.getInstance().getElixirAmount());
        Treasury.getInstance().getGoldAmountProperty().addListener((observable, oldValue, newValue) -> goldAmount.setText("" + newValue.intValue()));
        Treasury.getInstance().getElixirAmountProperty().addListener((observable, oldValue, newValue) -> elixirAmount.setText("" + newValue.intValue()));
    }
}
