package control.graphicsMenus;

public abstract class GraphicalVillageControl {
    protected GameVillage gameVillage;

    public GameVillage getGameVillage() {
        return gameVillage;
    }

    public void setGameVillage(GameVillage gameVillage) {
        this.gameVillage = gameVillage;
    }

    public void back() {
        gameVillage.back();
    }

    public abstract void init();
}
