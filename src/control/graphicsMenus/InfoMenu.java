package control.graphicsMenus;

import constants.BookOfConstants;
import constants.FXMLConstants;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import model.exceptions.NotEnoughMoneyException;
import model.game.building.Building;
import model.game.building.village.Barracks;
import model.game.building.village.TownHall;

import static constants.EntityNames.*;

public class InfoMenu extends BuildingMenu {


    public Label levelLabel;
    public Label healthLabel;
    public Label upgradeLabel;


    @Override
    public void setBuilding(Building building) {
        super.setBuilding(building);
        bind(levelLabel, building.levelProperty(), "level: ");
        healthLabel.setText("health: " + building.getHealth());
        upgradeLabel.setText("upgrade cost: " + building.upgradeCost());
    }

    public void upgrade() {
        boolean checkTownHall = building instanceof Barracks || building.isDefence();
        try {
            TownHall townHall = gameVillage.getGame().getVillage().getTownHall();
            if (checkTownHall && building.getLevel() >= townHall.getLevel())
                throw new IllegalArgumentException("Can't upgrade. Building level should be less than " +
                        "town hall level");
            gameVillage.getGame().getVillage().upgrade(building.getType(), building.getID());
        } catch (IllegalArgumentException | NotEnoughMoneyException e) {
            new Alert(Alert.AlertType.ERROR, e.getMessage()).show();
        }
    }

    public static String getInfoFxml(int type) {
        switch (BookOfConstants.getName(type)) {
            case GOLD_MINE:
            case ELIXIR_MINE:
            case BARRACKS:
            case CAMP:
                return FXMLConstants.INFO;
        }
        return "";
    }
}
