package control.graphicsMenus;

import constants.BookOfConstants;
import javafx.scene.control.Label;
import model.game.Village;
import model.game.building.Building;
import model.game.building.village.Camp;
import model.game.building.village.storage.Storage;
import model.game.ministry.Treasury;
import model.game.ministry.War;

import java.util.ArrayList;

import static constants.EntityNames.CAMP;
import static constants.EntityNames.ELIXIR_STORAGE;
import static constants.EntityNames.GOLD_STORAGE;

public class ShowResourcesControl extends GraphicalVillageControl {
    public Label goldLabel;
    public Label elixirLabel;

    public Label soldierCapacity;
    public Label soldierHolding;
    public Label goldStorageCapacity;
    public Label elixirStorageCapacity;

    @Override
    public void setGameVillage(GameVillage gameVillage) {
        super.setGameVillage(gameVillage);
        goldLabel.setText("Gold available: " +
                String.valueOf(Treasury.getInstance().getGoldAmount()));
        elixirLabel.setText("Elixir available: " +
                String.valueOf(Treasury.getInstance().getElixirAmount()));
        setSoldierCapacityMessage();
        setStoragesMessage();

    }

    private void setSoldierCapacityMessage() {
        soldierHolding.setText("Soldier count: " + War.getInstance().getNumberOfUnits());
        soldierCapacity.setText("Soldier capacity: " + War.getInstance().getCapacity());
    }

    private void setStoragesMessage() {
        for (int i = 0; i < 2; i++) {
            int capacity = i == 0 ? Treasury.getInstance().getGoldCapacity() : Treasury.getInstance().getElixirCapacity();
            Label capacityLabel = i == 0 ? goldStorageCapacity : elixirStorageCapacity;
            String messagePrefix = i == 0 ? "Gold " : "Elixir ";
            capacityLabel.setText(messagePrefix + "capacity: " + capacity);
        }
    }

    @Override
    public void init() {

    }
}
