package control.graphicsMenus;

import constants.BookOfConstants;
import javafx.scene.control.Label;
import model.game.building.Building;
import model.game.building.IncompleteBuilding;

public class
IncompleteBuildingMenu extends BuildingMenu {
    public Label timeRemaining;

    @Override
    public void setBuilding(Building building) {
        super.setBuilding(building);
        nameLabel.setText("Incomplete " +
                BookOfConstants.getName(getIncomplete().getFutureType()));
        bind(timeRemaining, getIncomplete().timeRemainingProperty(), "Time left: ");
    }

    public IncompleteBuilding getIncomplete() {
        return (IncompleteBuilding) building;
    }
}
