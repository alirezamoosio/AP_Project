package control.graphicsMenus;

import constants.*;
import control.Game;
import control.GameScene;
import javafx.animation.AnimationTimer;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import model.game.map.Position;
import model.game.building.Building;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class GameVillage extends GameScene {
    // FIXME: 6/6/18 NO Hardcode
    public static final int INITIAL_TURN_RATE = 10;
    private static int TURN_RATE = INITIAL_TURN_RATE;
    public ImageView searchIcon;
    private VillageStatusBar villageStatusBar;

    public static void setTurnRate(int turnRate) {
        TURN_RATE = turnRate;
    }

    public static int getTurnRate() {
        return TURN_RATE;
    }

    @Override
    public void setGame(Game game) {
        super.setGame(game);
        villageStatusBar.setAndLoad(game);

    }

    @Override
    protected void cellClick(int i, int j) {
        for (int k = 1; k < roots.size(); k++) {
            roots.remove(k);
        }
        Building building = game.getVillage().getMap().getCell(i, j).getBuilding();
        if (building != null)
            setBuildingMenu(building);
        else {
            NewBuilding newBuildingMenu = (NewBuilding) setMenu(FXMLConstants.NEW_BUILDING);
            newBuildingMenu.setClicked(new Position(i, j));
        }
    }


    public GraphicalVillageControl setMenu(String name) {
        GraphicalVillageControl controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(name));
            AnchorPane root = loader.load();
            splitPane.getItems().set(0, root);
            controller = loader.getController();
            controller.setGameVillage(this);
            if (!(controller instanceof  BuildingMenu | controller instanceof InfoMenu))
                controller.init();
            root.getStylesheets().add(BookOfConstants.getURL(CSSConstants.MENUS).toExternalForm());
            root.getChildren().addAll(villageStatusBar.getBar(), villageStatusBar.getDownBar());
            // FIXME: 6/6/18 why i can't move background color to css file!
            root.setStyle("-fx-background-color: #6f1516");
            roots.push(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return controller;
    }

    public void back() {
        roots.pop();
        if (!roots.isEmpty()) {
            roots.lastElement().getChildren().addAll(villageStatusBar.getBar(), villageStatusBar.getDownBar());
            splitPane.getItems().set(0, roots.lastElement());
            // handling the case of unfocusing all cells when returned to home page
            if (roots.size() == 1)
                unFocus();
        } else {
            setMenu(FXMLConstants.VILLAGE_STATUS_BAR);
        }
    }

    public Game getGame() {
        return game;
    }

    public BuildingMenu setBuildingMenu(Building building) {
        int type = building.getType();
        BuildingMenu control = (BuildingMenu) setMenu(BuildingMenu.getBuildingFxml(type));
        control.setBuilding(building);
        control.init();
        return control;
    }

    public InfoMenu setInfoMenu(Building building) {
        int type = building.getType();
        InfoMenu control = (InfoMenu) setMenu(InfoMenu.getInfoFxml(type));
        control.setBuilding(building);
        control.init();
        return control;
    }

    private void initStatusBar() {
        FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(FXMLConstants.VILLAGE_STATUS_BAR));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        villageStatusBar = loader.getController();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initStatusBar();
//        searchIcon.setImage(new Image(BookOfConstants.getURL(ResourceConstants.IMAGE_SEARCH).toExternalForm()));
        slider.valueProperty().addListener(
                (observable, oldValue, newValue) -> zoom(newValue.doubleValue()));
        setMenu(FXMLConstants.VILLAGE_MAIN);
        timer = new AnimationTimer() {
            private int counter = 0;

            @Override
            public void handle(long now) {
                if (counter > TURN_RATE && game != null) {
                    game.getVillage().nextNormalTurn();
                    counter = 0;
                }
                counter++;
            }
        };
        timer.start();
    }

}
