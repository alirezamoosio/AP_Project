package control.graphicsMenus;

import constants.BookOfConstants;
import constants.ConstantNames;
import constants.FXMLConstants;
import javafx.beans.property.IntegerProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import model.game.building.Building;


import static constants.EntityNames.*;

public class BuildingMenu extends GraphicalVillageControl {
    protected Building building;
    @FXML
    public Label nameLabel;

    public void info() {
        gameVillage.setInfoMenu(building);
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
        nameLabel.setText(building.getName() + " " + building.getID());
    }

    public static String getBuildingFxml(int type) {
        //should return the path of the fxml file of the menu for building of the corresponding type
        // TODO: 6/5/18 implement
        switch (BookOfConstants.getName(type)) {
            case BARRACKS:
                return FXMLConstants.BARRACKS;
            case ELIXIR_MINE:
            case GOLD_MINE:
                return FXMLConstants.MINE;
            case CAMP:
                return FXMLConstants.CAMP;
            case GOLD_STORAGE:
            case ELIXIR_STORAGE:
                return FXMLConstants.STORAGE;
            case TOWNHALL:
                return FXMLConstants.TOWN_HALL;
            case INCOMPLETE_BUILDING:
                return FXMLConstants.INCOMPLETE_BUILDING;
        }
        if (ConstantNames.isDefenseType(type))
            return FXMLConstants.INFO;
        return "";
    }

    @Override
    public void init() {
        System.out.println("parent init called");
    }

    public static void bind(Label label, IntegerProperty property, String prefix) {
        label.setText(prefix + String.valueOf(property.get()));
        property.addListener(observable -> label.setText(prefix + String.valueOf(property.get())));
    }
    public static void bind(Label label, IntegerProperty property) {
        bind(label, property, "");
    }
}
