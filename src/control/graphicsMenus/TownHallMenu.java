package control.graphicsMenus;

import javafx.scene.control.Label;
import model.game.building.Building;
import model.game.building.village.TownHall;
import model.game.ministry.Civil;

public class TownHallMenu extends InfoMenu {
    public Label numWorker;

    @Override
    public void setBuilding(Building building) {
        super.setBuilding(building);
        bind(numWorker, gameVillage.getGame().getVillage().getCivil().freeWorkerProperty(), "Free workers: ");
        nameLabel.setText("Town hall");
    }
}
