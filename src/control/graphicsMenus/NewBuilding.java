package control.graphicsMenus;

import constants.BookOfConstants;
import javafx.collections.FXCollections;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import model.exceptions.BuildingTowerException;
import model.exceptions.InvalidCoordinatesException;
import model.exceptions.OutOfWorkerException;
import model.game.map.Position;
import model.game.Village;
import model.game.building.IncompleteBuilding;
import model.game.map.Map;
import model.game.ministry.Civil;

import java.util.ArrayList;
import java.util.Comparator;

import static constants.ConstantNames.ELIXIR_REQUIRED;
import static constants.ConstantNames.GOLD_REQUIRED;

public class NewBuilding extends GraphicalVillageControl {

    public ListView<String> buildingList;
    public Label elixirLabel;
    public Label goldLabel;
    private Map map;
    private Position clicked;

    @Override
    public void init() {

    }


    @Override
    public void setGameVillage(GameVillage gameVillage) {
        super.setGameVillage(gameVillage);

        ArrayList<String> availableBuildings = gameVillage.getGame().getVillage().getTownHall().availableBuildings();
        Civil civil = Civil.getInstance();
        availableBuildings.sort(Comparator.naturalOrder());
        buildingList.setItems(FXCollections.observableArrayList(availableBuildings));
        buildingList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                goldLabel.setText("");
                elixirLabel.setText("");
                return;
            }
            int goldCost = BookOfConstants.getConstant(GOLD_REQUIRED, newValue);
            int elixirCost = BookOfConstants.getConstant(ELIXIR_REQUIRED, newValue);
            goldLabel.setText("Gold: " + goldCost);
            elixirLabel.setText("Elixir: " + elixirCost);
        });
        map = gameVillage.getGame().getVillage().getMap();
    }

    public void build() {
        Village village = gameVillage.getGame().getVillage();
        Civil civil = village.getCivil();
        try {
            String buildingName = buildingList.getSelectionModel().getSelectedItem();
            if (!civil.hasAvailableWorker()) {
                throw new OutOfWorkerException();
            }
            int x = clicked.getX();
            int y = clicked.getY();
            IncompleteBuilding building = village.build(x, y, BookOfConstants.getType(buildingName));
        }  catch (BuildingTowerException | OutOfWorkerException e) {
            new Alert(Alert.AlertType.ERROR, e.getMessage()).show();
        } catch (InvalidCoordinatesException e) {
            new Alert(Alert.AlertType.ERROR, "You can't build here!").show();
        }
    }

    public Position getClicked() {
        return clicked;
    }

    public void setClicked(Position clicked) {
        this.clicked = clicked;
    }
}
