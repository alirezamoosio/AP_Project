package control.graphicsMenus;

import constants.BookOfConstants;
import constants.FXMLConstants;
import control.multiMenus.MultiPlayerGUI;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
import view.View;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class VillageMain extends GraphicalVillageControl implements Initializable {

    public void showBuildings() {
//        gameVillage.setMenu(FXMLConstants.BUILDING_LIST);
        new Alert(Alert.AlertType.ERROR, "do todo").show();
    }

    public void showResources() {
        gameVillage.setMenu(FXMLConstants.SHOW_RESOURCES);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        init();
    }

    @Override
    public void init() {

    }

    public void attack(MouseEvent event) throws IOException {
        View.initAttack();
        FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(FXMLConstants.START_ATTACK));
        View.getInstance().pushAttackScene(loader.load());

        GraphicalVillageControl controller = loader.getController();
        controller.setGameVillage(gameVillage);
        controller.init();
    }

    public void startMulti(MouseEvent mouseEvent) throws IOException {
        View.initMulti();
        FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(FXMLConstants.START_MULTI));
        View.getInstance().pushMultiScene(loader.load());
        ((MultiPlayerGUI) loader.getController()).setGame(gameVillage.getGame());
    }
}
