package control.graphicsMenus;

import constants.BookOfConstants;
import javafx.scene.control.Label;
import model.game.Village;
import model.game.building.Building;
import model.game.building.village.Camp;
import model.game.building.village.storage.Storage;

import java.util.ArrayList;

import static constants.EntityNames.CAMP;
import static constants.EntityNames.ELIXIR_STORAGE;
import static constants.EntityNames.GOLD_STORAGE;

public class CampInfoMenu extends InfoMenu {
    public Label capacityLabel;
    public Label holdingLabel;

    @Override
    public void setBuilding(Building building) {
        // TODO: 6/6/18 redesign. isn't calling super method because it can't be upgraded
        this.building = building;
        levelLabel.setText("level: " + building.getLevel());
        healthLabel.setText("health: " + building.getHealth());
        holdingLabel.setText("Number of units: " + getCamp().getNumberOfUnits());
        capacityLabel.setText("capacity: " + getCamp().getCapacity());
    }


    public Camp getCamp() {
        return (Camp) building;
    }
}
