package control.graphicsMenus;

import constants.BookOfConstants;
import constants.ConstantNames;
import constants.ResourceConstants;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import model.game.building.village.Barracks;

import java.util.HashMap;

public class BarracksStatusMenu extends BuildingMenu {
    public Label nameLabel;
    @FXML
    private Label timeRemaining, currentUnit;
    @FXML
    private GridPane pane;

    @Override
    public void init() {
        Barracks barracks = (Barracks) building;
        String workingMessage, timeRemeainingMessage;
        try {
            workingMessage = BookOfConstants.getName(barracks.getCurrentUnit().getType());
            timeRemeainingMessage = "" + barracks.getCurrentUnit().getTimeRemaining();
        } catch (NullPointerException e) {
            workingMessage  ="";
            timeRemeainingMessage = "";
        }
        currentUnit.setText("Working on: " + workingMessage);
        timeRemaining.setText("Time ramining: " + timeRemeainingMessage);
        HashMap<Integer, Integer> waitingListMap = new HashMap<>();
        HashMap<Integer, Integer> doneListMap = new HashMap<>();
        for (Integer type : barracks.getWaitingList()) {
            waitingListMap.merge(type, 1, (a, b) -> a + b);
        }
        for (Integer type : barracks.getDoneList()) {
            doneListMap.merge(type, 1, (a, b) -> a + b);
        }
        int[] types = ConstantNames.getAllSoldierTypes();
        for (int i = 0; i < types.length; i++) {
            pane.add(new ImageView(new Image(BookOfConstants.getURL(ResourceConstants.getUnitIcon(types[i])).toExternalForm())), 0, i);
            Label name = new Label(BookOfConstants.getName(types[i]));
            Label waitingListCount = new Label(waitingListMap.get(types[i]).toString());
            Label doneListCount = new Label(doneListMap.get(types[i]).toString());
            name.setId("unit");
            waitingListCount.setId("unit");
            doneListCount.setId("unit");
            pane.add(name, 1, i);
            pane.add(waitingListCount, 2, i);
            pane.add(doneListCount, 3, i);
        }
    }
}
