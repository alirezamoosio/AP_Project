package control.graphicsMenus;

import constants.FXMLConstants;
import model.game.building.village.Barracks;

public class BarracksMenu extends InfoMenu {

    public Barracks getBarracks() {
        return (Barracks) getBuilding();
    }

    public void buildSoldiers() {
        SoldierList soldierList = (SoldierList) gameVillage.setMenu(FXMLConstants.SOLDIER_LIST);
        soldierList.setBuilding(building);
        soldierList.init();
    }

    public void status() {
        BarracksStatusMenu barracksStatusMenu = (BarracksStatusMenu) gameVillage.setMenu(FXMLConstants.BARRACKS_STATUS);
        barracksStatusMenu.setBuilding(building);
        barracksStatusMenu.init();
    }

}
