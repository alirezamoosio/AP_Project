package control;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import model.game.Village;

import java.util.HashMap;

public class Game {
    private Village village;
    private String name;
    private String jsonPath;
    private HashMap<String, String> mapsLoaded = new HashMap<>();

    public Game() {
        village = new Village();
    }

    public Game(String name) {
        this.name = name;
        village = new Village();
    }

    public Village getVillage() {
        return village;
    }

    public String getName() {
        return name;
    }

    public String getJsonPath() {
        return jsonPath;
    }

    public HashMap<String, String> getMapsLoaded() {
        return mapsLoaded;
    }

    public String getMapPath (String mapName) {
        return mapsLoaded.get(mapName);
    }

    public void setVillage(Village village) {
        this.village = village;
    }

    public void setJsonPath(String jsonPath) {
        this.jsonPath = jsonPath;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addLoadedMapPath(String name, String path) {
        mapsLoaded.put(name, path);
    }

}
