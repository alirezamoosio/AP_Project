package control.onlineUdp;

import control.GameFight;
import control.attackMenus.GameAttack;
import control.attackMenus.GraphicalAttackControl;
import javafx.scene.image.ImageView;
import model.game.entity.Entity;
import model.game.map.Map;
import network.udp.EntityMessage;
import network.udp.MapSender;

import java.lang.reflect.Array;
import java.net.InetAddress;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.ResourceBundle;

public class OnlineAttack extends GameAttack {

    MapSender mapSender = null;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        moveMade.addListener((observable, oldValue, newValue) -> {
            System.err.println("move made change to " + newValue.toString());
            if (newValue && mapSender != null) {
                mapSender.addMap(getAttackingMap().turnToMessages());
                moveMade.set(false);
            }
        });
    }

    @Override
    protected void destroyEntity(Entity entity, ImageView imageView) {
        super.destroyEntity(entity, imageView);
        if (mapSender != null) {
            EntityMessage message = new EntityMessage(entity);
            message.setNewHealth(0);
            mapSender.addMap(Collections.singletonList(message));
        }
    }

    private Map getAttackingMap() {
        return getGame().getVillage().getAttackingMap();
    }

    public void setOnlineDefender(InetAddress address, int port) {
        mapSender = new MapSender(address, port);
        new Thread(mapSender).start();
    }
}
