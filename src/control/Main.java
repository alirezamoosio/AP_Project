package control;


import constants.BookOfConstants;
import constants.FXMLConstants;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import view.View;

public class Main extends Application {

    public static void main(String[] args) {
//        Scanner inputScanner = new Scanner(System.in);
//        View view = new View(inputScanner);
//        BookOfConstants.load();
//        StartGameMenu startGameMenu = new StartGameMenu(view);
//        startGameMenu.start();
//        Game game = startGameMenu.getGame();
//        if (game != null) {
//            VillageMainMenu villageMainMenu = new VillageMainMenu(game, view);
//            villageMainMenu.start();
//        }
//        inputScanner.close();
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        View.initView(primaryStage);
        BookOfConstants.load();
        FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(FXMLConstants.START));
        View.getInstance().pushScene(loader.load());
    }
}
