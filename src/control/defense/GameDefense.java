package control.defense;

import constants.BookOfConstants;
import constants.CSSConstants;
import control.Game;
import control.GameFight;
import control.attackMenus.GraphicalAttackControl;
import javafx.animation.AnimationTimer;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import model.game.Village;
import model.game.building.Building;
import model.game.entity.Entity;
import model.game.map.Cell;
import model.game.map.Map;
import model.game.ministry.Civil;
import model.game.ministry.Treasury;
import model.game.unit.Unit;
import network.udp.EntityMessage;
import network.udp.MapReceiver;

import java.io.IOException;
import java.net.SocketException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class GameDefense extends GameFight {

    private MapReceiver mapReceiver;
    private static final int TURN_RATE = 10;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        slider.valueProperty().addListener(
                (observable, oldValue, newValue) -> zoom(newValue.doubleValue()));
        setGame(new Game());
        timer = new AnimationTimer() {
            private int counter = 0;

            @Override
            public void handle(long now) {
                if (counter > TURN_RATE) {
                    if (mapReceiver == null)
                        return;
                    while (mapReceiver.hasMessage()) {
                        EntityMessage message = mapReceiver.getNewMessage();
                        if (!message.isEnd()) {
                            map.changeWithMessage(message);
                        } else {
                            for (List<Unit> units : map.getUnitMap().values())
                                for (Unit unit : units)
                                    unit.destroy();
                            for (List<Building> buildings : map.getBuildingsMap().values())
                                for (Building building : buildings)
                                    building.restore();
                            Village.getInstance().setMap(map);
                            Civil.getInstance().setMap(map);
                        }
                    }
                }
                counter++;
            }
        };
        timer.start();
    }

    public void setPort(int port) {
        try {
            mapReceiver = new MapReceiver(port);
            new Thread(mapReceiver).start();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void cellClick(int x, int y) {
        Cell cell = map.getCell(x, y);
        if (cell.hasVisibleBuilding()) {
            setBuildingStatus(cell.getBuilding());
        } else {
            if (cell.hasUnit()) {
                setUnitStatus(cell.getUnits().get(0));
            } else {
                if (!roots.isEmpty())
                    back();
            }
        }
    }

    @Override
    protected GraphicalAttackControl setMenu(String name) {
        GraphicalAttackControl controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(name));
            AnchorPane root = loader.load();
            splitPane.getItems().set(0, root);
            controller = loader.getController();
            controller.setGameFight(this);
            root.getStylesheets().add(BookOfConstants.getURL(CSSConstants.MENUS).toExternalForm());
            root.setStyle("-fx-background-color: #6f1516");
            roots.push(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return controller;
    }

    @Override
    public void back() {
        roots.pop();
        if (!roots.isEmpty()) {
            splitPane.getItems().set(0, roots.lastElement());
            if (roots.size() == 1)
                unFocus();
        }
    }

    @Override
    public Map getMap() {
        Map oldMap = Civil.getInstance().getMap();
        return new Map(oldMap.getWidth(), oldMap.getHeight());
    }
}
