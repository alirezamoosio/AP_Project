package control;

import constants.FXMLConstants;
import control.attackMenus.BuildingStatus;
import control.attackMenus.GraphicalAttackControl;
import control.attackMenus.UnitStatus;
import model.game.building.Building;
import model.game.unit.Unit;

public abstract class GameFight extends GameScene {
    @Override
    protected void cellClick(int x, int y) {

    }

    protected BuildingStatus setBuildingStatus(Building building) {
        for (int k = 1; k < roots.size(); k++) {
            roots.remove(k);
        }
        BuildingStatus menu = (BuildingStatus) setMenu(FXMLConstants.BUILDING_STATUS);
        menu.setBuilding(building);
        return menu;
    }

    public UnitStatus setUnitStatus(Unit unit) {
        for (int k = 1; k < roots.size(); k++) {
            roots.remove(k);
        }
        UnitStatus menu = (UnitStatus) setMenu(FXMLConstants.UNIT_STATUS);
        menu.setUnit(unit);
        return menu;
    }

    protected abstract GraphicalAttackControl setMenu(String name);
}
