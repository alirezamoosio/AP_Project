package control.startMenus;

import com.gilecode.yagson.YaGson;
import constants.BookOfConstants;
import constants.FXMLConstants;
import constants.ResourceConstants;
import constants.Visual;
import control.Game;
import control.GameScene;
import control.graphicsMenus.GameVillage;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import model.game.ministry.Civil;
import model.game.ministry.Treasury;
import model.game.ministry.War;
import view.View;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;

public class StartGameMenu implements Initializable{
    @FXML
    private ImageView loading;

    @FXML
    private void newGameScene() throws IOException {
        FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(FXMLConstants.NEW_GAME));
        View.getInstance().pushScene(loader.load());
    }

    @FXML
    private void newLoadScene() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")+File.separator+"Desktop"));
        fileChooser.setTitle("Select Saved Json File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Json","*.json"));
        File file = fileChooser.showOpenDialog(View.getInstance().getPrimaryStage());
        try {
            Scanner scanner = new Scanner(file);
            YaGson yaGson = new YaGson();
            String gameString = scanner.nextLine();
            Game game = yaGson.fromJson(gameString, Game.class);
            load(game);
        } catch (Exception e) {
            e.printStackTrace();
            new Alert(Alert.AlertType.ERROR, "Not a valid file").show();
        }
    }

    private void load(Game game) {
        Treasury.setInstance(game.getVillage().getTreasury());
        Civil.setInstance(game.getVillage().getCivil());
        War.setInstance(game.getVillage().getWar());
        Task<FXMLLoader> loadTask = new Task<FXMLLoader>() {
            @Override
            protected FXMLLoader call() throws Exception {
                return new FXMLLoader(BookOfConstants.getURL(FXMLConstants.GAME_VILLAGE));
            }
        };
        loadTask.setOnSucceeded(event -> {
            FXMLLoader loader = loadTask.getValue();
            try {
                View.getInstance().pushScene(new Scene(loader.load(), Visual.GAME_WIDTH, Visual.GAME_HEIGHT), true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            GameVillage controller = loader.getController();
            controller.setGame(game);
        });
        new Thread(loadTask).start();
        loading.setVisible(true);
    }

    @FXML
    private void quit() {
        View.getInstance().close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loading.setImage(new Image(BookOfConstants.getURL(ResourceConstants.IMAGE_LOADING).toExternalForm()));
    }

    public void settings() throws IOException {
        FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(FXMLConstants.SETTINGS));
        View.getInstance().pushScene(loader.load());
    }
}
