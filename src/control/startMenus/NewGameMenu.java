package control.startMenus;

import constants.BookOfConstants;
import constants.FXMLConstants;
import constants.ResourceConstants;
import constants.Visual;
import control.Game;
import control.graphicsMenus.GameVillage;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import view.View;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class NewGameMenu implements Initializable {
    static BooleanProperty isLoaded = new SimpleBooleanProperty(true);
    @FXML
    private TextField gameName;
    @FXML
    private ImageView loading;

    @FXML
    private void back() {
        View.getInstance().popScene(false);
    }

    @FXML
    private void action() {
        if (gameName.getCharacters().toString().contains(" ")) {
            new Alert(Alert.AlertType.ERROR, "Name cannot have white space").show();
            return;
        }
        Task<FXMLLoader> loadTask = new Task<FXMLLoader>() {
            @Override
            protected FXMLLoader call() {
                return new FXMLLoader(BookOfConstants.getURL(FXMLConstants.GAME_VILLAGE));
            }
        };
        loadTask.setOnSucceeded(e -> {
            FXMLLoader loader = loadTask.getValue();
            Game game = new Game(gameName.getCharacters().toString());
            try {
                View.getInstance().pushScene(new Scene(loader.load(), Visual.GAME_WIDTH, Visual.GAME_HEIGHT), true);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            GameVillage controller = loader.getController();
            controller.setGame(game);
        });
        new Thread(loadTask).start();
        loading.setVisible(true);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loading.setImage(new Image(BookOfConstants.getURL(ResourceConstants.IMAGE_LOADING).toExternalForm()));
    }
}


