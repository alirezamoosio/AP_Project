package control.startMenus;

import control.attackMenus.GameAttack;
import control.graphicsMenus.GameVillage;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import view.View;

import java.net.URL;
import java.util.ResourceBundle;

public class SettingsMenu implements Initializable {
    public Slider villageRateSlider, attackRateSlider;
    public Label villageRate, attackRate;
    public CheckBox mute;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        villageRate.setText("Village Rate: " + GameVillage.INITIAL_TURN_RATE);
        attackRate.setText("Attack Rate: " + GameAttack.INITIAL_TURN_RATE);
        villageRateSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            GameVillage.setTurnRate((int) (newValue.intValue() * 0.9 + GameVillage.INITIAL_TURN_RATE));
            villageRate.setText("Village Rate: " + GameVillage.getTurnRate());
        });
        attackRateSlider
                .valueProperty().addListener((observable, oldValue, newValue) -> {
            GameAttack.setTurnRate((int) (newValue.intValue() * 0.9 + GameAttack.INITIAL_TURN_RATE));
            attackRate.setText("Attack Rate: " + GameAttack.getTurnRate());
        });
        mute.setSelected(!View.getInstance().isPlayingMusic());
    }

    @FXML
    private void handleMute() {
        if (View.getInstance().isPlayingMusic())
            View.getInstance().pauseMusic();
        else
            View.getInstance().playMusic();
    }

    public void back() {
        View.getInstance().popScene(false);
    }
}
