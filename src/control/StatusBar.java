package control;

import constants.BookOfConstants;
import constants.ResourceConstants;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import java.net.URL;
import java.util.ResourceBundle;

public class StatusBar implements Initializable{
    public HBox bar;
    public ImageView goldIcon, elixirIcon;
    public Label goldAmount, elixirAmount;
    protected Game game;

    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        goldIcon.setImage(new Image(BookOfConstants.getURL(ResourceConstants.IMAGE_GOLD_ICON).toExternalForm()));
        elixirIcon.setImage(new Image(BookOfConstants.getURL(ResourceConstants.IMAGE_ELIXIR_ICON).toExternalForm()));
    }
}
