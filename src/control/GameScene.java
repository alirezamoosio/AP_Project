package control;

import constants.BookOfConstants;
import constants.ConstantNames;
import constants.ResourceConstants;
import constants.Visual;
import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import model.game.Village;
import model.game.VillageMode;
import model.game.building.Building;
import model.game.building.defense.Trap;
import model.game.building.village.storage.Storage;
import model.game.entity.Entity;
import model.game.map.Cell;
import model.game.map.Map;
import model.game.map.Point;
import model.game.map.Position;
import model.game.ministry.Treasury;
import model.game.unit.Direction;
import model.game.unit.Unit;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Stack;

import static constants.EntityNames.ELIXIR_STORAGE;
import static constants.EntityNames.GOLD_STORAGE;

public abstract class GameScene implements Initializable {
    //    public GridPane gridPane;
    public Pane gamePane;
    public SplitPane splitPane;
    public Slider slider;
    protected Stack<AnchorPane> roots = new Stack<>();
    protected Game game;
    protected AnimationTimer timer;
    protected Map map;
    private double cellWidth = 100;
    private double cellHeight = 100;
    private Image unfocused = new Image(BookOfConstants.getURL(ResourceConstants.IMAGE_UNFOCUSED).toExternalForm(), cellWidth, cellHeight, false, true);
    private Image focused = new Image(BookOfConstants.getURL(ResourceConstants.IMAGE_FOCUSED).toExternalForm());
    private Image borderImage = new Image(BookOfConstants.getURL(ResourceConstants.IMAGE_BORDER).toExternalForm());
    private ImageView[][] backgrounds = new ImageView[Village.MAP_SIZE][Village.MAP_SIZE];
    private Image[][][] units = new Image[ConstantNames.getAllSoldierTypes().length][Direction.DIRECTIONS][Direction.GIF_IMAGES];
    private Cell focusedCell;

    public void setGame(Game game) {
        this.game = game;
        ResourceConstants.setUnitImages(units, cellWidth, cellHeight);
        map = getMap();
//        double width = Visual.GAME_WIDTH - Visual.SIDE_MENU_WIDTH;
//        double height = Visual.GAME_HEIGHT;
        addMap(map);
        gamePane.setOnMouseClicked(event -> {
            double x = event.getX();
            double y = event.getY();
            int xInt = (int) (x / cellWidth);
            int yInt = (int) (y / cellHeight);
            //border case
            if (xInt == 0 || xInt == map.getWidth() || yInt == 0 || yInt == map.getHeight())
                return;
            xInt--;
            yInt--;
            if (focusedCell != null)
                backgrounds[focusedCell.getPosition().getX()][focusedCell.getPosition().getY()].setImage(unfocused);
            focusedCell = map.getCell(xInt, yInt);
            backgrounds[focusedCell.getPosition().getX()][focusedCell.getPosition().getY()].setImage(focused);
            cellClick(xInt, yInt);
        });
        map.setListener(this::addEntityImage);
    }

    public Map getMap() {
        return
                game.getVillage().getMode() == VillageMode.NORMAL ?
                        game.getVillage().getMap() :
                        game.getVillage().getAttackingMap();
    }
    protected void addMap(Map map) {
        for (int i = 0; i < map.getWidth(); i++) {
            for (int j = 0; j < map.getHeight(); j++) {
                backgrounds[i][j] = new ImageView(unfocused);
                putImageView(backgrounds[i][j], new Point(i, j));
                gamePane.getChildren().add(backgrounds[i][j]);
                if (!map.getCell(i, j).hasBuilding())
                    continue;
                addEntityImage(map.getCell(i, j).getBuilding());
            }
        }
        for (int i = -1; i < map.getWidth(); i++) {
            ImageView border = new ImageView(borderImage);
            putImageView(border, new Point(-1, i));
            gamePane.getChildren().add(border);
        }
        for (int i = -1; i < map.getWidth(); i++) {
            ImageView border = new ImageView(borderImage);
            putImageView(border, new Point(map.getHeight(), i));
            gamePane.getChildren().add(border);
        }
        for (int i = -1; i <= map.getHeight(); i++) {
            ImageView border = new ImageView(borderImage);
            putImageView(border, new Point(i, -1));
            gamePane.getChildren().add(border);
        }
        for (int i = -1; i <= map.getHeight(); i++) {
            ImageView border = new ImageView(borderImage);
            putImageView(border, new Point(i, map.getWidth()));
            gamePane.getChildren().add(border);
        }
    }
    protected abstract void cellClick(int x, int y);

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void addEntityImage(Entity entity) {
        URL imageURL = BookOfConstants.getURL(ResourceConstants.getEntityIcon(entity));
        Image image = new Image(imageURL.toExternalForm(),
                cellWidth,
                cellHeight,
                false,
                true);
        ImageView imageView = new ImageView();
        imageView.setImage(image);
        Position position = entity.getPosition();
        int x = position.getX();
        int y = position.getY();
        putImageView(imageView, new Point(x, y));
//        imageView.setFitWidth(cellWidth);
//        imageView.setFitHeight(cellHeight);
        if (game.getVillage().getMode() == VillageMode.NORMAL || !(entity instanceof Trap))
            gamePane.getChildren().addAll(imageView);
        entity.isDestroyedProperty().addListener((observable, oldValue, newValue) -> {
            destroyEntity(entity, imageView);
        });
        entity.levelProperty().addListener(observable -> {
            URL newURL = BookOfConstants.getURL(ResourceConstants.getEntityIcon(entity));
            Image newImage = new Image(newURL.toExternalForm(),
                    cellWidth,
                    cellHeight,
                    false,
                    false);
            imageView.setImage(newImage);
        });
        // units have to move continuously so next listener is only for buildings!
        if (entity instanceof Building)
            entity.positionProperty().addListener((observable, oldValue, newValue) -> {
                Position newPosition = (Position) newValue;
                putImageView(imageView, newPosition.toPoint());
            });
        else {
            Unit unit = (Unit) entity;
            unit.setUnitImage(imageView);
            unit.PointProperty().addListener((observable, oldValue, newValue) -> {
                if (unit.getDirection() != null)
                    imageView.setImage(units[unit.getType() - ConstantNames.getAllSoldierTypes()[0]][unit.getDirection().getIndex()][unit.getGifIndex()]);
                Point point = (Point) newValue;
                putImageView(imageView, point);
            });
        }
    }

    protected void destroyEntity(Entity entity, ImageView imageView) {
        System.err.println((BookOfConstants.getName(entity.getType()) + " died"));
        if (!(entity instanceof Trap)) {
            gamePane.getChildren().remove(imageView);
        } else {
            gamePane.getChildren().add(imageView);
        }
        if (entity instanceof Building && !(entity instanceof Trap) && game.getVillage().getMode() == VillageMode.ATTACKING) {
            Treasury.getInstance().addGainedResource(BookOfConstants.getConstant(ConstantNames.GOLD_REQUIRED, entity.getType()),
                    BookOfConstants.getType(GOLD_STORAGE));
            Treasury.getInstance().addGainedResource(BookOfConstants.getConstant(ConstantNames.ELIXIR_REQUIRED, entity.getType()),
                    BookOfConstants.getType(ELIXIR_STORAGE));
            if (entity.getType() == BookOfConstants.getType(GOLD_STORAGE) || entity.getType() == BookOfConstants.getType(ELIXIR_STORAGE)) {
                Treasury.getInstance().addToStorage(((Storage) entity).getHolding(), entity.getType());
                ((Storage) entity).emptyStorage();
            }
            game.getVillage().getPrize(entity.getType());
        }

    }

    @FXML
    protected void zoom(double newFactor) {
        double prevWidth = cellWidth;
        double prevHeight = cellHeight;
        cellWidth = Visual.MAP_WIDTH / (1.0 * map.getWidth()) + 50 * newFactor;
        cellHeight = Visual.MAP_HEIGHT / (1.0 * map.getHeight()) + 50 * newFactor;
        for (Node node : gamePane.getChildren()) {
            ImageView imageView = (ImageView) node;
//            imageView.setFitWidth(cellWidth);
//            imageView.setFitHeight(cellHeight);
            putImageView(imageView, getModelPoint(imageView, new Point(prevWidth, prevHeight)));
        }
        gamePane.setPrefWidth((map.getWidth() + 2) * cellWidth);
        gamePane.setPrefHeight((map.getHeight() + 2) * cellHeight);
    }

    protected void unFocus() {
        if (focusedCell != null)
            backgrounds[focusedCell.getPosition().getX()][focusedCell.getPosition().getY()].setImage(unfocused);
    }

    public Game getGame() {
        return game;
    }

    public abstract void back();

    protected void putImageView(ImageView imageView, Point modelPoint) {
        imageView.setLayoutX((modelPoint.getX() + 1) * cellWidth);
        imageView.setLayoutY((modelPoint.getY() + 1) * cellHeight);
        imageView.setFitWidth(cellWidth);
        imageView.setFitHeight(cellHeight);
    }

    protected Point getLayoutPoint(ImageView imageView) {
        return new Point(imageView.getLayoutX(), imageView.getLayoutY());
    }

    protected Point getModelPoint(ImageView imageView, Point ratios) {
        return getLayoutPoint(imageView).pairWiseSubstract(ratios).pairWiseDivide(ratios);
    }
}
