package control.attackMenus;

import control.GameFight;
import control.graphicsMenus.GameVillage;

public abstract class GraphicalAttackControl {
    protected GameFight gameFight;

    public void setGameFight(GameFight gameFight) {
        this.gameFight = gameFight;
    }

    public void back() {
        gameFight.back();
    }

}
