package control.attackMenus;

import com.google.gson.Gson;
import constants.*;
import control.Game;
import control.graphicsMenus.GraphicalVillageControl;
import control.onlineUdp.OnlineAttack;
import io.JsonMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import model.exceptions.NotEnoughUnitException;
import model.game.VillageMode;
import model.game.ministry.Treasury;
import model.game.ministry.War;
import model.game.unit.Unit;
import network.tcp.Guest;
import view.View;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import static constants.EntityNames.ELIXIR_STORAGE;
import static constants.EntityNames.GOLD_STORAGE;

//import io.JsonMap;

public class StartAttackMenu extends GraphicalVillageControl {
    public GridPane pane;
    private Game game;

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    private Spinner[] counts = new Spinner[ConstantNames.getAllSoldierTypes().length];
    private HashMap<Integer, Spinner> soldierSpinners = new HashMap<>();

    @Override
    public void init() {
        int[] types = ConstantNames.getAllSoldierTypes();
        for (int i = 0; i < types.length; i++) {
            counts[i] = new Spinner(0, War.getInstance().getNumberOfUnits(types[i]), 0);
//            counts[i].setPromptText("Number");
            ImageView imageView = new ImageView(new Image(BookOfConstants.getURL(ResourceConstants.getUnitIcon(types[i])).toExternalForm()));
            pane.add(imageView, i, 0);
            Label count = new Label(War.getInstance().getNumberOfUnits(types[i]) + "");
            GridPane.setHalignment(imageView, HPos.CENTER);
            GridPane.setHalignment(count, HPos.CENTER);
            pane.add(count, i, 1);
            pane.add(counts[i], i, 2);
            soldierSpinners.put(types[i], counts[i]);
        }
    }

    public void load(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(View.getInstance().getAttackStage());
        try {
            Scanner scanner = new Scanner(file);
            String jsonString = scanner.nextLine();
            Gson gson = new Gson();
            game.getVillage().setAttackingMap((gson.fromJson(jsonString, JsonMap.class).convert()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            new Alert(Alert.AlertType.ERROR, "File is not valid").show();
        }
    }

    public void start() throws IOException {
        FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(FXMLConstants.ONLINE_ATTACK));
        View.getInstance().pushMultiScene(new Scene(loader.load(), Visual.GAME_WIDTH, Visual.GAME_HEIGHT), true);
        OnlineAttack controller = loader.getController();
        HashMap<Integer, ArrayList<Unit>> selectedUnits = new HashMap<>();
        for (int type : soldierSpinners.keySet()) {
            try {
                selectedUnits.put(type, War.getInstance().extractUnit(BookOfConstants.getName(type),
                        (Integer) soldierSpinners.get(type).getValue()));
            } catch (NotEnoughUnitException e) {
                e.printStackTrace();
            }
        }
        game.getVillage().setSelectedUnits(selectedUnits);
        game.getVillage().setMode(VillageMode.ATTACKING);
        controller.setGame(game);
        controller.setOnlineDefender(InetAddress.getByName(Guest.getInstance().getServerIP()), Guest.getInstance().getDefensePort());
        View.getInstance().setOnAttackCloseRequest(() -> {
            HashMap<Integer, ArrayList<Unit>> attacking = game.getVillage().getAttackingUnits();
            HashMap<Integer, ArrayList<Unit>> selected = game.getVillage().getSelectedUnits();
            ArrayList<Unit> comingBack = new ArrayList<>();
            for (ArrayList<Unit> units : attacking.values())
                for (Unit unit : units)
                    if (!unit.isDestroyed())
                        comingBack.add(unit);
            for (ArrayList<Unit> units : selected.values())
                for (Unit unit : units)
                    if (!unit.isDestroyed())
                        comingBack.add(unit);
            War.getInstance().unitsToCamp(comingBack);
            Treasury.getInstance().addToStorage(Treasury.getInstance().getGoldGainedProperty().get(), BookOfConstants.getType(GOLD_STORAGE));
            Treasury.getInstance().addToStorage(Treasury.getInstance().getElixirGainedProperty().get(), BookOfConstants.getType(ELIXIR_STORAGE));
            Treasury.getInstance().resetGainedResource();
            game.getVillage().setMode(VillageMode.NORMAL);
        });
    }

    @Override
    public void back() {
        View.getInstance().popMultiScene(false);
    }
}
