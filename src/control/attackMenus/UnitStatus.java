package control.attackMenus;

import control.graphicsMenus.BuildingMenu;
import javafx.scene.control.Label;
import model.game.unit.Unit;

public class UnitStatus extends GraphicalAttackControl {
    public Label healthLabel;
    public Label levelLabel;
    public Label description;

    public void setUnit(Unit unit) {
        BuildingMenu.bind(healthLabel, unit.healthProperty(), "Health: ");
        BuildingMenu.bind(levelLabel, unit.levelProperty(), "level: ");
        description.setText(unit.getName() + " " + (unit.getID() + 1));
    }
}
