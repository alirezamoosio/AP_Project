package control.attackMenus;

import control.graphicsMenus.BuildingMenu;
import javafx.scene.control.Label;
import model.game.building.Building;

public class BuildingStatus extends GraphicalAttackControl {
    public Label levelLabel;
    public Label healthLabel;
    public Label description;

    public void setBuilding(Building building) {
        BuildingMenu.bind(healthLabel, building.healthProperty(), "health: ");
        BuildingMenu.bind(levelLabel, building.levelProperty(), "level: ");
        description.setText(building.getName() + " " + (building.getID() + 1));
    }
}
