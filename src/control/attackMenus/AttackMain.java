package control.attackMenus;

import constants.BookOfConstants;
import constants.ConstantNames;
import constants.ResourceConstants;
import control.GameFight;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import model.exceptions.InvalidClassTypeException;
import model.exceptions.InvalidCoordinatesException;
import model.game.Village;
import model.game.map.Map;
import model.game.unit.Unit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class AttackMain extends GraphicalAttackControl {


    public Label chosenUnitLabel;
    public GridPane pane;
    private HashMap<Integer, ArrayList<Unit>> selectedUnits;
    private HashMap<Integer, ArrayList<Unit>> attackingUnits;
    private Button[] unitNames = new Button[ConstantNames.getAllSoldierTypes().length];
    //    private Unit chosenUnit;
    private Map attackMap;

    @Override
    public void setGameFight(GameFight gameFight) {
        super.setGameFight(gameFight);
        Village village = gameFight.getGame().getVillage();
        selectedUnits = village.getSelectedUnits();
        attackingUnits = village.getAttackingUnits();
        for (int type : ConstantNames.getAllSoldierTypes())
            attackingUnits.put(type, new ArrayList<>());
        int counter = 0;
        int[] types = ConstantNames.getAllSoldierTypes();
        for (int i = 0; i < types.length; i++) {
            ImageView icon = new ImageView(new Image(BookOfConstants.getURL(ResourceConstants.getUnitIcon(types[i])).toExternalForm()));
            unitNames[i] = new Button(BookOfConstants.getName(types[i]) + "       " + selectedUnits.get(types[i]).size());
            unitNames[i].setId("unit");
            unitNames[i].setTextFill(Color.RED);
            final int count = i;
            unitNames[i].setOnMouseClicked(event -> {
                chosenUnitLabel.setText(BookOfConstants.getName(types[count]));
                selectSoldier();
            });
            pane.add(icon, 0, counter);
            pane.add(unitNames[i], 1, counter);
            counter++;
        }
        attackMap = village.getAttackingMap();
    }

    private void selectSoldier() {
        String unitName = chosenUnitLabel.getText();
        int type;
        Unit chosenUnit;
        try {
            type = BookOfConstants.getType(unitName);
            chosenUnit = selectedUnits.get(type).get(0);
        } catch (InvalidClassTypeException e) {
            new Alert(Alert.AlertType.ERROR, "Choose unit").show();
            return;
        } catch (IndexOutOfBoundsException e) {
            new Alert(Alert.AlertType.ERROR, "No units of that kind").show();
            return;
        }
        GameAttack gameAttack = (GameAttack) gameFight;
        gameAttack.setClickAction(clicked -> {
            try {
                attackMap.addUnit(chosenUnit, clicked);
                selectedUnits.get(type).remove(chosenUnit);
                attackingUnits.get(type).add(chosenUnit);
                unitNames[type - ConstantNames.getAllSoldierTypes()[0]].setText(BookOfConstants.getName(type) + "       " + selectedUnits.get(type).size());
                //listener to remove unit from attacking units
                chosenUnit.isDestroyedProperty().addListener((observable, oldValue, newValue) -> {
                    if (newValue)
                        attackingUnits.get(type).remove(chosenUnit);
                });
                //graphical listener for unit stat
                chosenUnit.getUnitImage().setOnMouseClicked(event -> gameAttack.setUnitStatus(chosenUnit));
            } catch (InvalidCoordinatesException e) {
                new Alert(Alert.AlertType.ERROR, "You can only put units at the border").show();
            }
        });
    }

}
