package control.attackMenus;

import constants.BookOfConstants;
import constants.CSSConstants;
import constants.FXMLConstants;
import control.CellClickAction;
import control.Game;
import control.GameFight;
import control.GameScene;
import javafx.animation.AnimationTimer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import model.game.building.Building;
import model.game.map.Cell;
import javafx.scene.layout.VBox;
import model.game.map.Position;
import model.game.unit.Unit;
import view.View;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class GameAttack extends GameFight {

    protected BooleanProperty moveMade = new SimpleBooleanProperty();
    private CellClickAction clickAction = null;
    private AttackStatusBar attackStatusBar;
    public static final int INITIAL_TURN_RATE = 10;
    private static int TURN_RATE = INITIAL_TURN_RATE;

    public static void setTurnRate(int turnRate) {
        TURN_RATE = turnRate;
    }

    public static int getTurnRate() {
        return TURN_RATE;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initStatusBar();
        slider.valueProperty().addListener(
                (observable, oldValue, newValue) -> zoom(newValue.doubleValue()));
        timer = new AnimationTimer() {
            private int counter = 0;

            @Override
            public void handle(long now) {
                if (counter > TURN_RATE) {
                    System.out.println("next defensive turn called");
                    game.getVillage().nextDefensiveTurn();
                    counter = 0;
                    moveMade.set(true);
                }
                counter++;
            }
        };
        timer.start();
        View.getInstance().setOnAttackCloseRequest(() -> timer.stop());
    }

    private void initStatusBar() {
        FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(FXMLConstants.ATTACK_STATUS_BAR));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        attackStatusBar = loader.getController();
    }

    @Override
    public void setGame(Game game) {
        super.setGame(game);
        attackStatusBar.setGame(game);
        setMenu(FXMLConstants.ATTACK_MAIN);
    }

    @Override
    protected void cellClick(int x, int y) {
        if (clickAction != null) {
            clickAction.accept(new Position(x, y));
            clickAction = null;
            // this try catch block is for removing unit name label in select unit grid pane
            try {
                Label unitName = (Label) ((VBox) roots.lastElement().getChildren().get(0)).getChildren().get(0);
                unitName.setText("");
            } catch (NullPointerException | ClassCastException ignored) {
            }
        } else {
            Cell cell = map.getCell(x, y);
            if (cell.hasVisibleBuilding()) {
                setBuildingStatus(cell.getBuilding());
            }
        }
    }





    public void back() {
        roots.pop();
        if (!roots.isEmpty()) {
            roots.lastElement().getChildren().add(attackStatusBar.getBar());
            splitPane.getItems().set(0, roots.lastElement());
            if (roots.size() == 1)
                unFocus();
        } else {
            setMenu(FXMLConstants.ATTACK_STATUS_BAR);
        }
    }

    public GraphicalAttackControl setMenu(String name) {
        GraphicalAttackControl controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(BookOfConstants.getURL(name));
            AnchorPane root = loader.load();
            splitPane.getItems().set(0, root);
            controller = loader.getController();
            controller.setGameFight(this);
            root.getStylesheets().add(BookOfConstants.getURL(CSSConstants.MENUS).toExternalForm());
            root.getChildren().add(attackStatusBar.getBar());
            root.setStyle("-fx-background-color: #6f1516");
            roots.push(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return controller;
    }

    public CellClickAction getClickAction() {
        return clickAction;
    }

    public void setClickAction(CellClickAction clickAction) {
        this.clickAction = clickAction;
    }
}

