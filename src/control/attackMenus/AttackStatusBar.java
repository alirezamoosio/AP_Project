package control.attackMenus;

import control.StatusBar;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import model.game.ministry.Treasury;

import java.net.URL;
import java.util.ResourceBundle;

public class AttackStatusBar extends StatusBar{
    public HBox bar;
    public ImageView goldIcon, elixirIcon;
    public Label goldAmount, elixirAmount;
    private GameAttack gameAttack;

    public HBox getBar() {
        return bar;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        goldAmount.setText("" + Treasury.getInstance().getGoldGainedProperty().get());
        elixirAmount.setText("" + Treasury.getInstance().getElixirGainedProperty().get());
        Treasury.getInstance().getGoldGainedProperty().addListener((observable, oldValue, newValue) -> goldAmount.setText("" + newValue.intValue()));
        Treasury.getInstance().getElixirGainedProperty().addListener((observable, oldValue, newValue) -> elixirAmount.setText("" + newValue.intValue()));
    }
}