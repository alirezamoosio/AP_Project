package view;

@FunctionalInterface
public interface Request {
    void accept();
}
