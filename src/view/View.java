package view;

import constants.BookOfConstants;
import constants.ResourceConstants;
import constants.Visual;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class View {
    private Stack<Scene> primaryScenes = new Stack<>();
    private Stack<Scene> attackScenes = new Stack<>();
    private Stack<Scene> multiScenes = new Stack<>();
    private static List<Request> onAttackClose = new ArrayList<>();
    private static List<Request> onMultiClose = new ArrayList<>();
    private Stage primaryStage;
    private Stage attackStage;
    private Stage multiStage;
    private MediaPlayer music;
    private boolean isPlayingMusic;
    private static View instance = new View();
    // TODO: 6/4/18 don't hardcode babe
    public static final String GAME_TITLE = "War of Titans";
    public static final String ATTACK_TITLE = "Waaaar";
    public static final String MULTI_TITLE = "Multi Player";

    private View() {
    }

    public static void initView(Stage primaryStage) {
        instance.primaryStage = primaryStage;
        primaryStage.setTitle(GAME_TITLE);
        instance.initFont();
        instance.initMusic();
        // TODO: 6/4/18 Set Game Icon
    }

    public static void initAttack() {
        instance.attackStage = new Stage();
        instance.attackStage.setTitle(ATTACK_TITLE);
        instance.attackStage.setOnCloseRequest(event -> instance.closeAttackStage());
    }

    public static void initMulti() {
        instance.multiStage = new Stage();
        instance.multiStage.setTitle(MULTI_TITLE);
        instance.multiStage.setOnCloseRequest(event -> instance.closeMultiStage());
    }

    private void initFont() {
        Font.loadFont(BookOfConstants.getURL(ResourceConstants.FONT_ADVENTURE).toExternalForm(), 20);
    }

    private void initMusic() {
        music = new MediaPlayer(new Media(BookOfConstants.getURL(ResourceConstants.SONG_THEME).toExternalForm()));
        music.setCycleCount(MediaPlayer.INDEFINITE);
        music.play();
        music.setOnPaused(() -> {
            isPlayingMusic = false;
        });
        music.setOnPlaying(() -> {
            isPlayingMusic = true;
        });
    }

    public static View getInstance() {
        return instance;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void pushScene(Parent root) {
        Scene scene = new Scene(root, Visual.START_WIDTH, Visual.START_HEIGHT);
        pushScene(scene, false);
    }

    public void pushScene(Scene scene, boolean isMaximized) {
        primaryStage.setMaximized(isMaximized);
        primaryStage.setFullScreen(isMaximized);
        primaryStage.setScene(scene);
        primaryScenes.push(scene);
        if (primaryScenes.size() == 1) // showing stage for the first time
            primaryStage.show();
    }

    public void pushAttackScene(Parent root) {
        Scene scene = new Scene(root, Visual.ATTACK_WIDTH, Visual.ATTACK_HEIGHT);
        pushAttackScene(scene, false);
    }

    public void pushAttackScene(Scene scene, boolean isMaximized) {
        attackStage.setMaximized(isMaximized);
        attackStage.setScene(scene);
        attackScenes.push(scene);
        if (attackScenes.size() == 1)
            attackStage.show();
    }

    public void pushMultiScene(Parent root) {
        Scene scene = new Scene(root, Visual.MULTI_WIDTH, Visual.MULTI_HEIGHT);
        pushMultiScene(scene, false);
    }
    public void pushMultiScene(Parent root, boolean isMaximized) {
        Scene scene = new Scene(root, Visual.MULTI_WIDTH, Visual.MULTI_HEIGHT);
        pushMultiScene(scene, isMaximized);
    }

    public void pushMultiScene(Scene scene, boolean isMaximized) {
        multiStage.setMaximized(isMaximized);
        multiStage.setScene(scene);
        multiScenes.push(scene);
        if (multiScenes.size() == 1)
            multiStage.show();
    }

    public void popScene(boolean isMaximized) {
        primaryStage.setMaximized(isMaximized);
        primaryStage.setFullScreen(isMaximized);
        primaryScenes.pop();
        primaryStage.setScene(primaryScenes.peek());
    }

    public void popAttackScene(boolean isMaximized) {
        attackStage.setMaximized(isMaximized);
        attackScenes.pop();
        attackStage.setScene(attackScenes.peek());
    }

    public void popMultiScene(boolean isMaximized) {
        multiStage.setMaximized(isMaximized);
        multiScenes.pop();
        multiStage.setScene(multiScenes.peek());
    }

    public void setOnAttackCloseRequest(Request onAttackCloseRequest) {
        onAttackClose.add(onAttackCloseRequest);
    }

    public void setOnMultiCloseRequest(Request onMultiCloseRequest) {
        onMultiClose.add(onMultiCloseRequest);
    }

    public boolean isPlayingMusic() {
        return isPlayingMusic;
    }

    public void playMusic() {
        music.play();
    }

    public void pauseMusic() {
        music.pause();
    }

    public void close() {
        primaryStage.close();
    }

    public Stage getAttackStage() {
        return attackStage;
    }

    public Stage getMultiStage() {
        return multiStage;
    }

    public void closeAttackStage() {
        attackScenes.clear();
        for (Request request : onAttackClose)
            request.accept();
        attackStage.close();
        attackStage = null;
    }

    public void closeMultiStage() {
        multiScenes.clear();
        for (Request request : onMultiClose) {
            request.accept();
        }
        multiStage.close();
        multiStage = null;
    }
}
