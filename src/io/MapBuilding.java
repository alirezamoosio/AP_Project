package io;

public class MapBuilding {
    private int type;
    private int level;
    private int x;
    private int y;
    // TODO: 7/11/18 check if amount should be removed
    private int amount;

    public MapBuilding(int type, int level, int x, int y) {
        this.type = type;
        this.level = level;
        this.x = x;
        this.y = y;
    }

    public MapBuilding() {
    }

    public int getType() {
        return type;
    }

    public int getLevel() {
        return level;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getAmount() {
        return amount;
    }
}
