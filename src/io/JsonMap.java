package io;

import com.google.gson.Gson;
import constants.BookOfConstants;
import javafx.geometry.Pos;
import model.exceptions.StorageFullException;
import model.game.Village;
import model.game.building.IncompleteBuilding;
import model.game.building.village.storage.ElixirStorage;
import model.game.building.village.storage.GoldStorage;
import model.game.map.Map;
import model.game.building.Building;
import model.game.building.defense.DefenseBuilding;
import model.game.building.village.storage.Storage;
import model.game.map.Position;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import static constants.EntityNames.WALL;

public class JsonMap {
    private int[] size;
    private Wall[] walls;
    private Resource resources;
    private MapBuilding[] buildings;

    public static JsonMap mapToJson(Map map) {
        JsonMap jsonMap = new JsonMap();
        jsonMap.size = new int[]{map.getWidth(), map.getHeight()};
        int gold = 0;
        int elixir = 0;
        ArrayList<MapBuilding> jsonBuildings = new ArrayList<>();
        ArrayList<Wall> jsonWalls = new ArrayList<>();
        for (ArrayList<Building> mapBuildings : map.getBuildingsMap().values()) {
            for (Building building : mapBuildings) {
                if (building instanceof IncompleteBuilding)
                    continue;

                int type = building.getType();
                int level = building.getLevel();
                int x = building.getX();
                int y = building.getY();
                if (building instanceof model.game.building.defense.Wall) {
                    jsonWalls.add(new Wall(level, x, y));
                    continue;
                }
                if (building instanceof GoldStorage) {
                    gold += ((GoldStorage) building).getHolding();
                }
                if (building instanceof ElixirStorage) {
                    elixir += ((ElixirStorage) building).getHolding();
                }
                jsonBuildings.add(new MapBuilding(building.getType(), level, x, y));
            }
        }
        jsonMap.walls = jsonWalls.toArray(new Wall[0]);
        jsonMap.buildings = jsonBuildings.toArray(new MapBuilding[0]);
        jsonMap.resources = new Resource(gold, elixir);
        return jsonMap;
    }

    public Map convert() {
        Map attackMap = new Map(size[0], size[1]);
        try {
            for (Wall wall : walls) {
                Position position = new Position(wall.getX(), wall.getY());
                Building building = attackMap.newBuilding(position, BookOfConstants.getType(WALL));
                for (int i = 0; i < wall.getLevel(); i++) {
                    building.upgrade();
                }
            }

            for (MapBuilding mapBuilding : buildings) {
                Position position = new Position(mapBuilding.getX(), mapBuilding.getY());
                Building building = attackMap.newBuilding(position, mapBuilding.getType());
                while (building.getLevel() < mapBuilding.getLevel())
                    building.upgrade();
                if (building instanceof Storage) {
                    try {
                        ((Storage) building).add((mapBuilding.getAmount()));
                    } catch (StorageFullException e) {
                        ((Storage) building).fillStorage();
                    }
                }
            }

            // TODO: 5/4/18 : why should we read gold?
//        attackMap.setGold(resources.gold);
//        attackMap.setElixir(resources.elixir);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Invalid file");
        }
        return attackMap;
    }

    public static JsonMap readFromFile(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        Scanner scanner = new Scanner(file);
        StringBuilder jsonStringBuilder = new StringBuilder();
        while (scanner.hasNext()) {
            jsonStringBuilder.append(scanner.nextLine());
        }
        String jsonString = jsonStringBuilder.toString().
                replace(" ", "").
                replace("\t", "")
                .replace("\n","");
        Gson gson = new Gson();
        JsonMap jsonMap = gson.fromJson(jsonString, JsonMap.class);
        return jsonMap;
    }

    // TODO: 5/6/18 remove
//    public static JsonMap readFromFile(String filePath) throws FileNotFoundException {
//        // TODO: 4/30/18 check for bugs as it is copied code
//        File file = new File(filePath);
//        Scanner scanner = new Scanner(file);
//        Gson gson = new Gson();
//        String jsonString = scanner.nextLine();
//        JsonMap jsonMap = gson.fromJson(jsonString, JsonMap.class);
//        return jsonMap;
//    }


}





