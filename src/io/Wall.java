package io;

public class Wall {
    // TODO: 4/30/18 check if it needs to be implemented
    private int level;
    private int x;
    private int y;

    public Wall(int level, int x, int y) {
        this.level = level;
        this.x = x;
        this.y = y;
    }

    public int getLevel() {
        return level;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}