package io;

public class Resource {
    int gold;
    int elixir;

    public Resource(int gold, int elixir) {
        this.gold = gold;
        this.elixir = elixir;
    }
}
